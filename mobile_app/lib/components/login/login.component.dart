import 'dart:developer' as LOGGER;

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/home/home.component.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/user.model.dart';
import 'package:sith/services/user.service.dart';

import 'login.view.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/LoginPage';

  @override
  State<StatefulWidget> createState() => new LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final formKey = new GlobalKey<FormState>();

  String email;
  String password;
  String errorMessage;
  bool isLoading;
  bool isLoginForm;
  bool isCodis;

  @override
  void initState() {
    errorMessage = "";
    isLoading = false;
    isLoginForm = true;
    isCodis = false;
    super.initState();
  }

  void refreshWidget() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: new Text("Sith", style: TextStyle(fontSize: 20)),
        backgroundColor: ColorRessources.darkColor,
      ),
      body: LoginView(this).buildView(),
    );
  }

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      isLoading = false;
      return false;
    }
  }

  void validateAndSubmit() async {
    setState(() {
      errorMessage = "";
      isLoading = true;
    });
    if (validateAndSave()) {
      try {
        if (isLoginForm) {
          await FirebaseAuth.instance
              .signInWithEmailAndPassword(
                email: email,
                password: password,
              )
              .then(
                (user) => {
              UserService().getUser().then((onValue) =>
                  Navigator.pushReplacementNamed(context, HomePage.routeName))
                },
              );
        } else {
          await FirebaseAuth.instance
              .createUserWithEmailAndPassword(email: email, password: password)
              .then(
                (user) => {
              UserService().insertUser(new User(user.uid, isCodis)).then((
                  onValue) =>
                  Navigator.pushReplacementNamed(context, HomePage.routeName))
                },
              );
        }
        setState(() {
          isLoading = false;
        });
      } catch (e) {
        LOGGER.log("Error: $e");
        setState(() {
          isLoading = false;
          errorMessage = e.message;
        });
      }
    }
  }

  void resetForm() {
    formKey.currentState.reset();
    errorMessage = "";
  }

  void toggleFormMode() {
    resetForm();
    setState(() {
      isLoginForm = !isLoginForm;
    });
  }
}
