import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/login/login.component.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/glowRemover.view.dart';

class LoginView {
  LoginPageState loginPage;

  LoginView(_loginPage) {
    loginPage = _loginPage;
  }

  Widget buildView() {
    return Stack(
      children: <Widget>[
        showForm(),
        showCircularProgress(),
      ],
    );
  }

  Widget showCircularProgress() {
    if (loginPage.isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(height: 0.0, width: 0.0);
  }

  Widget showForm() {
    return Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0),
      child: new Form(
        key: loginPage.formKey,
        child: ScrollConfiguration(
          behavior: GlowRemover(),
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              SizedBox(height: 20),
              showLogo(),
              showEmailInput(),
              showPasswordInput(),
              showIsCodisBox(),
              showPrimaryButton(),
              showSecondaryButton(),
              showErrorMessage(),
            ],
          ),
        ),
      ),
    );
  }

  Widget showLogo() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: ColorRessources.transparentColor,
          radius: 48.0,
        ),
      ),
    );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: FlutterI18n.translate(loginPage.context, "email"),
          icon: new Icon(
            Icons.email,
            color: ColorRessources.primaryColor,
          ),
        ),
        validator: (value) => !EmailValidator.validate(value, true) ? 'Not a valid email.' : null,
        onSaved: (value) => loginPage.email = value.trim(),
      ),
    );
  }

  Widget showIsCodisBox() {
    return (!loginPage.isLoginForm)
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(FlutterI18n.translate(loginPage.context, "codis"),
              style: TextStyle(color: ColorRessources.primaryColor)),
              Checkbox(
                value: loginPage.isCodis,
                onChanged: (bool value) {
                  loginPage.isCodis = value;
                  loginPage.refreshWidget();
                },
              ),
            ],
          )
        : Container(width: 0, height: 0,);
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
          hintText: FlutterI18n.translate(loginPage.context, "password"),
          icon: new Icon(
            Icons.lock,
            color: ColorRessources.greyColor,
          ),
        ),
        validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
        onSaved: (value) => loginPage.password = value.trim(),
      ),
    );
  }

  Widget showPrimaryButton() {
    return new Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 40.0,
        child: new RaisedButton(
          elevation: 5.0,
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
          color: ColorRessources.primaryColor,
          child: new Text(
            loginPage.isLoginForm ? FlutterI18n.translate(loginPage.context, "login") : FlutterI18n.translate(loginPage.context, "create_account"),
            style: new TextStyle(
              fontSize: 20.0,
              color: ColorRessources.whiteColor,
            ),
          ),
          onPressed: loginPage.validateAndSubmit,
        ),
      ),
    );
  }

  Widget showSecondaryButton() {
    return new FlatButton(
      child: new Text(
        loginPage.isLoginForm
            ? FlutterI18n.translate(loginPage.context, "create_account")
            : FlutterI18n.translate(loginPage.context, "sign_in"), textAlign: TextAlign.center,
        style: new TextStyle(
          fontSize: 18.0,
          fontWeight: FontWeight.w300,
        ),
      ),
      onPressed: loginPage.toggleFormMode,
    );
  }

  Widget showErrorMessage() {
    if (loginPage.errorMessage != null && loginPage.errorMessage.length > 0) {
      return new Text(
        loginPage.errorMessage,
        style: TextStyle(
          fontSize: 13.0,
          color: ColorRessources.redColor,
          height: 1.0,
          fontWeight: FontWeight.w300,
        ),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }
}
