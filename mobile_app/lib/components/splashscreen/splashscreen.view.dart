import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:sith/components/home/home.component.dart';
import 'package:sith/components/login/login.component.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/services/user.service.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SplashScreen();
}

class _SplashScreen extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: 5),
    );
    animationController.repeat();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new FutureBuilder<FirebaseUser>(
      future: FirebaseAuth.instance.currentUser(),
      builder: (context, AsyncSnapshot<FirebaseUser> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          snapshot.hasData
              ? SchedulerBinding.instance.addPostFrameCallback((_) {
            UserService().getUser().then((onValue) =>
            {
              Navigator.pushReplacementNamed(
                  context, HomePage.routeName)
            });
          })
              : SchedulerBinding.instance.addPostFrameCallback((_) {
            Navigator.pushReplacementNamed(context, LoginPage.routeName);
          });
          return Container();
        } else {
          return loadingScreen();
        }
      },
    );
  }

  Widget loadingScreen() {
    return new Scaffold(
      backgroundColor: ColorRessources.darkColor,
      body: new Center(
        child: new Container(
          child: new AnimatedBuilder(
            animation: animationController,
            child: new Container(
              height: 150.0,
              width: 150.0,
              child: Center(child: CircularProgressIndicator()),
            ),
            builder: (BuildContext context, Widget _widget) {
              return new Transform.rotate(
                angle: animationController.value * 6.3,
                child: _widget,
              );
            },
          ),
        ),
      ),
    );
  }
}
