import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/home/home.view.dart';
import 'package:sith/domain/enum/category.model.dart';
import 'package:sith/domain/vehicule.model.dart';
import 'package:sith/services/user.service.dart';

class HomePage extends StatefulWidget {
  static const routeName = '/Home';

  @override
  State<StatefulWidget> createState() => new HomePageState();
}

class HomePageState extends State<HomePage> {
  bool isInterventionPage;

  @override
  void initState() {
    isInterventionPage = true;
    super.initState();
  }

  void refreshWidget() {
    setState(() {});
  }

  void changeRole() {
    UserService().switchRole().then((onValue) => refreshWidget());
  }

  Future<void> changeLangage() async {
    if (FlutterI18n.currentLocale(context) == Locale('fr')) {
      await FlutterI18n.refresh(context, Locale('en'));
    } else {
      await FlutterI18n.refresh(context, Locale('fr'));
    }
    this.refreshWidget();
  }

  Locale getCurrentLangage() {
    return FlutterI18n.currentLocale(context);
  }

  @override
  Widget build(BuildContext context) {
    return new HomeView(this).buildView(context);
  }

  List<Vehicule> _sortListByColor(List<Vehicule> vehicules) {
    List<Vehicule> orderedList = new List();
    Category.values.forEach((categ) {
      orderedList
          .addAll(vehicules.where((vehicule) => (vehicule.category == categ)));
    });
    return orderedList;
  }

  Map<String, List<Vehicule>> sortMapByColor(
      Map<String, List<Vehicule>> myVehicules) {
    Map<String, List<Vehicule>> orderedMap = new SplayTreeMap();
    myVehicules.forEach((type, vehicules) {
      orderedMap[type] = _sortListByColor(vehicules);
    });
    return orderedMap;
  }
}
