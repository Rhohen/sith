import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/home/home.component.dart';
import 'package:sith/components/intervention/form/form.component.dart';
import 'package:sith/components/intervention/list/list.component.dart';
import 'package:sith/components/intervention/vehicules.service.dart';
import 'package:sith/components/means/means_demand_list/means-demand-list.component.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/menu-actions.util.dart';
import 'package:sith/components/utils/menuitem.view.dart';
import 'package:sith/domain/enum/firefighter.model.dart';
import 'package:sith/domain/vehicule.model.dart';
import 'package:sith/services/user.service.dart';

class HomeView {
  final HomePageState _homePage;

  HomeView(this._homePage);

  Widget buildView(BuildContext context) {
    if (UserService().getLocalUser().isCodis) {
      return _buildCodisView(context);
    } else {
      return _buildCosView(context);
    }
  }

  DefaultTabController _buildCodisView(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
            appBar: new AppBar(
                title: new Text(FlutterI18n.translate(context, "sith")),
                bottom: _showTabBar(context),
                actions: <Widget>[
                  _showChangeRoleButton(),
                  PopupMenuButton<String>(
                    onSelected: (value) =>
                        MenuActions.choiceAction(value, context, _homePage.changeLangage),
                    itemBuilder: (BuildContext context) {
                      return MenuItems.choices.entries.map((elem) {
                        return PopupMenuItem<String>(
                          value: elem.key,
                          child: MenuItems.rowItem(FlutterI18n.translate(context, elem.key), elem.value),
                        );
                      }).toList();
                    },
                  )
                ]),
            body:
                TabBarView(physics: NeverScrollableScrollPhysics(), children: [
              InterventionsListPage(UserService().getLocalUser().isCodis),
              MeansDemandListPage()
            ]),
            floatingActionButton: Visibility(
                child: _showAddInterventionButton(context),
                visible: _homePage.isInterventionPage)));
  }

  Scaffold _buildCosView(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
            title: new Text(
                FlutterI18n.translate(context, "intervention_list_title")),
            actions: <Widget>[
              _showChangeRoleButton(),
              PopupMenuButton<String>(
                onSelected: (value) => MenuActions.choiceAction(value, context, _homePage.changeLangage),
                itemBuilder: (BuildContext context) {
                  return MenuItems.choices.entries.map((elem) {
                    return PopupMenuItem<String>(
                      value: elem.key,
                      child: MenuItems.rowItem(FlutterI18n.translate(context, elem.key), elem.value),
                    );
                  }).toList();
                },
              )
            ]),
        body: InterventionsListPage(UserService().getLocalUser().isCodis));
  }

  TabBar _showTabBar(BuildContext context) {
    return TabBar(
        tabs: [
          Tab(
              text: FlutterI18n.translate(context, "intervention_list_title"),
              icon: Icon(Icons.report_problem)),
          Tab(
              text: FlutterI18n.translate(context, "means_demand"),
              icon: Icon(Icons.directions_car))
        ],
        onTap: (int index) {
          if (index != 0) {
            _homePage.isInterventionPage = false;
            _homePage.refreshWidget();
          } else {
            _homePage.isInterventionPage = true;
            _homePage.refreshWidget();
          }
        });
  }

  Column _showChangeRoleButton() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        FlatButton(
          onPressed: _homePage.changeRole,
          textColor: ColorRessources.whiteColor,
          child: Column(
            children: <Widget>[
              Icon(Icons.swap_horiz),
              Text(UserService().getLocalUser().isCodis ? "COS" : "CODIS")
            ],
          ),
        ),
      ],
    );
  }

  FloatingActionButton _showAddInterventionButton(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      backgroundColor: ColorRessources.primaryColor,
      onPressed: () {
        Map<String, List<Vehicule>> myVehicules = new SplayTreeMap();

        Firefighter.values.forEach((vehiculeType) =>
            myVehicules[EnumToString.parse(vehiculeType)] = new List());

        VehiculesService()
            .getAvailableVehicules()
            .then((QuerySnapshot snapshot) {
          snapshot.documents.forEach((vehicule) {
            Vehicule myVehicule =
                Vehicule.from(vehicule.documentID, vehicule.data);
            String myType = EnumToString.parse(myVehicule.type);
            if (!myVehicules.containsKey(myType))
              myVehicules[myType] = new List();
            myVehicules[myType].add(myVehicule);
          });
          myVehicules = _homePage.sortMapByColor(myVehicules);
          Navigator.pushNamed(context, InterventionFormPage.routeName,
              arguments: myVehicules);
        });
      },
    );
  }
}
