import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sith/domain/intervention.model.dart';
import 'package:sith/services/firestore.service.dart';

class InterventionService {
  static CollectionReference interventionCollectionReference;

  static final InterventionService _singleton = initiate();

  factory InterventionService() {
    return _singleton;
  }

  /// Constructeur privé interne
  InterventionService._internal();

  static initiate() {
    interventionCollectionReference = FireConn().getInterventionsCollection();
    return InterventionService._internal();
  }

  Future<String> insertIntervention(Intervention intervention) {
    DocumentReference documentReference =
        interventionCollectionReference.document();
    return documentReference.setData(intervention.toJson()).then((onValue) {
      return documentReference.documentID;
    });
  }

  Future updateIntervention(Intervention intervention) {
    return interventionCollectionReference
        .document(intervention.uid)
        .updateData(intervention.toJson());
  }

  Future<DocumentSnapshot> getIntervention(String interventionUid) {
    return interventionCollectionReference.document(interventionUid).get();
  }

  Stream<QuerySnapshot> getInterventionsSnapshots() {
    return interventionCollectionReference.snapshots();
  }

  Stream<QuerySnapshot> getOpenedInterventionsSnapshots() {
    return interventionCollectionReference
        .where('closed', isEqualTo: false)
        .snapshots();
  }

  CollectionReference getInterventionCollectionReference() {
    return interventionCollectionReference;
  }
}
