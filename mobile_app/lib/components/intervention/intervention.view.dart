import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/means/means_datatable/means-datatable.component.dart';
import 'package:sith/components/sitac/sitac.component.dart';
import 'package:sith/components/utils/color.ressources.dart';

import 'intervention.component.dart';

class InterventionView {
  InterventionPageState interventionPage;
  BuildContext context;

  InterventionView(this.interventionPage, this.context);

  Scaffold buildView() {
    return Scaffold(
      appBar: new AppBar(title: new Text(interventionPage.intervention.name)),
      body: Container(
          child: Column(
        children: <Widget>[
          _showInformationsColumn(),
          Expanded(child: _showMeansColumn())
        ],
      )),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.map),
        backgroundColor: ColorRessources.primaryColor,
        onPressed: () {
          Navigator.pushNamed(context, Sitac.routeName,
              arguments: interventionPage.intervention);
        },
      ),
    );
  }

  Padding _showInformationsColumn() {
    List informationsWidgets = _constructList();
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[...informationsWidgets],
      ),
    );
  }

  Widget _showMeansColumn() {
    return MeansDataTablePage(interventionPage.intervention.uid);
  }

  Column _showCodeSinistre() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(FlutterI18n.translate(context, "codeSinistre"),
            style: TextStyle(fontWeight: FontWeight.bold)),
        Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Text(
              EnumToString.parse(interventionPage.intervention.codeSinistre),
              overflow: TextOverflow.ellipsis,
            ))
      ],
    );
  }

  Flexible _showAddress() {
    return Flexible(
      flex: 2,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(FlutterI18n.translate(context, "address"),
              style: TextStyle(fontWeight: FontWeight.bold)),
          Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Text(
                interventionPage.intervention.address,
                overflow: TextOverflow.ellipsis,
              ))
        ],
      ),
    );
  }

  Widget _showCoordonates() {
    if (interventionPage.intervention.coordinate != null) {
      return Flexible(
          flex: 1,
          child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(FlutterI18n.translate(context, "coordonates"),
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Flexible(
                    child: Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                                child: Text(
                              interventionPage.intervention.coordinate.latitude
                                      .toString() +
                                  " ; ",
                              overflow: TextOverflow.ellipsis,
                            )),
                            Flexible(
                                child: Text(
                              interventionPage.intervention.coordinate.longitude
                                  .toString(),
                              overflow: TextOverflow.ellipsis,
                            ))
                          ],
                        ))),
              ]));
    }
    return Container();
  }

  Flexible _showContact() {
    return Flexible(
        flex: 1,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(FlutterI18n.translate(context, "contact"),
                style: TextStyle(fontWeight: FontWeight.bold)),
            Flexible(
                child: Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: Text(
                      interventionPage.intervention.contact,
                      overflow: TextOverflow.ellipsis,
                    )))
          ],
        ));
  }

  List _constructList() {
    List informationsWidgets = [];
    if (interventionPage.intervention.codeSinistre != null) {
      informationsWidgets.add(_showCodeSinistre());
    }
    if (interventionPage.intervention.contact.isNotEmpty) {
      informationsWidgets.add(_showContact());
    }
    if (interventionPage.intervention.address.isNotEmpty) {
      informationsWidgets.add(_showAddress());
    }
    if (interventionPage.intervention.coordinate != null) {
      informationsWidgets.add(_showCoordonates());
    }
    return informationsWidgets;
  }
}
