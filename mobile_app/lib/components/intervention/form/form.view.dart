import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/intervention/form/mapBoxSearchForm.view.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/enum/codeSinistre.model.dart';

import 'draggable/draggable.component.dart';
import 'form.component.dart';

class InterventionFormView {
  InterventionFormPageState formPage;
  BuildContext context;

  InterventionFormView(this.formPage, this.context);

  Widget buildView() {
    return Scaffold(
      resizeToAvoidBottomInset: false, // set it to false
      appBar: AppBar(
        title: Text(FlutterI18n.translate(context, "intervention_title"),
            style: TextStyle(fontSize: 20)),
        backgroundColor: ColorRessources.darkColor,
      ),
      body: showForm(),
    );
  }

  Widget showForm() {
    return Form(
      key: formPage.formKey,
      child: Stack(
        children: <Widget>[
          ListView(
            shrinkWrap: true,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Flexible(child: _showLabel()),
                  Flexible(child: _showCodeSinistre()),
                  Flexible(child: _showContact()),
                ],
              ),
              _showAddress(),
              Row(
                children: <Widget>[
                  Flexible(child: _showLatitude()),
                  Flexible(child: _showLongitude()),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
                child: DraggableComponent(
                    formPage.vehicules, formPage.notifyParent, false),
              ),
              SizedBox(
                height: 80,
              )
            ],
          ),
          Align(
            alignment: FractionalOffset.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(child: _showCancelButton()),
                Flexible(child: _showValidateButton()),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _showLabel() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 25, 20, 0),
      child: TextFormField(
        controller: formPage.labelController,
        maxLines: 1,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: 'Label *',
          icon: Icon(
            Icons.create,
            color: ColorRessources.greyColor,
          ),
        ),
        validator: (value) => value.isEmpty
            ? FlutterI18n.translate(context, "label_required_error_message")
            : null,
        onChanged: (value){
          formPage.intervention.name = value;
        },
        onSaved: (value) {
          formPage.intervention.name = value;
          formPage.labelController.text = value;
          formPage.refreshWidget();
        },
      ),
    );
  }

  Widget _showCodeSinistre() {
    return Padding(
      padding: EdgeInsets.only(top: 25),
      child: Row(
        children: [
          Icon(
            Icons.report,
            color: ColorRessources.greyColor,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: DropdownButtonFormField<CodeSinistre>(
              isExpanded: true,
              isDense: true,
              hint: Text(
                FlutterI18n.translate(context, "code_sinistre"),
                overflow: TextOverflow.ellipsis,
              ),
              // Not necessary for Option 1
              value: formPage.intervention.codeSinistre,
              onChanged: (CodeSinistre code) {
                formPage.intervention.codeSinistre = code;
                formPage.refreshWidget();
              },
              items: CodeSinistre.values.map<DropdownMenuItem<CodeSinistre>>(
                  (CodeSinistre codeSinistre) {
                return DropdownMenuItem<CodeSinistre>(
                  value: codeSinistre,
                  child: Text(EnumToString.parse(codeSinistre)),
                );
              }).toList(),
              validator: (value) => null != value
                  ? null
                  : FlutterI18n.translate(
                      context, "code_sinistre_required_error_message"),
            ),
          ),
        ],
      ),
    );
  }

  Widget _showContact() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 25, 20, 0),
      child: TextFormField(
        controller: formPage.contactController,
        maxLines: 1,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
          hintText: 'Contact',
          icon: Icon(
            Icons.phone,
            color: ColorRessources.greyColor,
          ),
        ),
        validator: (value) => formPage.validateContact(value)
            ? null
            : FlutterI18n.translate(context, "contact_required_error_message"),
        onSaved: (value) {
          formPage.intervention.contact = value;
          formPage.contactController.text = value;
          formPage.refreshWidget();
        },
      ),
    );
  }

  Widget _showAddress() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: MapBoxPlaceSearchFormWidget(
        enable: formPage.isAddressMapBoxEnable,
        textEditingController: formPage.textEditingController,
        geocodeOnSelect: true,
        formPage: formPage,
        latitudeController: formPage.latitudeController,
        longitudeController: formPage.longitudeController,
        language: 'fr',
        country: 'fr',
        height: MediaQuery.of(context).size.height * 0.4,
        apiKey: formPage.apikey,
        limit: 10,
        searchHint: FlutterI18n.translate(context, "address_form"),
        onSelected: (place) {
          formPage.addressController.text = place.placeName;
          formPage.intervention.address = place.placeName;
          formPage.refreshWidget();
          if (formPage.addressController.text.isEmpty &&
              formPage.latitude.isEmpty &&
              formPage.longitude.isEmpty) {
            FlutterI18n.translate(context, "address_required_error_message");
          }
        },
      ),
    );
  }

  Widget _showLatitude() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: TextFormField(
          enabled: formPage.intervention.address.isEmpty &&
              formPage.isAddressMapBoxEnable,
          maxLines: 1,
          keyboardType: TextInputType.phone,
          controller: formPage.latitudeController,
          decoration: InputDecoration(
            hintText: 'Latitude ',
            icon: Icon(
              Icons.location_on,
              color: ColorRessources.greyColor,
            ),
          ),
          validator: (value) => (value.isEmpty && formPage.longitude.isEmpty) ||
                  (formPage.validateLatitude(value) &&
                      formPage.validateLongitude(formPage.longitude))
              ? null
              : FlutterI18n.translate(context, "coord_required_error_message"),
          onSaved: (value) {
            formPage.latitude = value;
            formPage.refreshWidget();
          }),
    );
  }

  Widget _showLongitude() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
      child: TextFormField(
          enabled: formPage.intervention.address.isEmpty &&
              formPage.isAddressMapBoxEnable,
          controller: formPage.longitudeController,
          maxLines: 1,
          keyboardType: TextInputType.phone,
          decoration: InputDecoration(hintText: 'Longitude'),
          validator: (value) => (value.isEmpty && formPage.latitude.isEmpty) ||
                  (formPage.validateLongitude(value) &&
                      formPage.validateLatitude(formPage.latitude))
              ? null
              : FlutterI18n.translate(context, "coord_required_error_message"),
          onSaved: (value) {
            formPage.longitude = value;
            formPage.refreshWidget();
          }),
    );
  }

  Widget _showValidateButton() {
    return Padding(
      padding: EdgeInsets.only(bottom: 15, left: 30),
      child: Material(
        elevation: 5, // needed
        color: Color.fromRGBO(46, 139, 87, 1),
        borderRadius: BorderRadius.circular(10),
        child: InkWell(
          child: Padding(
            padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
            child: Text(
              FlutterI18n.translate(context, "validate_button"),
              style: TextStyle(color: ColorRessources.whiteColor, fontSize: 17),
            ),
          ),
          borderRadius: BorderRadius.circular(10),
          splashColor: ColorRessources.splashColor,
          onTap: () {
            formPage.validateAndSubmit();
          },
        ),
      ),
    );
  }

  Widget _showCancelButton() {
    return Padding(
      padding: EdgeInsets.only(bottom: 15, right: 30),
      child: Material(
        elevation: 5, // needed
        color: ColorRessources.whiteColor,
        borderRadius: BorderRadius.circular(10),
        child: InkWell(
          child: Padding(
            padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
            child: Text(
              FlutterI18n.translate(context, "cancel_button"),
              style: TextStyle(
                  color: Color.fromRGBO(46, 139, 87, 1), fontSize: 17),
            ),
          ),
          borderRadius: BorderRadius.circular(10),
          splashColor: ColorRessources.whiteColor,
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
