import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:mapbox_search/mapbox_search.dart';
import 'package:sith/components/intervention/form/form.component.dart';
import 'package:sith/components/intervention/form/manualPositionSelection.view.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/services/geocoding.service.dart';

class MapBoxPlaceSearchFormWidget extends StatefulWidget {
  MapBoxPlaceSearchFormWidget(
      {@required this.apiKey,
      this.onSelected,
      // this.onSearch,
      this.searchHint = 'Search',
      this.language = 'en',
      this.location,
      this.limit = 5,
      this.country,
      this.context,
      this.height,
      this.popOnSelect = false,
      this.geocodeOnSelect = false,
      this.formPage,
      this.latitudeController,
      this.longitudeController,
      this.enable = true,
      @required this.textEditingController});

  final bool enable;
  final TextEditingController textEditingController;
  final TextEditingController longitudeController;
  final TextEditingController latitudeController;
  final InterventionFormPageState formPage;

  /// True if there is a geocoding process to make
  final bool geocodeOnSelect;

  /// True if there is different search screen and you want to pop screen on select
  final bool popOnSelect;

  ///To get the height of the page
  final BuildContext context;

  /// Height of whole search widget
  final double height;

  /// API Key of the MapBox.
  final String apiKey;

  /// The callback that is called when one Place is selected by the user.
  final void Function(MapBoxPlace place) onSelected;

  /// The callback that is called when the user taps on the search icon.
  // final void Function(MapBoxPlaces place) onSearch;

  /// Language used for the autocompletion.
  ///
  /// Check the full list of [supported languages](https://docs.mapbox.com/api/search/#language-coverage) for the MapBox API
  final String language;

  /// The point around which you wish to retrieve place information.
  final Location location;

  /// Limits the no of predections it shows
  final int limit;

  ///Limits the search to the given country
  ///
  /// Check the full list of [supported countries](https://docs.mapbox.com/api/search/) for the MapBox API
  final String country;

  ///Search Hint Localization
  final String searchHint;

  @override
  _MapBoxPlaceSearchWidgetState createState() =>
      _MapBoxPlaceSearchWidgetState(formPage, textEditingController);
}

class _MapBoxPlaceSearchWidgetState extends State<MapBoxPlaceSearchFormWidget>
    with SingleTickerProviderStateMixin {
  TextEditingController textEditingController;

  // SearchContainer height.
  Animation _containerHeight;

  // Place options opacity.
  Animation _listOpacity;

  // MapBoxPlace _selectedPlace;

  Timer _debounceTimer;
  InterventionFormPageState formPage;

  _MapBoxPlaceSearchWidgetState(this.formPage, this.textEditingController);

  @override
  void initState() {
    formPage.animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    _containerHeight = Tween<double>(
            begin: 73,
            end: widget.height ??
                MediaQuery.of(widget.context).size.height - 60 ??
                300)
        .animate(
      CurvedAnimation(
        curve: Interval(0.0, 0.5, curve: Curves.easeInOut),
        parent: formPage.animationController,
      ),
    );
    _listOpacity = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(
      CurvedAnimation(
        curve: Interval(0.5, 1.0, curve: Curves.easeInOut),
        parent: formPage.animationController,
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    if (_debounceTimer != null) {
      _debounceTimer.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(horizontal: 5),
        width: MediaQuery.of(context).size.width,
        child: _searchContainer(
          child: _searchInput(context),
        ),
      );

  // Widgets
  Widget _searchContainer({Widget child}) {
    return AnimatedBuilder(
        animation: formPage.animationController,
        builder: (context, _) {
          return Container(
            height: _containerHeight.value - 15,
            decoration: _containerDecoration(),
            alignment: Alignment.center,
            padding: EdgeInsets.only(top: 5),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: child,
                ),
                Expanded(
                  child: Opacity(
                    opacity: _listOpacity.value,
                    child: ListView(
                      // addSemanticIndexes: true,
                      // itemExtent: 10,
                      children: <Widget>[
                        for (var places in formPage.placePredictions)
                          _placeOption(places),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _searchInput(BuildContext context) {
    List<Widget> gestures = [
      InkWell(
        child: Center(
            child: Container(
                child: Text(FlutterI18n.translate(context, "erase"),
                    style: TextStyle(
                      color: ColorRessources.greyColor,
                    )))),
        onTap: () {
          textEditingController.clear();
          formPage.animationController.animateTo(0.5);
          setState(() => formPage.placePredictions = []);
          formPage.animationController.reverse();
          formPage.intervention.address = "";
          formPage.longitudeController.text = "";
          formPage.latitudeController.text = "";
          formPage.longitude = "";
          formPage.latitude = "";
          formPage.isAddressMapBoxEnable = true;
          formPage.refreshWidget();
        },
      ),
      Padding(
        padding: EdgeInsets.only(left: 6, right: 5),
        child: Container(
          width: 1,
          height: 15,
          color: ColorRessources.accentColor,
        ),
      ),
      GestureDetector(
        child: Icon(
          Icons.location_searching,
          color: ColorRessources.accentColor,
        ),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ManualPositionSelection(formPage)));
        },
      )
    ];
    return Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              enabled: widget.enable,
              decoration: _inputStyle(),
              controller: textEditingController,
              style: InputDecorationTheme().hintStyle,
              onChanged: (value) {
                formPage.intervention.address = value;
                formPage.refreshWidget();
                _debounceTimer?.cancel();
                _debounceTimer = Timer(Duration(milliseconds: 750), () {
                  if (mounted) {
                    _autocompletePlace(value);
                    setState(() {});
                  }
                });
              },
            ),
          ),
          Container(width: 15),
          ...gestures
        ],
      ),
    );
  }

  Widget _placeOption(MapBoxPlace prediction) {
    String place = prediction.text;
    String fullName = prediction.placeName;

    return MaterialButton(
      padding: EdgeInsets.symmetric(horizontal: 10),
      onPressed: () => _selectPlace(prediction),
      child: ListTile(
        title: Text(
          place.length < 45
              ? "$place"
              : "${place.replaceRange(45, place.length, "")} ...",
          style: InputDecorationTheme().hintStyle,
          maxLines: 1,
        ),
        subtitle: Text(
          fullName,
          overflow: TextOverflow.ellipsis,
          style: InputDecorationTheme().hintStyle,
          maxLines: 1,
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 0,
        ),
      ),
    );
  }

  // Styling
  InputDecoration _inputStyle() {
    return InputDecoration(
      hintText: widget.searchHint,
      border: InputBorder.none,
      contentPadding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
    );
  }

  BoxDecoration _containerDecoration() {
    return BoxDecoration(
      color: ColorRessources.whiteColor,
      borderRadius: BorderRadius.all(Radius.circular(6.0)),
      boxShadow: [
        BoxShadow(color: ColorRessources.blackColor, blurRadius: 0, spreadRadius: 0)
      ],
    );
  }

  // Methods
  void _autocompletePlace(String input) async {
    /// Will be called when the input changes. Making callbacks to the Places
    /// Api and giving the user Place options
    ///
    if (input.length > 0) {
      var placesSearch = PlacesSearch(
        apiKey: widget.apiKey,
        country: widget.country,
      );

      final predictions = await placesSearch.getPlaces(
        input,
        location: widget.location,
      );

      await formPage.animationController.animateTo(0.5);

      setState(() => formPage.placePredictions = predictions);

      await formPage.animationController.forward();
    } else {
      await formPage.animationController.animateTo(0.5);
      setState(() => formPage.placePredictions = []);
      await formPage.animationController.reverse();
    }
  }

  Future<List<MapBoxPlace>> _geoCode(String address) async {
    var geoCodingService = GeoCoding(
      apiKey: widget.apiKey,
      country: "fr",
    );

    return await geoCodingService.getLatLng(address);
  }

  void _selectPlace(MapBoxPlace prediction) async {
    /// Will be called when a user selects one of the Place options.

    // Sets TextField value to be the location selected
    textEditingController.value = TextEditingValue(
      text: prediction.placeName,
      selection: TextSelection.collapsed(offset: prediction.placeName.length),
    );

    // Makes animation
    await formPage.animationController.animateTo(0.5);
    setState(() {
      formPage.placePredictions = [];
      // _selectedPlace = prediction;
    });
    formPage.animationController.reverse();

    // Calls the `onSelected` callback
    widget.onSelected(prediction);

    if (widget.geocodeOnSelect) {
      List<MapBoxPlace> addresses = await _geoCode(prediction.placeName);
      if (addresses.isNotEmpty) {
        List<double> lngLat = addresses.first.center;
        if (widget.formPage != null) {
          String longitude = lngLat.first.toString();
          widget.formPage.longitude = longitude;

          String latitude = lngLat.last.toString();
          widget.formPage.latitude = latitude;

          if (widget.longitudeController != null) {
            String longitude = lngLat.first.toString();
            widget.longitudeController.text = longitude;
          }
          if (widget.latitudeController != null) {
            String latitude = lngLat.last.toString();
            widget.latitudeController.text = latitude;
          }
        }
      }
    }

    if (widget.popOnSelect) Navigator.pop(context);
  }
}
