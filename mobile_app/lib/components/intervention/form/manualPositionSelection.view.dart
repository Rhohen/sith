import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:sith/components/intervention/form/form.component.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:synchronized/synchronized.dart';

import '../../sitac/map/map.resources.dart';

// ignore: must_be_immutable
class ManualPositionSelection extends StatefulWidget {
  InterventionFormPageState formPage;

  ManualPositionSelection(this.formPage);

  @override
  ManualPositionSelectionState createState() {
    return new ManualPositionSelectionState(formPage);
  }
}

class ManualPositionSelectionState extends State<ManualPositionSelection> {
  List<Marker> markers = new List();
  Marker marker;

  var lock = Lock();
  InterventionFormPageState formPage;

  ManualPositionSelectionState(this.formPage);

  @override
  void initState() {
    marker = null;
    super.initState();
  }

  final String apikey = MapResources.token;

  void placeMarker(LatLng latLng) {
    lock.synchronized(() {
      markers.clear();
      marker = Marker(
        anchorPos: AnchorPos.exactly(new Anchor(-21.5,-42)),
        width: 1.0,
        height: 1.0,
        point: latLng,
        builder: (context) => Container(
          child: IconButton(
            padding: EdgeInsets.all(0),
            alignment: Alignment.topCenter,
            icon: Icon(Icons.location_on),
            color: ColorRessources.darkColor,
            iconSize: 45.0,
            onPressed: () {},
          ),
        ),
      );
      markers.add(marker);
      setState(() {});
    });
  }

  void _validateMarker(LatLng point) {
    formPage.longitude = point.longitude.toString();
    formPage.latitude = point.latitude.toString();
    formPage.longitudeController.text = point.longitude.toString();
    formPage.latitudeController.text = point.latitude.toString();
    formPage.placePredictions = [];
    formPage.animationController.reverse();
    formPage.retrieveAddressFromLatLng(point);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(FlutterI18n.translate(context, "address_required_message"))),
      body: Stack(
        children: <Widget>[
          FlutterMap(
            options: MapOptions(
                onTap: placeMarker,
                center: LatLng(48.11417, -1.68083)), // Rennes par défaut
            layers: [
              TileLayerOptions(
                urlTemplate:
                    "https://api.mapbox.com/styles/v1/hugueslb/ck7un1qfm1ibn1imqpci9y2cq/tiles/256/{z}/{x}/{y}@2x?access_token={accessToken}",
                additionalOptions: {
                  'accessToken': apikey,
                  'id': 'mapbox.mapbox-streets-v8'
                },
              ),
              MarkerLayerOptions(
                markers: [...markers],
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 20),
              child: RaisedButton(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                elevation: 5.0,
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),
                color: ColorRessources.darkColor,
                child: Text(
                  FlutterI18n.translate(context, "validate_button"),
                  style: TextStyle(
                    fontSize: 25.0,
                    color: ColorRessources.whiteColor,
                  ),
                ),
                onPressed:
                    marker != null ? () => _validateMarker(marker.point) : null,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
