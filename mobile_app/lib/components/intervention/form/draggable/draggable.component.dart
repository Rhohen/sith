import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sith/domain/vehicule.model.dart';

import 'draggable.view.dart';

class DraggableComponent extends StatefulWidget {
  static const routeName = '/DraggableComponent';
  final Function(DraggableComponentState) notifyParent;
  final bool popUpCall;

  final Map<String, List<Vehicule>> availableVehicules;

  DraggableComponent(
      this.availableVehicules, this.notifyParent, this.popUpCall);

  @override
  State<StatefulWidget> createState() =>
      DraggableComponentState(availableVehicules, notifyParent, this.popUpCall);
}

class DraggableComponentState extends State<DraggableComponent> {
  final GlobalKey availableVehiculesKey = new GlobalKey();
  final bool popUpCall;
  final Map<String, List<Vehicule>> availableVehicules;
  final Function(DraggableComponentState) notifyParent;

  //Map and list
  List<Vehicule> currentList = [];
  List<Vehicule> deployedVehicules = [];

  int currentDraggableItemIndex = 0;

  //Interface widget size
  int nbLists;
  Size availableVehiculeWidgetSize;

  //Booleans
  bool availableIsDragging = false;
  bool deployedIsDragging = false;

  DraggableComponentState(
      this.availableVehicules, this.notifyParent, this.popUpCall);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => getAvailableVehiculeSize());
  }

  @override
  Widget build(BuildContext context) {
    return new DraggableView(this).buildView();
  }

  void updateCurrentDragItemInfo(list, index) {
    currentList = list;
    currentDraggableItemIndex = index;
  }

  void clearDeployableMeans() {
    List<Vehicule> toRemove = [];
    deployedVehicules.forEach((e) => toRemove.add(e));
    deployedVehicules.removeWhere((e) => toRemove.contains(e));
    toRemove
        .forEach((e) => availableVehicules[EnumToString.parse(e.type)].add(e));
  }

  void getAvailableVehiculeSize() {
    RenderBox _availableVehiculesRenderBox =
        availableVehiculesKey.currentContext.findRenderObject();
    availableVehiculeWidgetSize = _availableVehiculesRenderBox.size;
    setState(() {});
  }

  DraggableComponentState getState() {
    return this;
  }

  refreshWidget() {
    widget.notifyParent(this);
  }

  void refresh() {
    setState(() {});
  }

  void validateVehicule() {
    widget.notifyParent(this);
  }
}
