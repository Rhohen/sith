import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/intervention/form/draggable/draggable.component.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/colorUtils.dart';
import 'package:sith/domain/vehicule.model.dart';

class DraggableView {
  DraggableComponentState draggable;

  DraggableView(this.draggable);

  Widget buildView() {
    List<Widget> vehiculesComponents = new List();

    draggable.nbLists = draggable.availableVehicules.length;
    int idList = 0;
    draggable.availableVehicules.forEach((key, val) {
      vehiculesComponents.add(Text(
        "$key " + FlutterI18n.translate(draggable.context, "available"),
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
      ));
      vehiculesComponents.add(
        Align(
          alignment: Alignment.topLeft,
          child: _showVehiculeList(val),
        ),
      );
      if (idList != draggable.nbLists - 1) {
        vehiculesComponents.add(SizedBox(
          height: 25,
        ));
      }
      idList++;
    });

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 4,
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                    color:
                        draggable.deployedIsDragging ? ColorRessources.draggableColor : null,
                    borderRadius: BorderRadius.circular(12)),
                padding: EdgeInsets.only(top: 3),
                child: Column(
                  key: draggable.availableVehiculesKey,
                  children: [
                    ...vehiculesComponents,
                  ],
                ),
              ),
              _dragTargetToAvailable(),
            ],
          ),
        ),
        VerticalDivider(
          width: 10,
        ),
        Expanded(
          flex: 3,
          child: Stack(
            children: [
              Container(
                height: (draggable.availableVehiculeWidgetSize != null &&
                        draggable.availableVehiculeWidgetSize.height > 0)
                    ? draggable.availableVehiculeWidgetSize.height
                    : 125,
                decoration: BoxDecoration(
                    color:
                        draggable.availableIsDragging ? ColorRessources.draggableColor : null,
                    borderRadius: BorderRadius.circular(12)),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text(
                            FlutterI18n.translate(
                                draggable.context, "deployedVehicule"),
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        _showClearMeans(),
                      ],
                    ),
                    _showDeployedVehicules(),
                  ],
                ),
              ),
              _dragTargetToDeployable(),
            ],
          ),
        ),
      ],
    );
  }

  Container _showVehiculeList(List<Vehicule> vehicules) {
    return Container(
      height: 120,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        itemCount: vehicules.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: LongPressDraggable(
              child: buildCard(
                  '${vehicules[index].name}',
                  convertCategoryToColor(
                      EnumToString.parse(vehicules[index].category)),
                  120,
                  100),
              feedback: buildCard(
                  '${vehicules[index].name}',
                  convertCategoryToColor(
                      EnumToString.parse(vehicules[index].category)),
                  120,
                  100),
              childWhenDragging: buildCard('', ColorRessources.greyColor, 120, 100),
              onDragStarted: () {
                draggable.updateCurrentDragItemInfo(vehicules, index);
                draggable.availableIsDragging = true;
                draggable.refresh();
              },
              onDragEnd: (details) {
                draggable.availableIsDragging = false;
                draggable.refresh();
              },
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
    );
  }

  Container _showDeployedVehicules() {
    return Container(
      height: (draggable.availableVehiculeWidgetSize != null &&
              draggable.availableVehiculeWidgetSize.height > 0)
          ? draggable.availableVehiculeWidgetSize.height - 25
          : 100,
      decoration: BoxDecoration(
          color: draggable.availableIsDragging ? ColorRessources.draggableColor : null,
          borderRadius: BorderRadius.circular(12)),
      child: ListView.separated(
        physics: ScrollPhysics(),
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: draggable.deployedVehicules.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 50,
            child: LongPressDraggable(
              child: buildCard(
                  '${draggable.deployedVehicules[index].name}',
                  convertCategoryToColor(EnumToString.parse(
                      draggable.deployedVehicules[index].category)),
                  120,
                  50),
              feedback: buildCard(
                  '${draggable.deployedVehicules[index].name}',
                  convertCategoryToColor(EnumToString.parse(
                      draggable.deployedVehicules[index].category)),
                  50,
                  200),
              childWhenDragging: buildCard('', ColorRessources.greyColor, 120, 100),
              onDragStarted: () {
                draggable.updateCurrentDragItemInfo(
                    draggable.deployedVehicules, index);
                draggable.deployedIsDragging = true;
                draggable.refresh();
                draggable.refreshWidget();
              },
              onDragEnd: (details) {
                draggable.deployedIsDragging = false;
                draggable.refresh();
              },
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
    );
  }

  DragTarget _dragTargetToDeployable() {
    return DragTarget(
      builder: (BuildContext context, List candidateData, List rejectedData) {
        return Container(
          height: (draggable.availableVehiculeWidgetSize != null &&
                  draggable.availableVehiculeWidgetSize.height > 0)
              ? draggable.availableVehiculeWidgetSize.height
              : 100,
        );
      },
      onWillAccept: (data) =>
          draggable.currentList.isNotEmpty &&
          !draggable.deployedVehicules.contains(
              draggable.currentList[draggable.currentDraggableItemIndex]),
      onAccept: (data) {
        final Vehicule newDeployeditem =
            draggable.currentList.removeAt(draggable.currentDraggableItemIndex);
        draggable.deployedVehicules
            .insert(draggable.deployedVehicules.length, newDeployeditem);
        draggable.currentList = [];
        draggable.currentDraggableItemIndex = 0;
        draggable.refreshWidget();
        if (draggable.popUpCall) draggable.validateVehicule();
        draggable.refresh();
      },
    );
  }

  DragTarget _dragTargetToAvailable() {
    return DragTarget(
      builder: (BuildContext context, List candidateData, List rejectedData) {
        return Container(
          height: (draggable.availableVehiculeWidgetSize != null &&
                  draggable.availableVehiculeWidgetSize.height > 0)
              ? draggable.availableVehiculeWidgetSize.height
              : 0,
        );
      },
      onWillAccept: (data) {
        if (draggable.currentList.isNotEmpty) {
          String idList = EnumToString.parse(
              draggable.currentList[draggable.currentDraggableItemIndex].type);
          Vehicule vehiculeMean =
              draggable.currentList[draggable.currentDraggableItemIndex];
          return !draggable.availableVehicules[idList].contains(vehiculeMean);
        } else {
          return false;
        }
      },
      onAccept: (data) {
        final Vehicule newDeployeditem =
            draggable.currentList.removeAt(draggable.currentDraggableItemIndex);
        draggable.availableVehicules[EnumToString.parse(newDeployeditem.type)]
            .add(newDeployeditem);
        draggable.currentList = [];
        draggable.currentDraggableItemIndex = 0;
        draggable.refreshWidget();
        draggable.refresh();
      },
    );
  }

  Container buildCard(String title, Color color, double height, double width) {
    return Container(
      height: height,
      width: width,
      child: Card(
        color: color,
        child: Center(
          child: Text(
            title,
            overflow: TextOverflow.fade,
            style: TextStyle(
              fontSize: 18,
              color: ColorRessources.blackColor,
            ),
          ),
        ),
      ),
    );
  }

  InkWell _showClearMeans() {
    return InkWell(
      child: Icon(Icons.delete, color: ColorRessources.accentColor),
      onTap: () {
        draggable.clearDeployableMeans();
        draggable.refreshWidget();
        draggable.refresh();
      },
    );
  }
}
