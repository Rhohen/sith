import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:latlong/latlong.dart';
import 'package:mapbox_search/mapbox_search.dart';
import 'package:sith/components/intervention/intervention.component.dart';
import 'package:sith/components/intervention/intervention.service.dart';
import 'package:sith/components/intervention/vehicules.service.dart';
import 'package:sith/components/means/means.service.dart';
import 'package:sith/domain/enum/location.model.dart' as LocationEnum;
import 'package:sith/domain/intervention.model.dart';
import 'package:sith/domain/means.model.dart';
import 'package:sith/domain/vehicule.model.dart';

import 'draggable/draggable.component.dart';
import 'form.view.dart';

class InterventionFormPage extends StatefulWidget {
  static const routeName = '/InterventionFormPage';

  final Map<String, List<Vehicule>> vehicules;

  InterventionFormPage(this.vehicules);

  @override
  State<StatefulWidget> createState() =>
      new InterventionFormPageState(vehicules);
}

class InterventionFormPageState extends State<InterventionFormPage> {
  final String apikey =
      "pk.eyJ1IjoiaHVndWVzbGIiLCJhIjoiY2s3bmVzc3Q5MDF1YjNtbjZtbDk0MHJzbSJ9.4jg-JD9dLmOHMGAUmrl20A";
  final formKey = new GlobalKey<FormState>();
  final addressController = TextEditingController();
  final TextEditingController textEditingController = TextEditingController();
  DraggableComponentState draggableComponent;
  bool isAddressMapBoxEnable = true;
  String errorMessage;
  Intervention intervention;
  String latitude;
  String longitude;
  TextEditingController contactController;
  TextEditingController labelController;
  List<MapBoxPlace> placePredictions = [];
  AnimationController animationController;

  bool isLoading;
  TextEditingController latitudeController;
  TextEditingController longitudeController;

  Map<String, List<Vehicule>> vehicules;

  InterventionFormPageState(this.vehicules);

  @override
  void initState() {
    errorMessage = "";
    intervention = Intervention("", "", null, "", "", null, false);
    latitude = "";
    longitude = "";
    isLoading = false;
    latitudeController = new TextEditingController();
    longitudeController = new TextEditingController();
    contactController = new TextEditingController();
    labelController = new TextEditingController();
    super.initState();
  }

  void refreshWidget() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return new InterventionFormView(this, context).buildView();
  }

  bool _validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      isLoading = false;
      return false;
    }
  }

  void validateAndSubmit() async {
    setState(() {
      errorMessage = "";
      isLoading = true;
    });
    if (_validateAndSave()) {
      intervention.coordinate =
          LatLng(double.parse(latitude), double.parse(longitude));
      Means mean;
      DateTime date;
      DocumentReference documentReference;
      InterventionService()
          .insertIntervention(intervention)
          .then((interventionUid) async {
        intervention.uid = interventionUid;
        if (draggableComponent != null) {
          List<Vehicule> deployedVehicules =
              draggableComponent.getState().deployedVehicules;
              deployedVehicules.forEach((vehicule) => {
                date = DateTime.now(),
                mean = Means(
                  null,
                  vehicule.name,
                  vehicule.type,
                  date,
                  date,
                  null,
                  null,
                  LocationEnum.Location.CRM,
                  vehicule.category,
                  true,
                  interventionUid,
                ),
                documentReference = MeansService().createMean(interventionUid),
                MeansService().insertMean(documentReference, mean),
                vehicule.isAvailable = false,
                vehicule.idIntervention = interventionUid,
                VehiculesService().replaceVehicule(vehicule),
              });
        }
      }).then((onValue) {
        Navigator.popAndPushNamed(context, InterventionPage.routeName,
            arguments: intervention);
      });
    }
  }

  void notifyParent(DraggableComponentState componentState) {
    draggableComponent = componentState;
  }

  void resetForm() {
    formKey.currentState.reset();
    errorMessage = "";
  }

  bool validateContact(String value) {
    Pattern pattern = '^(\\+33|0)[1-9]([0-9]{2}){4}|\$';
    RegExp regex = RegExp(pattern);
    return regex.hasMatch(value);
  }

  bool validateLatitude(String value) {
    Pattern pattern = '^(-)?[1-9]?\\d\\.\\d+\$';
    RegExp regex = RegExp(pattern);
    return regex.hasMatch(value);
  }

  bool validateLongitude(String value) {
    Pattern pattern = '^(-)?[1-9]?\\d\\.\\d+\$';
    RegExp regex = RegExp(pattern);
    return regex.hasMatch(value);
  }

  void retrieveAddressFromLatLng(LatLng latLng) {
    var reverseGeoCoding = ReverseGeoCoding(
      apiKey: apikey,
      limit: 1,
    );

    reverseGeoCoding
        .getAddress(Location(lat: latLng.latitude, lng: latLng.longitude))
        .then((feature) {
      if (feature.isNotEmpty) {
        String addressName = feature.first.placeName;
        addressController.text = addressName;
        intervention.address = addressName;
        textEditingController.text = addressName;
      } else {
        addressController.text = "";
        intervention.address = "";
        textEditingController.text = "Aucune adresse trouvée";
        isAddressMapBoxEnable = false;
      }
      FocusScope.of(context).requestFocus(new FocusNode());
      setState(() {});
    });
  }
}
