import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/means/means.service.dart';
import 'package:sith/domain/intervention.model.dart';

import '../intervention.service.dart';
import '../vehicules.service.dart';
import 'list.view.dart';

// ignore: must_be_immutable
class InterventionsListPage extends StatefulWidget {
  static const routeName = '/InterventionsListPage';
  bool _isCodis;

  InterventionsListPage(this._isCodis);

  @override
  State<StatefulWidget> createState() =>
      new InterventionsListPageState(this._isCodis);
}

class InterventionsListPageState extends State<InterventionsListPage> {
  bool isCodis;

  InterventionsListPageState(this.isCodis);

  @override
  void initState() {
    super.initState();
  }

  void refreshWidget(){
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return new InterventionsListView(this, context).buildView();
  }

  Stream<QuerySnapshot> getInterventionSnapshots() {
    return InterventionService().getOpenedInterventionsSnapshots();
  }

  Future finishIntervention(Intervention intervention) async {
    await VehiculesService().releaseVehicules(intervention.uid);
    await MeansService().releaseMeans(intervention.uid);
    await MeansService().deleteMeansDemandByInterventionId(intervention.uid);
    intervention.closed = true;
    return InterventionService().updateIntervention(intervention);
  }
}