import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/intervention.model.dart';

import '../intervention.component.dart';
import 'list.component.dart';

class InterventionCard extends StatelessWidget {
  final Intervention _intervention;
  final InterventionsListPageState _interventionsListPage;
  final BuildContext _context;

  InterventionCard(
      this._intervention, this._interventionsListPage, this._context);

  @override
  Widget build(BuildContext context) {
    List actions = [
      SlideAction(
        onTap: () => _interventionsListPage.finishIntervention(_intervention),
        child: Container(
          decoration: BoxDecoration(
            color: ColorRessources.orangeColor,
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.priority_high, color: ColorRessources.whiteColor),
                Text(FlutterI18n.translate(context, "close_intervention"),
                    style: TextStyle(color: ColorRessources.whiteColor)),
              ],
            ),
          ),
        ),
      )
    ];

    return Slidable(
      actionPane: SlidableScrollActionPane(),
      enabled: true,
      closeOnScroll: false,
      secondaryActions:
          _interventionsListPage.isCodis ? <Widget>[...actions] : <Widget>[],
      key: ObjectKey(0),
      child: ListTile(
        title: Text(
          _intervention.name,
          overflow: TextOverflow.ellipsis,
        ),
        subtitle: Text(EnumToString.parse(_intervention.codeSinistre)),
        trailing: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          return _displayAddress(_intervention, constraints.maxWidth / 2);
        }),
        onTap: () {
          Navigator.pushNamed(_context, InterventionPage.routeName,
              arguments: _intervention);
        },
      ),
    );
  }

  Container _displayAddress(Intervention intervention, double maxWidth) {
    String textValue = "";
    if (intervention.address.isNotEmpty) {
      textValue = intervention.address;
    } else if (intervention.coordinate != null) {
      textValue = intervention.coordinate.latitude.toStringAsFixed(3) +
          ".." +
          " ; " +
          intervention.coordinate.longitude.toStringAsFixed(3) +
          "..";
    }
    return Container(
      width: maxWidth,
      child: Align(
        alignment: Alignment.centerRight,
        child: Text(
          textValue,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
