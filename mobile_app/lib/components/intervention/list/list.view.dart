import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sith/domain/intervention.model.dart';

import 'intervention-card.view.dart';
import 'list.component.dart';

class InterventionsListView {
  InterventionsListPageState interventionsListPage;
  BuildContext context;

  InterventionsListView(this.interventionsListPage, this.context);

  StreamBuilder buildView() {
    return StreamBuilder<QuerySnapshot>(
      stream: interventionsListPage.getInterventionSnapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData)
          return Center(child: CircularProgressIndicator());
        return ListView(
            children: snapshot.data.documents.map((DocumentSnapshot document) {
              return Card(
                  child: InterventionCard(
                      Intervention.from(document), interventionsListPage,
                      context));
            }).toList());
      },
    );
  }
}
