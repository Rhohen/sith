import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sith/domain/means.model.dart';
import 'package:sith/domain/vehicule.model.dart';
import 'package:sith/services/firestore.service.dart';

class VehiculesService {
  static CollectionReference formCollectionReference;

  static final VehiculesService _singleton = initiate();

  factory VehiculesService() {
    return _singleton;
  }

  /// Constructeur privé interne
  VehiculesService._internal();

  static initiate() {
    formCollectionReference = FireConn().getPersistentVehiculesCollection();
    return VehiculesService._internal();
  }

  Future<QuerySnapshot> getAvailableVehicules() {
    return formCollectionReference
        .where('isAvailable', isEqualTo: true)
        .getDocuments();
  }

  Future<QuerySnapshot> getVehicules(String type) {
    return formCollectionReference
        .where('isAvailable', isEqualTo: true)
        .where('type', isEqualTo: type)
        .getDocuments();
  }

  Future replaceVehicule(Vehicule vehicule) {
    return formCollectionReference
        .document(vehicule.uid)
        .setData(vehicule.toJson());
  }

  releaseVehicules(String idIntervention) async {
    Vehicule vehicule;
    await formCollectionReference
        .where('idIntervention', isEqualTo: idIntervention)
        .getDocuments()
        .then((querySnapshot) =>
    {
      querySnapshot.documents.forEach((document) =>
      {
        vehicule =
            Vehicule.from(document.documentID, document.data),
        vehicule.isAvailable = true,
        vehicule.idIntervention = null,
        replaceVehicule(vehicule)
      })
    });
  }

  releaseOneMeans(Means means) async {
    Vehicule vehicule;
    await formCollectionReference
      .where('idIntervention', isEqualTo: means.idIntervention)
      .where('name', isEqualTo: means.name)
      .getDocuments()
      .then((querySnapshot) =>
        {
          querySnapshot.documents.forEach((document) =>
          {
            vehicule = Vehicule.from(document.documentID, document.data),
            vehicule.isAvailable = true,
            vehicule.idIntervention = null,
            replaceVehicule(vehicule)
          })
        }
      );
  }
}
