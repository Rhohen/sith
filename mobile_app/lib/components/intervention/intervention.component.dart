import 'package:flutter/material.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/domain/intervention.model.dart';

import 'intervention.view.dart';

class InterventionPage extends StatefulWidget {
  static const routeName = '/InterventionPage';

  final Intervention _intervention;

  InterventionPage(this._intervention);

  @override
  State<StatefulWidget> createState() => new InterventionPageState(this._intervention);
}

class InterventionPageState extends State<InterventionPage> {
  Intervention intervention;

  InterventionPageState(this.intervention);

  @override
  void initState() {
    super.initState();
  }

  void refreshWidget(){
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return new InterventionView(this, context).buildView();
  }
}