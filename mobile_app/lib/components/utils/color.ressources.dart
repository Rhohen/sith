import 'package:flutter/material.dart';

class ColorRessources {
  static const Color redColor = Colors.red;
  static const Color blueColor = Colors.blue;
  static const Color greenColor = Colors.green;
  static const Color orangeColor = Colors.orange;
  static const Color purpleColor = Colors.purple;
  static const Color greyColor = Colors.grey;

  static final Color primaryColor = Colors.grey[800];
  static final Color darkColor = Colors.grey[800];
  static final Color darkColorTransparent = Colors.black.withOpacity(0);
  static final Color accentColor = Colors.grey[600];
  static final Color grey500 = Colors.grey[500];
  static final Color splashColor = Colors.grey[400];
  static final Color draggableColor = Colors.grey[300];
  static final Color backgroundSidebar = Colors.grey[200];
  static final Color grey300 = Colors.grey[300];
  static const Color blackColor = Colors.black;
  static const Color exitToAppColor = Colors.black87;
  static const Color whiteColor = Colors.white;
  static const Color transparentColor = Colors.transparent;
  static final Color addMeansErrorColor = Colors.red[300];
  static final Color switchActiveTrack = Colors.orangeAccent;
  static final Color switchActiveColor = Colors.deepOrangeAccent;

  static final Color addTravelsPointsErrorColors = Colors.red[300];

  static const String red = 'red';
  static const String blue = 'blue';
  static const String green = 'green';
  static const String orange = 'orange';
  static const String purple = 'purple';
  static const String black = 'black';
  static const String grey = 'grey';

}