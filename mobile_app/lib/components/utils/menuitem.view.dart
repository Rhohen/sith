import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/utils/color.ressources.dart';

class MenuItems {
  static const String SignOut = 'Log Out';
  static const String ChangeLang = 'Language';

  static const Map<String, Icon> choices = const {
    SignOut: Icon(
      Icons.exit_to_app,
      color: ColorRessources.exitToAppColor,
    ),
    ChangeLang: Icon(
      Icons.language,
      color: ColorRessources.exitToAppColor,
    ),
  };

  static Widget rowItem(String itemName, Icon icon) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Spacer(),
          icon,
          Spacer(flex: 2),
          Text(itemName),
          Spacer(),
        ],
      ),
    );
  }
}
