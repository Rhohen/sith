
import 'package:flutter/material.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/enum/category.model.dart';

Color convertStringToColor(String color){
  switch(color) {
    case ColorRessources.green:
      return ColorRessources.greenColor;
    case ColorRessources.red:
      return ColorRessources.redColor;
    case ColorRessources.blue:
      return ColorRessources.blueColor;
    case ColorRessources.orange:
      return ColorRessources.orangeColor;
    case ColorRessources.purple:
      return ColorRessources.purpleColor;
    case ColorRessources.black:
      return ColorRessources.blackColor;
    default:
      return ColorRessources.greyColor;
  }
}

Color convertCategoryEnumToColor(Category category){
  if(category == Category.SECOURS){
    return ColorRessources.greenColor;
  }
  else if(category == Category.EAU){
    return ColorRessources.blueColor;
  }
  else if(category == Category.RISQUES){
    return ColorRessources.orangeColor;
  }
  else if(category == Category.INCENDIE){
    return ColorRessources.redColor;
  }
  else if(category == Category.COMMANDEMENT){
    return ColorRessources.purpleColor;
  }
  else if(category == Category.DEFAUT){
    return ColorRessources.blackColor;
  }
  else return ColorRessources.greyColor;
}

Color convertCategoryToColor(String category){
  switch(category) {
    case 'SECOURS':
      return ColorRessources.greenColor;
    case 'INCENDIE':
      return ColorRessources.redColor;
    case 'EAU':
      return ColorRessources.blueColor;
    case 'RISQUES':
      return ColorRessources.orangeColor;
    case 'COMMANDEMENT':
      return ColorRessources.purpleColor;
    case 'DEFAUT':
      return ColorRessources.blackColor;
    default:
      return ColorRessources.greyColor;
  }
}

Category convertColorToCategory(Color color){
  if(color == ColorRessources.greenColor){
    return Category.SECOURS;
  }
  else if(color == ColorRessources.blueColor){
    return Category.EAU;
  }
  else if(color == ColorRessources.orangeColor){
    return Category.RISQUES;
  }
  else if(color == ColorRessources.redColor){
    return Category.INCENDIE;
  }
  else if(color == ColorRessources.purpleColor){
    return Category.COMMANDEMENT;
  }
  else{
    return Category.DEFAUT;
  }
}


String convertColorToString(Color color) {
  if (color == ColorRessources.redColor) {
    return ColorRessources.red;
  }
  else if (color == ColorRessources.greenColor) {
    return ColorRessources.green;
  }
  else if (color == ColorRessources.blueColor) {
    return ColorRessources.blue;
  }
  else if (color == ColorRessources.orangeColor) {
    return ColorRessources.orange;
  }
  else if (color == ColorRessources.purpleColor) {
    return ColorRessources.purple;
  }
  else if (color == ColorRessources.blackColor) {
    return ColorRessources.black;
  }
  else {
    return ColorRessources.grey;
  }
}
