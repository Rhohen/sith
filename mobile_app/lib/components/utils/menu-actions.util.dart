import 'package:flutter/cupertino.dart';
import 'package:sith/components/utils/menuitem.view.dart';
import 'package:sith/services/user.service.dart';

class MenuActions {
  static void choiceAction(String choice, BuildContext context, Function action) {
    if (choice == MenuItems.SignOut) {
      UserService().logout(context);
    }
    if (choice == MenuItems.ChangeLang) {
      action();
    }
  }
}
