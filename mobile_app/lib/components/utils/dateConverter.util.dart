import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateConverter {
  static String formatDateTime(DateTime date) {
    return DateFormat.Hm().format(date);
  }

  static String formatDateFromFileName(String fileName) {
    List<String> substrings = fileName.split("-");
    int year = int.parse(substrings[1].substring(0,4));
    int month = int.parse(substrings[1].substring(4,6));
    int day = int.parse(substrings[1].substring(6,8));
    int hour = int.parse(substrings[2].substring(0,2));
    int minute = int.parse(substrings[2].substring(2,4));
    int second = int.parse(substrings[2].substring(4,6));
    DateTime date = DateTime(year, month, day, hour, minute, second);
    return DateFormat.yMd().format(date) + " - " + DateFormat.Hms().format(date);
  }

  static TimeOfDay dateToTime(DateTime date) {
    return new TimeOfDay.fromDateTime(date);
  }

  static DateTime timeToDate(TimeOfDay time) {
    final now = new DateTime.now();
    return new DateTime(now.year, now.month, now.day, time.hour, time.minute);
  }
}