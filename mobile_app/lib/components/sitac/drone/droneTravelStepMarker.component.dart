import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:sith/components/sitac/batteryIndicator/batteryIndicator.dart';
import 'package:sith/components/sitac/bottomSheet/bottomSheet.component.dart';
import 'package:sith/components/sitac/map/map.resources.dart';
import '../sitac.component.dart';

class TargetStepMarker {
  LatLng point;
  BuildContext context;
  String interventionUid;
  String poiDroneUid;
  String imagePath;
  static bool isPopupVisible;
  int nbPictures;

  TargetStepMarker(this.point, this.context, this.interventionUid,
      this.poiDroneUid, this.imagePath, this.nbPictures) {
    isPopupVisible = false;
  }

  Marker getMarker() {
    return Marker(
      anchorPos: (imagePath == MapResources.POI_DRONE_DRAW_PATH)
          ? AnchorPos.exactly(Anchor(29, 0))
          : AnchorPos.align(AnchorAlign.center),
      height: (MapResources.POI_DRONE_DRAW_PATH != this.imagePath) ? 60 : 30,
      width: (MapResources.POI_DRONE_DRAW_PATH != this.imagePath) ? 60 : 30,
      point: point,
      builder: (ctx) => GestureDetector(
        onTap: () {
          if (null != this.poiDroneUid && this.poiDroneUid.isNotEmpty) {
            if (isPopupVisible) {
              isPopupVisible = false;
              Navigator.popUntil(context, ModalRoute.withName(Sitac.routeName));
            } else {
              isPopupVisible = true;
              showModalBottomSheet(
                isScrollControlled: true,
                context: context,
                builder: (context) =>
                    BottomSheetSitac(interventionUid, poiDroneUid),
              );
            }
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: Image(
                image: AssetImage(imagePath),
                color: Colors.black,
              ),
            ),
            SizedBox(
              height: 2,
            ),
            (MapResources.POI_DRONE_DRAW_PATH != this.imagePath)
                ? Flexible(
                    child: Text(
                      nbPictures.toString(),
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        backgroundColor: Colors.white,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  Marker getDrone(int battery) {
    return Marker(
      height: 75,
      width: 50,
      point: LatLng(point.latitude, point.longitude),
      builder: (ctx) => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image(
            image: AssetImage(MapResources.DRONE_PATH),
            color: Colors.red[800],
          ),
          SizedBox(
            height: 5,
          ),
          BatteryIndicator(
            batteryLv: battery,
            style: BatteryIndicatorStyle.skeumorphism,
            showPercentNum: false,
            size: 18.0,
            ratio: 3.0,
          ),
        ],
      ),
    );
  }
}
