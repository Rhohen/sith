import 'package:flutter/cupertino.dart';

class FloatingElement {
  IconData iconData;
  String heroTag;
  Color color;
  bool selected;

  FloatingElement(this.iconData, this.heroTag, this.color, this.selected);
}
