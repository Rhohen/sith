import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:latlong/latlong.dart';
import 'package:sith/components/means/means.service.dart';
import 'package:sith/components/sitac/map/map.resources.dart';
import 'package:sith/domain/means.model.dart';
import 'package:sith/domain/poiIcon.model.dart';
import 'package:sith/services/firestore.service.dart';

class PoiIconService {
  static CollectionReference _poiIconsCollectionReference;

  static final PoiIconService _singleton = initiate();

  static String targetMarkerType;
  static String targetAssetPath;
  static String targetColor;

  factory PoiIconService() {
    return _singleton;
  }

  /// Constructeur privé interne
  PoiIconService._internal();

  static initiate() {
    _poiIconsCollectionReference = FireConn().getInterventionsCollection();
    return PoiIconService._internal();
  }

  CollectionReference getPoiIconsCollection(String interventionId) {
    return _poiIconsCollectionReference.document(interventionId).collection("poiIcons");
  }

  DocumentReference getPoiIconsDocument(String interventionId) {
    return getPoiIconsCollection(interventionId).document();
  }

  Future<QuerySnapshot> getPoiIcons(String interventionId) {
    return getPoiIconsCollection(interventionId).getDocuments();
  }

  Stream<QuerySnapshot> gePoiIconStream(String interventionUid) {
    return getPoiIconsCollection(interventionUid).snapshots();
  }

  // tout les means avec un id
  List<PoiIcon> getMeansInCRM(List<PoiIcon> list) {
    List<PoiIcon> means = [];
    for(PoiIcon icon in list){
      if (icon.idMeans != null && !icon.isOnMap) {
        means.add(icon);
      }
    }
    return means;
  }

  Future insertPoiIcon(DocumentReference document, PoiIcon icon) {
    return document.setData(icon.toJson());
  }

  Future updatePoiIcon(PoiIcon icon) {
    return getPoiIconsCollection(icon.idIntervention).document(icon.uid).updateData(icon.toJson());
  }

  Future deleteIcon(PoiIcon icon) {
    return getPoiIconsCollection(icon.idIntervention).document(icon.uid).delete();
  }

  Future<PoiIcon> getPoiIconOfMeans(String interventionId, Means means) async {
    PoiIcon icon;
    await getPoiIcons(interventionId).then((iconsDocs){
      iconsDocs.documents.forEach((i) {
        if(PoiIcon.from(i).idMeans == means.uid){
          icon = PoiIcon.from(i);
        }
      });
      if(icon == null){ // if null, create a new Icon
        insertPoiIcon(getPoiIconsDocument(interventionId), getPoiIconFromMeans(means)).then((i) {
          icon = PoiIcon.from(i);
        });
      }
    });
    return icon;
  }

  void updatePoiIconFromMeans(Means means) async {
    PoiIcon icon = await getPoiIconOfMeans(means.idIntervention, means);
    icon.category = means.category;
    icon.label = defineLabel(means);
    updatePoiIcon(icon);
  }


  // TODO No more used. Can be still useful later
  void synchronizeMeansAndPoiIcon(String interventionUid, LatLng coordinates){
    List<Means> means = [];
    List<PoiIcon> icons = [];
    MeansService().fetchAllMeans(interventionUid).then((meansDocs) { // Fetch all Means
      PoiIconService().getPoiIcons(interventionUid).then((iconsDocs) { // Fetch all PoiIcons
        meansDocs.documents.forEach((m) {
          means.add(Means.from(m.documentID, m.data));
        });
        if(iconsDocs.documents != null){
          iconsDocs.documents.forEach((i) {
            icons.add(PoiIcon.from(i));
          });
          // Remove means which already have an icon
          int i = 0;
          while(i < means.length){
            if(_iconsContainsMeans(icons, means[i].uid)){
              means.removeAt(i);
            }
            else i++;
          }
        }
        means.forEach((m) { //Synchronize
          if(m.isValid == null || m.isValid) {
            PoiIconService().insertPoiIcon(
              PoiIconService().getPoiIconsDocument(interventionUid),
              getPoiIconFromMeans(m, coordinates: coordinates),
            );
          }
        });
      });
    });
  }

  bool _iconsContainsMeans(List<PoiIcon> icons, String id){
    bool ret = false;
    icons.forEach((i) {
      if(i.idMeans == id){
        ret = true;
      }
    });
    return ret;
  }

  String defineLabel(Means means){
    return (means.name == null ? EnumToString.parse(means.type) : means.name);
  }

  PoiIcon getPoiIconFromMeans(Means means, {LatLng coordinates}){
    if (coordinates == null) coordinates = LatLng(MapResources.initialLat, MapResources.initialLng);
    String label = defineLabel(means);
    return PoiIcon(
      null,
      means.idIntervention,
      means.uid,
      coordinates,
      means.category,
      MapResources.assetPath + 'moyen-prevu' +
        MapResources.assetExtention,
      means.type,
      label,
      "",
      false,
      false
    );
  }

}