import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/sitac/map/map.service.dart';
import 'package:sith/components/sitac/sidebarSITAC/sidebarSITAC.component.dart';
import 'package:sith/components/sitac/sitac.view.dart';
import 'package:sith/domain/intervention.model.dart';
import 'package:sith/domain/poiDrone.model.dart';
import 'package:sith/domain/poiIcon.model.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'drone/floatingElement.dart';
import 'map/map.component.dart';
import 'map/map.resources.dart';
import 'map/map.service.dart';

class Sitac extends StatefulWidget {
  static const routeName = '/SITAC';

  Intervention intervention;

  Sitac(this.intervention){
    MapService().resetTargetParameters();
  }

  @override
  State<StatefulWidget> createState() => new SitacState(this.intervention);
}

class SitacState extends State<Sitac> {
  final GlobalKey<MapPageState> mapPageKey = GlobalKey();
  final GlobalKey<FabCircularMenuState> fabCircularMenuKey = GlobalKey();

  MapPage map;
  Flushbar flushbar;
  AlertDialog alertDialog;
  String errorMessage;
  SidebarSITAC sidebarSITAC;
  List<PoiIcon> icons = [];
  List<PoiDrone> iconsDrone = [];
  bool show = false;
  final Intervention intervention;
  YoutubePlayerController controller;
  IconData icon;


  @override
  void initState() {
    MapService.targetMarkerType = MapResources.NONE;
    flushbar = Flushbar();
    alertDialog = AlertDialog();
    controller = YoutubePlayerController(
      initialVideoId: YoutubePlayer.convertUrlToId('https://www.youtube.com/watch?v=MyaHvSwiFuo'),
      flags: YoutubePlayerFlags(
        autoPlay: true,
      ),
    );
    super.initState();
  }

  SitacState(this.intervention) {
    map = MapPage(intervention, icons, this.mapPageKey);
    sidebarSITAC = SidebarSITAC(intervention.uid, icons);
  }

  void setIconFromDb(AsyncSnapshot<QuerySnapshot> snapshotDrone,
      AsyncSnapshot<QuerySnapshot> snapshotIcon) {
    if (snapshotDrone.data != null) {
      iconsDrone = [];
      snapshotDrone.data.documents.forEach((document) {
        iconsDrone.add(PoiDrone.from(document));
      });
      iconsDrone.sort((a, b) => a.step.compareTo(b.step));
      map.setIconsDrone(iconsDrone);
    }
    if (snapshotIcon.data != null) {
      icons = [];
      snapshotIcon.data.documents.forEach((document) {
        icons.add(PoiIcon.from(document));
      });
      map.setIcons(icons);
      sidebarSITAC.setIcons(icons);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SitacView(this).buildView(context);
  }

  List<FloatingElement> elems = [
    FloatingElement(
        Icons.gesture, MapResources.NON_CLOSED, Colors.white, false),
    FloatingElement(
        Icons.crop_square, MapResources.CLOSED, Colors.white, false),
    FloatingElement(Icons.all_out, MapResources.ZONE_INC, Colors.white, false),
    FloatingElement(Icons.videocam, MapResources.VIDEO, Colors.white, false),
  ];

  bool isFabMenuIconSelected() {
    for (FloatingElement elem in elems) {
      if (elem.selected) return true;
    }
    return false;
  }

  deselectFabMenuIcons() {
    for (FloatingElement elem in elems) {
      elem.selected = false;
    }
    MapService.targetMarkerType = MapResources.NONE;
  }

  refreshWidget() {
    if (!mounted) return;
    setState(() {
      sidebarSITAC = SidebarSITAC(intervention.uid, icons);
    });
  }
}
