import 'package:flutter/material.dart';
import 'package:sith/components/sitac/sitac.ressource.dart';

import 'meansRepresentation.component.dart';

class MeansRepresentationView {
  MeansRepresentationState meansRepresentation;

  MeansRepresentationView(_meansRepresentation) {
    meansRepresentation = _meansRepresentation;
  }

  Widget buildView() {
    return Center(
      child: Container(
        child: Stack(
          alignment: Alignment(0.0, 0.4),
          children: <Widget>[
            Center(
              child: new Image.asset(
                meansRepresentation.getAsset(),
                width: ((meansRepresentation.widget.means != null) && (meansRepresentation.widget.means.poiIcon == meansRepresentation.widget.poiIcon))
                    ? meansRepresentation.widget.width + SITACRessource.iconIncreaseWidth : meansRepresentation.widget.width,
                color: meansRepresentation.widget.color,
                fit: BoxFit.fill,
              ),
            ),
            Align(
              child: Text(meansRepresentation.widget.poiIcon.label,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: meansRepresentation.getTextSize(),
                      color: meansRepresentation.widget.color,
                  )
              ),
            ),
          ],
        ),
      )
    );
  }
}
