import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/means/means.service.dart';
import 'package:sith/components/sitac/map/map.resources.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/colorUtils.dart';
import 'package:sith/domain/means.model.dart';
import 'package:sith/domain/poiIcon.model.dart';

import '../poiIcons.service.dart';
import '../sitac.ressource.dart';
import 'meansRepresentation.view.dart';

// ignore: must_be_immutable
class MeansRepresentation extends StatefulWidget {

  final PoiIcon poiIcon ;
  Color color;
  String assetImage;
  double width;
  String type;
  MeansRepresentation means;
  bool isSwitched;

  MeansRepresentation(this.width, this.poiIcon, {this.means}){
    this.color = convertCategoryEnumToColor(poiIcon.category);
    this.assetImage = poiIcon.iconPath;
    this.type = EnumToString.parse(poiIcon.type);
  }

  Widget switchButton(){
    Means meansTmp;
    return Switch(
      value: poiIcon.isActive,
      onChanged: (value) {
        poiIcon.isActive = !poiIcon.isActive;
        MeansService().getMean(poiIcon.idIntervention, poiIcon.idMeans).then(
          (m) {
            meansTmp = Means.from(poiIcon.idMeans, m.data);
            meansTmp.engageHour = (meansTmp.engageHour == null && poiIcon.isActive ? DateTime.now() : meansTmp.engageHour);
            MeansService().updateMean(poiIcon.idIntervention, meansTmp).then((onValue) {
              PoiIconService().updatePoiIcon(poiIcon);
            });
          }
        );
      },
      activeTrackColor: ColorRessources.switchActiveTrack,
      activeColor: ColorRessources.switchActiveColor,
    );
  }

  @override
  MeansRepresentationState createState() => new MeansRepresentationState();
}

class MeansRepresentationState extends State<MeansRepresentation> {

  Color color ;

  MeansRepresentationState();

  @override
  void initState() {
    super.initState();
    setAsset();
  }

  void refreshWidget(){
    if(!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return new MeansRepresentationView(this).buildView();
  }

  String getAsset(){
    if(widget.poiIcon.isActive){
      return MapResources.assetPath + "moyen-actif" + MapResources.assetExtention;
    }
    return MapResources.assetPath + "moyen-prevu" + MapResources.assetExtention;
  }

  void setAsset(){
    if(widget.poiIcon.isActive && widget.poiIcon.iconPath.contains('prevu')){
      widget.poiIcon.iconPath = MapResources.assetPath + "moyen-actif" + MapResources.assetExtention;
      PoiIconService().updatePoiIcon(widget.poiIcon);
      refreshWidget();
    }
  }

  double getTextSize(){
    if (widget.poiIcon.label.length < 7){
      return widget.width / SITACRessource.meansRepresentationTextSize ;
    }
    else if(widget.poiIcon.label.length < 8){
      return widget.width / (SITACRessource.meansRepresentationTextSize + 1) ;
    }
    else return widget.width / (SITACRessource.meansRepresentationTextSize + 2) ;
  }

  void changeColor(Color color) => setState(() => color = color);
}