import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:frino_icons/frino_icons.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:sith/components/sitac/map/map.resources.dart';
import 'package:sith/components/sitac/map/map.service.dart';
import 'package:sith/components/sitac/poiDrone.service.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/components/sitac/sitac.component.dart';
import 'package:sith/components/sitac/sitac.ressource.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import 'drone/floatingElement.dart';

class SitacView {
  SitacState sitacState;

  SitacView(this.sitacState);

  RawMaterialButton createRawMaterialButton(
      int index, String heroTag, FloatingElement floatingElement) {
    return RawMaterialButton(
      shape: CircleBorder(),
      elevation: floatingElement.selected ? 2 : 0,
      padding: EdgeInsets.all(10.0),
      constraints: BoxConstraints(),
      child: Icon(floatingElement.iconData, color: floatingElement.color),
      fillColor: floatingElement.selected
          ? ColorRessources.grey500
          : ColorRessources.primaryColor,
      onPressed: () {
        sitacState.mapPageKey.currentState.points.isNotEmpty
            ? _showAlertDialog(
                index,
                heroTag,
                FlutterI18n.translate(
                    sitacState.context, "travelStepAlertDialogTitle"),
                FlutterI18n.translate(
                    sitacState.context, "travelStepAlertDialogDesc"))
            : switchSelected(index, heroTag);
        sitacState.refreshWidget();
      },
    );
  }

  RawMaterialButton clearRawMaterialButton() {
    return RawMaterialButton(
      shape: CircleBorder(),
      elevation: 0,
      padding: EdgeInsets.all(10.0),
      constraints: BoxConstraints(),
      child: Icon(Icons.delete_forever, color: Colors.white),
      fillColor: ColorRessources.primaryColor,
      onPressed: () {
        sitacState.mapPageKey.currentState.clearCurrentTravelStepLayer();
        sitacState.refreshWidget();
      },
    );
  }

  switchSelected(int index, String heroTag) {
    for (int i = 0; i < sitacState.elems.length; i++) {
      if (i != index)
        sitacState.elems[i].selected = false;
      else
        sitacState.elems[i].selected = !sitacState.elems[i].selected;
    }
    if (sitacState.elems[index].selected)
      MapService.targetMarkerType = heroTag;
    else
      MapService.targetMarkerType = MapResources.NONE;
    sitacState.mapPageKey.currentState.clearAllTravelStep();
  }

  Widget buildView(BuildContext context) {
    List<RawMaterialButton> buttonsList = List<RawMaterialButton>();
    for (int index = 0; index < sitacState.elems.length; index++)
      buttonsList.add(createRawMaterialButton(
          index, sitacState.elems[index].heroTag, sitacState.elems[index]));
    buttonsList.add(clearRawMaterialButton());

    return Scaffold(
      resizeToAvoidBottomInset: false, // set it to false
      appBar: AppBar(
        title: Text(FlutterI18n.translate(context, "sitac_map"),
            style: TextStyle(fontSize: 20)),
        backgroundColor: ColorRessources.primaryColor,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: PoiDroneService().gePoiDroneStream(sitacState.intervention.uid),
        builder: (context, snapshotDrone) {
          return StreamBuilder<QuerySnapshot>(
            stream:
                PoiIconService().gePoiIconStream(sitacState.intervention.uid),
            builder: (context, snapshotIcon) {
              sitacState.setIconFromDb(snapshotDrone, snapshotIcon);
              return Stack(children: <Widget>[
                sitacState.map,
                Row(
                  children: <Widget>[
                    Flexible(fit: FlexFit.tight, child: SizedBox()),
                    Align(
                      alignment: Alignment.center,
                      child: getButton(MediaQuery.of(context).size.height),
                    ),
                    if (sitacState.show)
                      Container(
                        width: MediaQuery.of(context).size.width *
                            SITACRessource.sidebarSize,
                        child: sitacState.sidebarSITAC,
                      ),
                  ],
                ),
                Visibility(
                    visible: MapService.targetMarkerType == MapResources.VIDEO,
                    child: Center(child: _showVideo())),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Visibility(
                    visible: (MapService.targetMarkerType ==
                            MapResources.NON_CLOSED ||
                        MapService.targetMarkerType == MapResources.CLOSED ||
                        MapService.targetMarkerType == MapResources.ZONE_INC ||
                        MapService.targetMarkerType == MapResources.ZONE_EX),
                    child: _getTravelStepMenuOptions(),
                  ),
                ),
              ]);
            },
          );
        },
      ),
      floatingActionButton: FabCircularMenu(
        key: sitacState.fabCircularMenuKey,
        animationDuration: const Duration(milliseconds: 350),
        animationCurve: Curves.easeInOutCirc,
        ringColor: ColorRessources.primaryColor,
        ringDiameter: 283.0,
        ringWidth: 60,
        fabElevation: 2,
        fabOpenIcon: Icon(
          FrinoIcons.f_drone,
          color: Colors.white,
        ),
        fabCloseIcon: Icon(
          Icons.arrow_drop_down,
          color: Colors.white,
        ),
        fabSize: 55,
        fabColor: ColorRessources.primaryColor,
        fabMargin: EdgeInsets.only(right: 16, bottom: 16),
        children: buttonsList,
      ),
    );
  }

  Widget _getTravelStepMenuOptions() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: MapService.targetMarkerType == MapResources.ZONE_INC ||
              MapService.targetMarkerType == MapResources.ZONE_EX,
          child: Padding(
            padding: EdgeInsets.only(left: 10, bottom: 5),
            child: Row(
              children: [
                AbsorbPointer(
                  absorbing:
                      !(MapService.targetMarkerType == MapResources.ZONE_INC),
                  child: RawMaterialButton(
                      shape: CircleBorder(),
                      elevation: 2,
                      padding: EdgeInsets.all(10.0),
                      constraints: BoxConstraints(),
                      child: MapService.targetMarkerType ==
                              MapResources.ZONE_INC
                          ? Icon(Icons.panorama_fish_eye, color: Colors.white)
                          : Icon(Icons.not_interested, color: Colors.white),
                      fillColor:
                          MapService.targetMarkerType == MapResources.ZONE_INC
                              ? Colors.blue
                              : Colors.red,
                      onPressed: null),
                ),
                SizedBox(
                  width: 15,
                ),
                RawMaterialButton(
                    shape: CircleBorder(),
                    elevation: 2,
                    padding: EdgeInsets.all(10.0),
                    constraints: BoxConstraints(),
                    child: Icon(Icons.navigate_next, color: Colors.white),
                    fillColor: ColorRessources.primaryColor,
                    onPressed: () {
                      sitacState.mapPageKey.currentState.beginNextArea();
                      sitacState.refreshWidget();
                    }),
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              AbsorbPointer(
                absorbing:
                    MapService.targetMarkerType == MapResources.ZONE_INC ||
                        MapService.targetMarkerType == MapResources.ZONE_EX,
                child: RawMaterialButton(
                  shape: CircleBorder(),
                  elevation: 2,
                  padding: EdgeInsets.all(10.0),
                  constraints: BoxConstraints(),
                  child: Icon(Icons.undo, color: Colors.white),
                  fillColor: (MapService.targetMarkerType ==
                              MapResources.ZONE_INC ||
                          MapService.targetMarkerType == MapResources.ZONE_EX)
                      ? ColorRessources.grey500
                      : ColorRessources.primaryColor,
                  onPressed: () =>
                      sitacState.mapPageKey.currentState.undoTravelStep(),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              AbsorbPointer(
                absorbing:
                    MapService.targetMarkerType == MapResources.ZONE_INC ||
                        MapService.targetMarkerType == MapResources.ZONE_EX,
                child: RawMaterialButton(
                  shape: CircleBorder(),
                  elevation: 2,
                  padding: EdgeInsets.all(10.0),
                  constraints: BoxConstraints(),
                  child: Icon(Icons.redo, color: Colors.white),
                  fillColor: (MapService.targetMarkerType ==
                              MapResources.ZONE_INC ||
                          MapService.targetMarkerType == MapResources.ZONE_EX)
                      ? ColorRessources.grey500
                      : ColorRessources.primaryColor,
                  onPressed: () =>
                      sitacState.mapPageKey.currentState.redoTravelStep(),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              RawMaterialButton(
                shape: CircleBorder(),
                elevation: 2,
                padding: EdgeInsets.all(10.0),
                constraints: BoxConstraints(),
                child: Icon(Icons.delete, color: Colors.white),
                fillColor: ColorRessources.primaryColor,
                onPressed: () {
                  sitacState.mapPageKey.currentState.clearTravelStep();
                  sitacState.refreshWidget();
                },
              ),
              SizedBox(
                width: 15,
              ),
              RawMaterialButton(
                shape: CircleBorder(),
                elevation: 2,
                padding: EdgeInsets.all(10.0),
                constraints: BoxConstraints(),
                child: Icon(Icons.check, color: Colors.white),
                fillColor: ColorRessources.primaryColor,
                onPressed: () {
                  bool isValidated = false;
                  switch (MapService.targetMarkerType) {
                    case MapResources.NON_CLOSED:
                      isValidated = sitacState.mapPageKey.currentState
                          .validateTravelStepNonClosed();
                      sitacState.errorMessage =
                          "travelStepNumberErrorNonClosed";
                      break;
                    case MapResources.CLOSED:
                      isValidated = sitacState.mapPageKey.currentState
                          .validateTravelStepClosed();
                      sitacState.errorMessage = "travelStepNumberErrorClosed";
                      break;
                    case MapResources.ZONE_INC:
                    case MapResources.ZONE_EX:
                      isValidated = sitacState.mapPageKey.currentState
                          .validateTravelStepZone();
                      sitacState.errorMessage = "travelStepNumberErrorZone";
                      break;
                    case MapResources.VIDEO:
                      break;
                  }
                  if (isValidated) {
                    sitacState.deselectFabMenuIcons();
                    sitacState.fabCircularMenuKey.currentState.close();
                  } else {
                    _showFlushMessage(
                        FlutterI18n.translate(
                            sitacState.context, sitacState.errorMessage),
                        ColorRessources.addTravelsPointsErrorColors,
                        Duration(seconds: 3));
                  }
                  sitacState.refreshWidget();
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  _showVideo() {
    return YoutubePlayer(
      controller: sitacState.controller,
      width: MediaQuery.of(sitacState.context).size.width * 0.9,
      onReady: () {
      },
    );
  }

  _showFlushMessage(String message, Color color, Duration duration) {
    sitacState.flushbar.dismiss().then((onValue) {
      sitacState.flushbar = Flushbar(
        flushbarPosition: FlushbarPosition.TOP,
        icon: Icon(
          Icons.info_outline,
          size: 28.0,
          color: color,
        ),
        title: FlutterI18n.translate(sitacState.context, "error"),
        leftBarIndicatorColor: color,
        message: "$message",
        duration: duration,
        isDismissible: true,
        dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      );
      sitacState.flushbar.show(sitacState.context);
    });
  }

  _showAlertDialog(int index, String heroTag, String title, String desc) {
    Alert(
      context: sitacState.context,
      type: AlertType.warning,
      title: "$title",
      desc: "$desc",
      closeFunction: () {},
      buttons: [
        DialogButton(
          child: Text(
            FlutterI18n.translate(sitacState.context, "cancel_button"),
            style: TextStyle(fontSize: 20),
          ),
          onPressed: () => Navigator.pop(sitacState.context),
          radius: BorderRadius.all(Radius.circular(10)),
          color: ColorRessources.grey300,
        ),
        DialogButton(
          child: Text(
            FlutterI18n.translate(sitacState.context, "continue_button"),
            style: TextStyle(fontSize: 20),
          ),
          onPressed: () {
            switchSelected(index, heroTag);
            sitacState.refreshWidget();
            if (!sitacState.isFabMenuIconSelected())
              sitacState.fabCircularMenuKey.currentState.close();
            Navigator.pop(sitacState.context);
          },
          radius: BorderRadius.all(Radius.circular(10)),
          color: ColorRessources.grey300,
        ),
      ],
    ).show();
  }

  Widget getButton(double height) {
    return GestureDetector(
      onTap: () {
        sitacState.show = !sitacState.show;
        sitacState.refreshWidget();
      },
      child: Container(
        width: SITACRessource.sidebarExpandButtonWidth,
        height: SITACRessource.sidebarExpandButtonHeight,
        decoration: BoxDecoration(
          color: ColorRessources.primaryColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            bottomLeft: Radius.circular(10),
          ),
        ),
        child: Icon(sitacState.show ? Icons.chevron_right : Icons.chevron_left,
            color: ColorRessources.whiteColor),
      ),
    );
  }
}
