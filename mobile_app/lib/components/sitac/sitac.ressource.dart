class SITACRessource {
  static const double sidebarSize = 0.30;
  static const double sidebarExpandButtonWidth = 30;
  static const double sidebarExpandButtonHeight = 150;
  static const double iconIncreaseWidth = 30;
  static const double meansIconsWidth = 80;
  static const double ressourcesIconsWidth = 60;
  static const double meansRepresentationTextSize = 5;
}