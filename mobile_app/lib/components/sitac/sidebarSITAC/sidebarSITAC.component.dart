import 'package:flutter/material.dart';
import 'package:sith/components/sitac/map/map.service.dart';
import 'package:sith/components/sitac/meansRepresentation/meansRepresentation.component.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/components/sitac/sidebarSITAC/sidebarSITAC.view.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/colorUtils.dart';
import 'package:sith/domain/poiIcon.model.dart';

import 'iconSITAC/iconSidebarSITAC.component.dart';

// ignore: must_be_immutable
class SidebarSITAC extends StatefulWidget {

  final String _interventionUid;
  List<PoiIcon> _icons;
  SidebarSITACState _sidebarSITACState;

  SidebarSITAC(this._interventionUid, this._icons){
    _sidebarSITACState = SidebarSITACState(this._interventionUid);
  }

  void setIcons(List<PoiIcon> icons){
    this._icons = icons;
    _sidebarSITACState.setIcons(_icons);
  }

  @override
  State<StatefulWidget> createState() => _sidebarSITACState;
}

class SidebarSITACState extends State<SidebarSITAC> {

  Color currentColor = ColorRessources.blackColor;

  IconSidebarSITAC selectedIcon;
  MeansRepresentation selectedMeans;

  List<String> meansIcons = <String> [
    "assets/moyen-actif.png",
  ];

  List<String> externalMeansIcons = <String> [
    "assets/moyen-actif-externe.png",
  ];

  List<String> ressourcesIcons = <String> [
    "assets/ressource-eau.png",
    "assets/ressource-point-sensible.png",
    "assets/ressource-sinistre.png",
    "assets/ressource-source-danger.png",
  ];

  List<PoiIcon> meansRepresentation = [];

  String interventionUid;

  SidebarSITACState(this.interventionUid);

  @override
  void initState(){
    super.initState();
    meansRepresentation = [] ;
    meansRepresentation = PoiIconService().getMeansInCRM(widget._icons);
  }

  @override
  Widget build(BuildContext context) {
    return SidebarSITACView(this).buildView(context);
  }

  void changeColor(Color color) {
    currentColor = color;
    MapService().setTargetColor(convertColorToString(currentColor));
    refreshWidget();
  }

  void setIcons(List<PoiIcon> icons){
    meansRepresentation = PoiIconService().getMeansInCRM(icons);
    refreshWidget();
  }

  refreshWidget(){
    if(!mounted) return;
    setState(() {});
  }
}
