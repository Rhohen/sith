import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/sitac/map/map.resources.dart';
import 'package:sith/components/sitac/map/map.service.dart';
import 'package:sith/components/sitac/meansRepresentation/meansRepresentation.component.dart';
import 'package:sith/components/sitac/sidebarSITAC/sidebarSITAC.component.dart';
import 'package:sith/components/sitac/sitac.ressource.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/poiIcon.model.dart';

import 'colorPicker/colorPicker.dart';
import 'iconSITAC/iconSidebarSITAC.component.dart';

class SidebarSITACView {
  SidebarSITACState sidebarSITAC;

  SidebarSITACView(_sidebarSITAC) {
    sidebarSITAC = _sidebarSITAC;
  }

  Widget buildView(BuildContext context) {
    return Container(
      color: ColorRessources.backgroundSidebar,
      child: Column(
        children: [
          BlockPicker(
            pickerColor: ColorRessources.blackColor,
            onColorChanged: sidebarSITAC.changeColor,
          ),
          Flexible(
            child: ListView(
              children: <Widget>[
                _expansionTile('${FlutterI18n.translate(context, "means")}', sidebarSITAC.meansIcons,
                    SITACRessource.meansIconsWidth, MapResources.MEANS),
                _expansionTileCRM(sidebarSITAC.meansRepresentation,
                    SITACRessource.meansIconsWidth, MapResources.MEANS),
                _expansionTile(
                    '${FlutterI18n.translate(context, "external_means")}',
                    sidebarSITAC.externalMeansIcons,
                    SITACRessource.meansIconsWidth,
                    MapResources.EXTERNAL_MEANS),
                _expansionTile('${FlutterI18n.translate(context, "resources")}', sidebarSITAC.ressourcesIcons,
                    SITACRessource.ressourcesIconsWidth, MapResources.RESOURCE),
              ],
            ),
          )
        ],
      ),
    );
  }

  ExpansionTile _expansionTile(
      String header, List<String> icons, double width, String type) {
    return new ExpansionTile(
      title: Text(
        header,
        textAlign: TextAlign.center,
      ),
      children: <Widget>[
        Column(
          children: icons.map(
            (icon) {
              IconSidebarSITAC iconSidebarSITAC;
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: GestureDetector(
                      child: iconSidebarSITAC = IconSidebarSITAC(
                          icon,
                          sidebarSITAC.currentColor,
                          width,
                          type,
                          sidebarSITAC.selectedIcon),
                      onTap: () => {
                        sidebarSITAC.selectedIcon = iconSidebarSITAC,
                        sidebarSITAC.selectedMeans = null,
                        MapService().setTargetParameters(
                            sidebarSITAC.selectedIcon.type,
                            sidebarSITAC.selectedIcon.assetImage),
                        sidebarSITAC.refreshWidget(),
                      },
                    ),
                  ),
                ],
              );
            },
          ).toList(),
        )
      ],
    );
  }

  ExpansionTile _expansionTileCRM(
      List<PoiIcon> poiIcons, double width, String type) {
    return new ExpansionTile(
      title: Text(
        "CRM",
        textAlign: TextAlign.center,
      ),
      children: <Widget>[
        Column(
          children: poiIcons.map(
            (poiIcon) {
              MeansRepresentation meansSidebarSITAC;
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: GestureDetector(
                      child: meansSidebarSITAC = MeansRepresentation(
                        width,
                        poiIcon,
                        means: sidebarSITAC.selectedMeans,
                      ),
                      onTap: () => {
                        sidebarSITAC.selectedMeans = meansSidebarSITAC,
                        sidebarSITAC.selectedIcon = null,
                        MapService().setTargetMeans(sidebarSITAC.selectedMeans),
                        sidebarSITAC.refreshWidget(),
                      },
                    ),
                  ),
                  meansSidebarSITAC.switchButton(),
                  Padding(
                    padding: EdgeInsets.all(20.0),
                  )
                ],
              );
            },
          ).toList(),
        )
      ],
    );
  }
}
