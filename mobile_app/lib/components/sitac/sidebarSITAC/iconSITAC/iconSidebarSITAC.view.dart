import 'package:flutter/material.dart';
import 'package:sith/components/sitac/sitac.ressource.dart';

import 'iconSidebarSITAC.component.dart';

class IconSidebarSITACView {

  IconSidebarSITACState iconSidebarSITAC;

  IconSidebarSITACView(this.iconSidebarSITAC);

  Widget buildView() {
    return
      Image(
        image: AssetImage(iconSidebarSITAC.widget.assetImage),
        color: iconSidebarSITAC.widget.color,
        width: ((iconSidebarSITAC.widget.iconSidebarSITAC != null) && (iconSidebarSITAC.widget.iconSidebarSITAC.assetImage == iconSidebarSITAC.widget.assetImage))
            ? iconSidebarSITAC.widget.width + SITACRessource.iconIncreaseWidth : iconSidebarSITAC.widget.width,
    );
  }
}