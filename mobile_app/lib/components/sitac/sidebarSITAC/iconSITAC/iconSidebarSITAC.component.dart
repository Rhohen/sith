import 'package:flutter/material.dart';
import 'package:sith/components/sitac/map/poiMarker/poiMarker.abstract.dart';

import 'iconSidebarSITAC.view.dart';

// ignore: must_be_immutable
class IconSidebarSITAC extends StatefulWidget{

  String assetImage;
  Color color;
  double width;
  String type;
  IconSidebarSITAC iconSidebarSITAC;

  IconSidebarSITAC(this.assetImage ,this.color, this.width, this.type, this.iconSidebarSITAC);

  @override
  IconSidebarSITACState createState() => new IconSidebarSITACState();

}

class IconSidebarSITACState extends State<IconSidebarSITAC> {

  List<PoiMarker> meansPoiMarkers = [];

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new IconSidebarSITACView(this).buildView();
  }

  refresh(){
    if(!mounted) return;
    setState(() {});
  }
}

