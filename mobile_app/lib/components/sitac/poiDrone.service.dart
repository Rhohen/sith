import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sith/domain/poiDrone.model.dart';
import 'package:sith/services/firestore.service.dart';

class PoiDroneService {
  static CollectionReference interventionCollectionReference;

  static final PoiDroneService _singleton = initiate();

  factory PoiDroneService() {
    return _singleton;
  }

  /// Constructeur privé interne
  PoiDroneService._internal();

  static initiate() {
    interventionCollectionReference = FireConn().getInterventionsCollection();
    return PoiDroneService._internal();
  }

  CollectionReference getPoiDrone(String interventionId) {
    return interventionCollectionReference
        .document(interventionId)
        .collection('poiDrone');
  }

  Future<void> insertPoiDrone(String interventionId, PoiDrone poiDrone) {
    return interventionCollectionReference
        .document(interventionId)
        .collection('poiDrone')
        .document()
        .setData(poiDrone.toJson());
  }

  Future<void> clearPreviousPoiDrone(String interventionId) {
    return interventionCollectionReference
        .document(interventionId)
        .collection('poiDrone')
        .getDocuments()
        .then((snapshot) {
      for (DocumentSnapshot ds in snapshot.documents) {
        ds.reference.delete();
      }
    });
  }

  Stream<QuerySnapshot> gePoiDroneStream(String interventionUid) {
    return getPoiDroneCollection(interventionUid).snapshots();
  }

  CollectionReference getPoiDroneCollection(String interventionId) {
    return interventionCollectionReference
        .document(interventionId)
        .collection("poiDrone");
  }

  Stream<QuerySnapshot> getImages(String interventionId, String poiDroneUid) {
    return FireConn()
        .getInterventionsCollection()
        .document(interventionId)
        .collection("poiDrone")
        .document(poiDroneUid)
        .collection("images")
        .orderBy("timestamp")
        .snapshots();
  }
}
