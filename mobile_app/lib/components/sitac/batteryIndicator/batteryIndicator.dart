library battery_indicator;

import 'package:flutter/material.dart';

enum BatteryIndicatorStyle { flat, skeumorphism }

class BatteryIndicator extends StatelessWidget {
  /// indicator style，[BatteryIndicatorStyle.flat] and [BatteryIndicatorStyle.skeumorphism]
  final BatteryIndicatorStyle style;

  /// widget`s width / height , default to 2.5：1
  final double ratio;

  /// color of borderline , and fill color when colorful is false
  final Color mainColor;

  /// if colorful = true , then the fill color will automatic change depend on battery value
  final bool colorful;

  /// whether paint fill color
  final bool showPercentSlide;

  /// whether show battery value , Recommended [NOT] set to True when colorful = false
  final bool showPercentNum;

  /// default to 14.0
  final double size;

  /// default to 100
  final int batteryLv;

  BatteryIndicator({
    this.style = BatteryIndicatorStyle.flat,
    this.ratio = 2.5,
    this.mainColor = Colors.black,
    this.colorful = true,
    this.showPercentNum = true,
    this.showPercentSlide = true,
    this.size = 14.0,
    this.batteryLv = 100,
  });

  @override
  Widget build(BuildContext context) {
    return (batteryLv >= 0)
        ? new Container(
            child: new SizedBox(
              height: size,
              width: size * ratio,
              child: new CustomPaint(
                painter: new BatteryIndicatorPainter(
                    batteryLv, style, showPercentSlide, colorful, mainColor),
                child: new Center(
                  child: new Padding(
                    padding: new EdgeInsets.only(
                        right: style == BatteryIndicatorStyle.flat
                            ? 0.0
                            : size * ratio * 0.04),
                    child: showPercentNum
                        ? new Text(
                            '$batteryLv',
                            style: new TextStyle(fontSize: size - 2),
                          )
                        : new Text(
                            '',
                            style: new TextStyle(fontSize: size - 2),
                          ),
                  ),
                ),
              ),
            ),
          )
        : Container(
            height: 0,
            width: 0,
          );
  }
}

class BatteryIndicatorPainter extends CustomPainter {
  int batteryLv;
  BatteryIndicatorStyle style;
  bool colorful;
  bool showPercentSlide;
  Color mainColor;

  BatteryIndicatorPainter(this.batteryLv, this.style, this.showPercentSlide,
      this.colorful, this.mainColor);

  @override
  void paint(Canvas canvas, Size size) {
    if (style == BatteryIndicatorStyle.flat) {
      canvas.drawRRect(
          new RRect.fromLTRBR(0.0, size.height * 0.05, size.width,
              size.height * 0.95, const Radius.circular(100.0)),
          new Paint()
            ..color = mainColor
            ..strokeWidth = 0.5
            ..style = PaintingStyle.stroke);

      if (showPercentSlide) {
        canvas.clipRect(new Rect.fromLTWH(0.0, size.height * 0.05,
            size.width * fixedBatteryLv / 100, size.height * 0.95));

        double offset = size.height * 0.1;

        canvas.drawRRect(
            new RRect.fromLTRBR(
                offset,
                size.height * 0.05 + offset,
                size.width - offset,
                size.height * 0.95 - offset,
                const Radius.circular(100.0)),
            new Paint()
              ..color = colorful ? getBatteryLvColor : mainColor
              ..style = PaintingStyle.fill);
      }
    } else {
      canvas.drawRRect(
          new RRect.fromLTRBR(0.0, size.height * 0.05, size.width * 0.92,
              size.height * 0.95, new Radius.circular(size.height * 0.1)),
          new Paint()
            ..color = mainColor
            ..strokeWidth = 0.8
            ..style = PaintingStyle.stroke);

      canvas.drawRRect(
          new RRect.fromLTRBR(size.width * 0.92, size.height * 0.25, size.width,
              size.height * 0.75, new Radius.circular(size.height * 0.1)),
          new Paint()
            ..color = mainColor
            ..style = PaintingStyle.fill);

      if (showPercentSlide) {
        canvas.clipRect(new Rect.fromLTWH(0.0, size.height * 0.05,
            size.width * 0.92 * fixedBatteryLv / 100, size.height * 0.95));

        double offset = size.height * 0.1;

        canvas.drawRRect(
            new RRect.fromLTRBR(
                offset,
                size.height * 0.05 + offset,
                size.width * 0.92 - offset,
                size.height * 0.95 - offset,
                new Radius.circular(size.height * 0.1)),
            new Paint()
              ..color = colorful ? getBatteryLvColor : mainColor
              ..style = PaintingStyle.fill);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return (oldDelegate as BatteryIndicatorPainter).batteryLv != batteryLv ||
        (oldDelegate as BatteryIndicatorPainter).mainColor != mainColor;
  }

  get fixedBatteryLv => batteryLv < 10 ? 4 + batteryLv / 2 : batteryLv;

  get getBatteryLvColor => batteryLv < 15
      ? Colors.red
      : batteryLv < 30 ? Colors.orange : Colors.green;
}
