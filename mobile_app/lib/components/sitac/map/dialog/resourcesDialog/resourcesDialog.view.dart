import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/sitac/map/dialog/resourcesDialog/resourcesDialog.component.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/enum/category.model.dart';

/// This class is the view of the Ressources.
class ResourcesDialogView {
  final ResourcesDialogState resourcesDialogState;
  final BuildContext _context;

  ResourcesDialogView(this.resourcesDialogState, this._context);

  Widget buildView() {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))
      ),
      title: Text(FlutterI18n.translate(resourcesDialogState.context, "resources")),
      content: getContent()
    );
  }

  Widget getContent() {
    return Container(
      width: double.maxFinite,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(FlutterI18n.translate(resourcesDialogState.context, "category") +
                      " : "),
                  Flexible(child: _selectCategory()),
                  _getMoveButton(),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                  onPressed: () => {
                    resourcesDialogState.deletePoiIcon(),
                  Navigator.of(_context).pop(),
                  },
                  child: Icon(Icons.delete),
                ),
                  RaisedButton(
                    child: Text(FlutterI18n.translate(resourcesDialogState.context, "save")),
                    onPressed: () {
                      resourcesDialogState.updatePoiIcon();
                      Navigator.of(_context).pop();
                    },
                  ),
                ],
              ),
            ],
          )
        ]
      )
    );
  }

  Widget _getMoveButton() {
    return Material(
      color: Colors.white,
      child: Center(
        child: Ink(
          decoration: ShapeDecoration(
            color: ColorRessources.accentColor,
            shape: CircleBorder(),
          ),
          child: IconButton(
            icon: Icon(Icons.pin_drop),
            color: Colors.white,
            onPressed: () {
              resourcesDialogState.moveMarker();
              Navigator.of(_context).pop();
            },
          ),
        ),
      ),
    );
  }

  Widget _selectCategory() {
    return DropdownButton<Category>(
      value: resourcesDialogState.poi.category,
      onChanged: (value) {
        resourcesDialogState.poi.category = value;
        resourcesDialogState.refreshWidget();
      },
      items: Category.values.map((Category category) {
        return DropdownMenuItem<Category>(
            value: category, child: Text(EnumToString.parse(category)));
      }).toList(),
      isDense: false,
      isExpanded: true,
    );
  }

}