import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/sitac/map/dialog/resourcesDialog/resourcesDialog.view.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/domain/poiIcon.model.dart';

/// This class manages the ressources.
// ignore: must_be_immutable
class ResourcesDialog extends StatefulWidget {

  PoiIcon _poi;
  Function() moveMarker;

  ResourcesDialog(this._poi, this.moveMarker);

  @override
  State<StatefulWidget> createState() => new ResourcesDialogState(this._poi);
}

class ResourcesDialogState extends State<ResourcesDialog> {

  PoiIcon poi;

  ResourcesDialogState(this.poi);

  @override
  Widget build(BuildContext context) {
    return new ResourcesDialogView(this, context).buildView();
  }

  /// This method is in charge to delete a PoiIcon.
  void deletePoiIcon() {
    PoiIconService().deleteIcon(this.poi);
  }

  /// This method is in charge
  /// to update the PoiIcon in the database.
  void updatePoiIcon(){
    PoiIconService().updatePoiIcon(poi);
  }

  /// This method is in charge to update the label.
  updateLabel(){
    String townshipCode = "";
    if(poi.township.length > 2 ){
      townshipCode = poi.township.substring(0, 2);
    }
    else townshipCode = poi.township;
    poi.label = '${EnumToString.parse(poi.type)}  $townshipCode';
  }

  void moveMarker(){
    widget.moveMarker();
  }

  refreshWidget(){
    setState(() {});
  }

}