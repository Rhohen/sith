import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/intervention/vehicules.service.dart';
import 'package:sith/components/means/means.service.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/enum/category.model.dart';
import 'package:sith/domain/enum/firefighter.model.dart';
import 'package:sith/domain/means.model.dart';
import 'package:sith/domain/poiIcon.model.dart';
import 'meansDialog.view.dart';

// ignore: must_be_immutable
class MeansDialog extends StatefulWidget {

  PoiIcon _poi;
  Function() moveMarker;

  MeansDialog(this._poi,  this.moveMarker);

  @override
  State<StatefulWidget> createState() => MeansDialogState(_poi);
}

class MeansDialogState extends State<MeansDialog> {

  PoiIcon poiIcon;
  bool isSwitched;
  Firefighter selectedType;
  Category selectedCategory;
  String typedTowship;
  String typedLabel;

  MeansDialogState(this.poiIcon);

  @override
  void initState() {
    super.initState();
    isSwitched = poiIcon.isActive;
    selectedType = poiIcon.type;
    selectedCategory = poiIcon.category;
    typedLabel = poiIcon.label;
    typedTowship = poiIcon.township;
  }

  @override
  Widget build(BuildContext context) {
    return MeansDialogView(this, context).buildView();
  }

  updateLabel(){
    String townshipCode = "";
    List<String> parts;
    RegExp regExp = RegExp('^([A-Z]{2,4}( - #[1-9]{1,3})?( [a-zA-z]{1,2})?)\$');

    if(typedTowship.length >= 2){
      townshipCode = ' ' + typedTowship.substring(0,2);
    }
    else if( typedTowship.length == 1 ){
      townshipCode = ' ' + typedTowship;
    }
    else townshipCode = '';

    if( typedLabel == null || typedLabel == '' ){
      typedLabel = '${EnumToString.parse(selectedType)}  $townshipCode';
    }
    else if(regExp.hasMatch(typedLabel)) {
      parts = typedLabel.split(' ');
      typedLabel = EnumToString.parse(selectedType);
      if (parts.length >= 3){
        typedLabel += ' ${parts[1]} ${parts[2]}';
      }
      typedLabel += '$townshipCode';
    }
  }

  void changeCategory(Category category){
//  poiIcon.category = category;
    selectedCategory = category;
    refreshWidget();
  }

  void changeType(Firefighter type){
//  poiIcon.type = type;
    selectedType = type;
    updateLabel();
    refreshWidget();
  }

  void updatePoiIcon(){
    Means means;
    poiIcon.category = selectedCategory;
    poiIcon.type = selectedType;
    poiIcon.township = typedTowship;
    poiIcon.label = typedLabel;
    MeansService().getMean(poiIcon.idIntervention, poiIcon.idMeans).then(
      (m) {
        means = Means.from(poiIcon.idMeans, m.data);
        means.category = poiIcon.category;
        means.type = poiIcon.type;
        means.engageHour = (means.engageHour == null && poiIcon.isActive ? DateTime.now() : means.engageHour);
        MeansService().updateMean(poiIcon.idIntervention, means).then((onValue) {
          PoiIconService().updatePoiIcon(poiIcon);
        });
      }
    );
  }

  void moveMarker(){
    widget.moveMarker();
  }

  Widget switchButton(){
    return Switch(
      value: isSwitched,
      onChanged: (value) {
        setState(() {
          isSwitched = value;
          poiIcon.isActive = !poiIcon.isActive;
        });
      },
      activeTrackColor: ColorRessources.switchActiveTrack,
      activeColor: ColorRessources.switchActiveColor,
    );
  }

  putInCRM(){
    poiIcon.isOnMap = false;
    poiIcon.isActive = false;
    updatePoiIcon();
  }

  release() async {
    await MeansService().getMean(poiIcon.idIntervention, poiIcon.idMeans).then((meanSnapshot) {
      Means means = Means.from(poiIcon.idMeans, meanSnapshot.data);
      means.releaseHour = DateTime.now();
      MeansService().updateMean(poiIcon.idIntervention, means).then((onValue) {
        PoiIconService().deleteIcon(poiIcon);
        VehiculesService().releaseOneMeans(means);
      });
    });
  }

  refreshWidget(){
    setState(() {});
  }

}