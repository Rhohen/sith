import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/enum/category.model.dart';
import 'package:sith/domain/enum/firefighter.model.dart';

import 'meansDialog.component.dart';

class MeansDialogView {
  final MeansDialogState meansDialogState;
  final BuildContext _context;

  MeansDialogView(this.meansDialogState, this._context);

  Widget buildView() {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))
      ),
      content: getContent(),
    );
  }

  Widget getContent() {
    return Container(
      width: double.maxFinite,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _firstRow(),
              _secondRow(),
              _thirdRow(),
            ],
          )
        ]
      )
    );
  }

  Widget _firstRow(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Flexible(
          child:
          Container(
            width: 275,
            height: 50,
            child : TextField(
              controller: TextEditingController()..text = '${meansDialogState.typedLabel}',
              enabled: false,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: '${FlutterI18n.translate(meansDialogState.context, "labelNoDot")}'),
              onSubmitted: (text) => meansDialogState.typedLabel = text,
            ),
          ),
        ),
        Flexible(
          child: Container(
            width: 275,
            height: 50,
            child : TextField(
              controller: TextEditingController()..text = '${meansDialogState.typedTowship}',
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: '${FlutterI18n.translate(meansDialogState.context, "township")}'),
              onSubmitted: (text) => {
                meansDialogState.typedTowship = text,
                meansDialogState.updateLabel(),
                meansDialogState.refreshWidget(),
              },
            ),
          ),
        ),
        Material(
          child: Center(
            child: Ink(
              decoration: ShapeDecoration(
                color: ColorRessources.accentColor,
                shape: CircleBorder(),
              ),
              child: IconButton(
                icon: Icon(Icons.pin_drop),
                color: ColorRessources.whiteColor,
                onPressed: () {
                  meansDialogState.moveMarker();
                  Navigator.of(_context).pop();
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _secondRow(){
    return  Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(FlutterI18n.translate(meansDialogState.context, "category") +
            " : "),
        Flexible(child:  Container(
          width: 160,
          child : _selectCategory())
        ),
        Text(FlutterI18n.translate(meansDialogState.context, "type") +
            " : "),
        Flexible(child: Container(
          width: 150,
          child : _printType()),
        ),
        Text(FlutterI18n.translate(meansDialogState.context, "active") +
            " : "),
        meansDialogState.switchButton(),
      ],
    );
  }

  Widget _selectCategory() {
    return DropdownButton<Category>(
      value: meansDialogState.selectedCategory,
      onChanged: (value) {
        meansDialogState.changeCategory(value);
      },
      items: Category.values.map((Category category) {
        return DropdownMenuItem<Category>(
            value: category, child: Text(EnumToString.parse(category)));
      }).toList(),
      isDense: false,
      isExpanded: true,
    );
  }

  Widget _thirdRow(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        RaisedButton(
          child: Text(FlutterI18n.translate(meansDialogState.context, "put_crm")),
          onPressed: () {
            meansDialogState.putInCRM();
            Navigator.of(_context).pop();
          },
        ),
        RaisedButton(
          child: Text(FlutterI18n.translate(meansDialogState.context, "release")),
          onPressed: () {
            meansDialogState.release();
            Navigator.of(_context).pop();
          },
        ),
        RaisedButton(
          child: Text(FlutterI18n.translate(meansDialogState.context, "validate_button")),
          onPressed: () {
            meansDialogState.updatePoiIcon();
            Navigator.of(_context).pop();
          },
        ),
      ],
    );
  }

  Widget _printType() {
    return Text('${EnumToString.parse(meansDialogState.selectedType)}', style: TextStyle(fontWeight: FontWeight.bold),);
  }
}
