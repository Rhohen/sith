
import 'package:flutter/cupertino.dart';
import 'package:sith/components/sitac/map/dialog/interventionDialog/interventionDialog.view.dart';
import 'package:sith/domain/intervention.model.dart';

class InterventionDialog extends StatelessWidget {

  final Intervention intervention;

  InterventionDialog(this.intervention);

  @override
  Widget build(BuildContext context) {
    return InterventionDialogView(this).buildView(context);
  }
}
