
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/sitac/map/dialog/interventionDialog/interventionDialog.component.dart';

class InterventionDialogView {

  InterventionDialog dialog;

  InterventionDialogView(this.dialog);

  Widget buildView(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))
      ),
      title: Text(dialog.intervention.name),
      content: getContent(context),
    );
  }

  Widget getContent(BuildContext context) {
    return Container(
      child : Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text('${FlutterI18n.translate(context, "codeSinistre")}', style: TextStyle(fontWeight: FontWeight.bold)),
              Text('${EnumToString.parse(dialog.intervention.codeSinistre)}'),
              Spacer(),
              Text('${FlutterI18n.translate(context, "contact")}', style: TextStyle(fontWeight: FontWeight.bold)),
              Text('${dialog.intervention.contact}'),
            ],
          ),
          if(dialog.intervention.address != null) Text(
              '${FlutterI18n.translate(context, "address_form")} : ', style: TextStyle(fontWeight: FontWeight.bold)),
          if(dialog.intervention.address != null) Text('${dialog.intervention.address}'),
          if(dialog.intervention.coordinate != null) Text(
              '${FlutterI18n.translate(context, "coordonates")}', style: TextStyle(fontWeight: FontWeight.bold),
          ),
          if(dialog.intervention.coordinate != null) Text(
              '${dialog.intervention.coordinate.latitude} : ${dialog.intervention.coordinate.longitude}'
          ),
        ],
      ),
    );
  }
}