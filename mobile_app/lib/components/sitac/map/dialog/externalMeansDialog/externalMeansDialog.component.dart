import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/poiIcon.model.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import '../../../poiIcons.service.dart';
import 'externalMeansDialog.view.dart';


class ExternalMeansDialog extends StatefulWidget {
  PoiIcon _poi;
  Function() moveMarker;

  ExternalMeansDialog(this._poi, this.moveMarker);

  @override
  // State<StatefulWidget> createState() => new ExternalMeansDialogState(this._poi);
  State<StatefulWidget> createState() => ExternalMeansDialogState(_poi);
}

class ExternalMeansDialogState extends State<ExternalMeansDialog> {
  PoiIcon poiIcon;
  bool isSwitched;

  ExternalMeansDialogState(this.poiIcon);


  @override
  void initState() {
    super.initState();
    isSwitched = poiIcon.isActive;
  }

  @override
  Widget build(BuildContext context) {
    return ExternalMeansDialogView(this, context).buildView();
  }

  /// This method is in charge to delete a PoiIcon.
  void deletePoiIcon() {
    PoiIconService().deleteIcon(this.poiIcon);
  }

  /// This method is in charge
  /// to update the PoiIcon in the database.
  void updatePoiIcon(){
    PoiIconService().updatePoiIcon(poiIcon);
  }

  void moveMarker(){
    widget.moveMarker();
  }

  Widget switchButton(){
    return Switch(
      value: isSwitched,
      onChanged: (value) {
        setState(() {
          isSwitched = value;
          poiIcon.isActive = !poiIcon.isActive;
        });
      },
      activeTrackColor: ColorRessources.switchActiveTrack,
      activeColor: ColorRessources.switchActiveColor,
    );
  }

  refreshWidget(){
    if(!mounted) return;
    setState(() {});
  }

}