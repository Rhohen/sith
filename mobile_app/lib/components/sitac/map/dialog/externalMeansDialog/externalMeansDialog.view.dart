import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/enum/category.model.dart';

import 'externalMeansDialog.component.dart';

class ExternalMeansDialogView {

  final ExternalMeansDialogState externalMeansDialogState;
  final BuildContext _context;

  ExternalMeansDialogView(this.externalMeansDialogState, this._context);

  Widget buildView() {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))
      ),
      title: Text(FlutterI18n.translate(externalMeansDialogState.context, "external_means")),
      content: getContent(),
    );
  }


  Widget _getMoveButton() {
    return Material(
      color: Colors.white,
      child: Center(
        child: Ink(
          decoration: ShapeDecoration(
            color: ColorRessources.accentColor,
            shape: CircleBorder(),
          ),
          child: IconButton(
            icon: Icon(Icons.pin_drop),
            color: Colors.white,
            onPressed: () {
              externalMeansDialogState.moveMarker();
              Navigator.of(_context).pop();
            },
          ),
        ),
      ),
    );
  }

  Widget getContent() {
    return Container(
        width: double.maxFinite,
        child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _firstRow(),
                  _secondRow(),
                ],
              )
            ]
        )
    );
  }

  Widget _firstRow(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Flexible(
          child:
          Container(
            width: 260,
            height: 50,
            child : TextField(
              controller: TextEditingController()..text = '${externalMeansDialogState.poiIcon.label}',
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: '${FlutterI18n.translate(externalMeansDialogState.context, "labelNoDot")}'),
              onSubmitted: (text) =>externalMeansDialogState.poiIcon.label = text,
            ),
          ),
        ),
        Flexible(
          child: Container(
            width: 260,
            height: 50,
            child: _selectCategory(),
          ),
        ),
        _getMoveButton(),
      ],
    );
  }

  Widget _secondRow(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(FlutterI18n.translate(externalMeansDialogState.context, "active") +
            " : "),
        externalMeansDialogState.switchButton(),
        RaisedButton(
          child: Text(FlutterI18n.translate(externalMeansDialogState.context, "release")),
          onPressed: () {
            externalMeansDialogState.deletePoiIcon();
            Navigator.of(_context).pop();
          },
        ),
        RaisedButton(
          child: Text(FlutterI18n.translate(externalMeansDialogState.context, "validate_button")),
          onPressed: () {
            externalMeansDialogState.updatePoiIcon();
            Navigator.of(_context).pop();
          },
        ),
      ],
    );
  }

  /// This widget is in charge to change the color
  Widget _selectCategory() {
    return DropdownButton<Category>(
      value: externalMeansDialogState.poiIcon.category,
      onChanged: (value) {
        externalMeansDialogState.poiIcon.category = value;
        externalMeansDialogState.refreshWidget();
      },
      items: Category.values.map((Category category) {
        return DropdownMenuItem<Category>(
            value: category, child: Text(EnumToString.parse(category)));
      }).toList(),
      isDense: false,
      isExpanded: true,
    );
  }

}