import 'dart:core';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/services/telemetry.service.dart';
import 'map.component.dart';
import 'map.resources.dart';

class MapView {
  final MapPageState mapPage;

  MapControllerImpl impl;

  MapView(this.mapPage);

  Widget buildView(BuildContext context) {
    return (mapPage.mapLoaded
        ? getMap(context)
        : Center(child: CircularProgressIndicator()));
  }

  Widget getMap(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
        stream: TelemetryService().getDroneTelemetry(mapPage.intervention.uid),
        builder: (context, snapshotTelemetry) {
          if (snapshotTelemetry.hasData &&
              null != snapshotTelemetry.data.data &&
              snapshotTelemetry.data.data.isNotEmpty) {
            mapPage.addTelemetry(snapshotTelemetry.data);
          }
          return Stack(
            children: <Widget>[
              Container(
                child: FlutterMap(
                  options: MapOptions(
                    plugins: [
                      //MarkerClusterPlugin(),
                    ],
                    center: mapPage.mapCenter,
                    minZoom: 5.0,
                    zoom: MapResources.initialZoom,
                    onTap: (point) => mapPage.addElementOnMap(point),
                  ),
                  layers: [
                    TileLayerOptions(
                      urlTemplate: MapResources.url,
                      additionalOptions: {
                        'accessToken': MapResources.token,
                        'id': MapResources.mapId,
                      },
                    ),
                    mapPage.persistentPOILayer(),
                    mapPage.meansLayer(),
                    mapPage.externalMeansLayer(),
                    mapPage.resourcesLayer(),
                    mapPage.poiDronePolylineLayer(),
                    mapPage.poiDronePolygonLayer(),
                    mapPage.poiDroneMarkerLayer(),
                    PolylineLayerOptions(
                        polylines: mapPage.statefulMapController.lines),
                    PolygonLayerOptions(
                        polygons: mapPage.statefulMapController.polygons),
                    MarkerLayerOptions(
                        markers: mapPage.statefulMapController.markers),
                  ],
                ),
              ),
              if (mapPage.isOnMove)
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 20),
                    child: RaisedButton(
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      elevation: 5.0,
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      color: ColorRessources.darkColor,
                      child: Text(
                        FlutterI18n.translate(context, "cancel_button"),
                        style: TextStyle(
                          fontSize: 25.0,
                          color: ColorRessources.whiteColor,
                        ),
                      ),
                      onPressed: () {
                        mapPage.deselectMoveMarker();
                      },
                    ),
                  ),
                ),
            ],
          );
        });
  }
}
