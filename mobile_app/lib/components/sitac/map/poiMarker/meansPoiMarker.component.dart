import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:sith/components/sitac/map/dialog/meansDialog/meansDialog.component.dart';
import 'package:sith/components/sitac/map/poiMarker/poiMarker.abstract.dart';
import 'package:sith/components/sitac/meansRepresentation/meansRepresentation.component.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/domain/poiIcon.model.dart';

import '../map.resources.dart';

class MeansPoiMarker extends PoiMarker {

  MeansPoiMarker(PoiIcon poi, Function moveMarker) : super(poi, moveMarker);

  @override
  Marker getMarker(BuildContext context) {
    return Marker(
      //anchorPos: AnchorPos.align(AnchorAlign.center),
      height: MapResources.poiHeight,
      width: MapResources.poiWidth,
      point: poi.coordinates,
      builder: (ctx) =>
        GestureDetector(
          onTap: () =>
            showDialog(
              context: ctx,
              child: MeansDialog(poi, moveMarkerOnMap),
            ),
          child: Stack(
            children: <Widget>[
              MeansRepresentation(MapResources.poiWidth ,poi),
              if(isMoving) Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: ColorRessources.accentColor,
                    width: 3,
                  ),
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
            ],
          ),
        ),
    );
  }

}
