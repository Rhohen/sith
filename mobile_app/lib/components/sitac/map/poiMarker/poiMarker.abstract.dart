import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/domain/poiIcon.model.dart';

abstract class PoiMarker {

  @protected
  PoiIcon poi;

  bool isMoving = false;

  Function(PoiMarker) moveMarker;

  PoiMarker(this.poi, this.moveMarker);

  Marker getMarker(BuildContext ctx);

  moveMarkerOnMap(){
    this.moveMarker(this);
    isMoving = true;
  }

  moveTo(LatLng point){
    poi.coordinates = point;
    if(poi.isActive != null) poi.isActive = false;
    PoiIconService().updatePoiIcon(poi);
  }
}
