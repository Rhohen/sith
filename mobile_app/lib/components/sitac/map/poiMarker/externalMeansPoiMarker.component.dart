import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:sith/components/sitac/map/dialog/externalMeansDialog/externalMeansDialog.component.dart';
import 'package:sith/components/sitac/map/poiMarker/poiMarker.abstract.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/colorUtils.dart';
import 'package:sith/domain/poiIcon.model.dart';

import '../map.resources.dart';

class ExternalMeansPoiMarker extends PoiMarker {
  ExternalMeansPoiMarker(PoiIcon poi, Function moveMarker)
      : super(poi, moveMarker);

  @override
  Marker getMarker(BuildContext context) {
    return Marker(
      height: MapResources.poiHeight,
      width: MapResources.poiWidth,
      point: poi.coordinates,
      builder: (ctx) => GestureDetector(
        onTap: () => showDialog(
          context: context,
          child: ExternalMeansDialog(poi, moveMarkerOnMap),
        ),
        child: Stack(
          alignment: Alignment(0.0, 0.0),
          children: <Widget>[
            Image(
                image: AssetImage(getAssetPath()),
                color: convertCategoryEnumToColor(poi.category)),
            Align(
              child: Text(poi.label,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: getTextSize(),
                    color: convertCategoryEnumToColor(poi.category),
                  )),
            ),
            if (isMoving)
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: ColorRessources.accentColor,
                    width: 3,
                  ),
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
          ],
        ),
      ),
    );
  }

  double getTextSize() {
    if (poi != null && poi.label != null) {
      if (poi.label.length < 7) {
        return 10;
      } else if (poi.label.length < 8) {
        return 9;
      }
    }
    return 8;
  }

  String getAssetPath(){
    if(poi.isActive){
      return MapResources.assetPath + 'moyen-actif-externe' + MapResources.assetExtention;
    }
    return MapResources.assetPath + 'moyen-prevu-externe' + MapResources.assetExtention;
  }
}
