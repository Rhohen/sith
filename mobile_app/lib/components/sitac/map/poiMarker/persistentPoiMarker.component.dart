import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:sith/components/sitac/map/poiMarker/poiMarker.abstract.dart';
import 'package:sith/components/utils/colorUtils.dart';
import 'package:sith/domain/persistentPoi.model.dart';

import '../map.resources.dart';

class PersistentPoiMarker extends PoiMarker {
  PersistentPoi _persistentPoi;

  PersistentPoiMarker(this._persistentPoi, Function moveMarker) : super(null, moveMarker);

  @override
  Marker getMarker(BuildContext ctx) {
    return Marker(
      //anchorPos: AnchorPos.align(AnchorAlign.center),
      height: MapResources.poiHeight,
      width: MapResources.poiWidth,
      point: _persistentPoi.coordinates,
      builder: (ctx) =>
          Image(image: AssetImage(_persistentPoi.iconPath), color: convertStringToColor(_persistentPoi.color)),
    );
  }
}
