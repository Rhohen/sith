import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:sith/components/sitac/map/dialog/resourcesDialog/resourcesDialog.component.dart';
import 'package:sith/components/sitac/map/poiMarker/poiMarker.abstract.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/colorUtils.dart';
import 'package:sith/domain/poiIcon.model.dart';

import '../map.resources.dart';

class ResourcesPoiMarker extends PoiMarker {

  ResourcesPoiMarker(PoiIcon poi, Function moveMarker) : super(poi, moveMarker);

  @override
  Marker getMarker(BuildContext context) {
    return Marker(
      //anchorPos: AnchorPos.align(AnchorAlign.center),
      height: MapResources.poiHeight,
      width: MapResources.poiWidth,
      point: poi.coordinates,
      builder: (ctx) =>
        GestureDetector(
          onTap: () =>
              showDialog(
                context: context,
                child: ResourcesDialog(poi, moveMarkerOnMap),
              ),
          child: Stack(
            children: <Widget>[
              Image(image: AssetImage(poi.iconPath), color: convertCategoryEnumToColor(poi.category)),
              if(isMoving) Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: ColorRessources.accentColor,
                    width: 3,
                  ),
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
            ],
          ),
        ),
    );
  }
}