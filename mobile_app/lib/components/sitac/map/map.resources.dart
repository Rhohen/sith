class MapResources {
  static const double poiHeight = 50;
  static const double poiWidth = 50;
  static const String assetPath = 'assets/';
  static const String assetExtention = '.png';
  static const double initialZoom = 17.0;
  static const double initialLat = 48.114712;
  static const double initialLng = -1.636456;
  static const String token =
      'pk.eyJ1IjoiaHVndWVzbGIiLCJhIjoiY2s3bmVzc3Q5MDF1YjNtbjZtbDk0MHJzbSJ9.4jg-JD9dLmOHMGAUmrl20A';
  static const String url =
      'https://api.mapbox.com/styles/v1/hugueslb/ck7un1qfm1ibn1imqpci9y2cq/tiles/256/{z}/{x}/{y}@2x?access_token={accessToken}';
  static const String mapId = 'mapbox.mapbox-streets-v8';
  static const String NON_CLOSED = 'nonclosed';
  static const String CLOSED = 'closed';
  static const String ZONE_INC = 'zone_inc';
  static const String ZONE_EX = 'zone_ex';
  static const String VIDEO = 'video';
  static const String MEANS = 'means';
  static const String EXTERNAL_MEANS = 'externalMeans';
  static const String RESOURCE = 'ressource';
  static const String DRONE_PATH = 'assets/drone.png';
  static const String POI_DRONE_PATH = 'assets/poiDrone.png';
  static const String POI_DRONE_DRAW_PATH = 'assets/poiDroneDraw.png';
  static const String NONE = '';
}
