import 'dart:async';
import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:map_controller/map_controller.dart';
import 'package:sith/components/means/means.service.dart';
import 'package:sith/components/sitac/drone/droneTravelStepMarker.component.dart';
import 'package:sith/components/sitac/map/dialog/interventionDialog/interventionDialog.component.dart';
import 'package:sith/components/sitac/map/poiMarker/externalMeansPoiMarker.component.dart';
import 'package:sith/components/sitac/map/poiMarker/meansPoiMarker.component.dart';
import 'package:sith/components/sitac/map/poiMarker/persistentPoiMarker.component.dart';
import 'package:sith/components/sitac/map/poiMarker/poiMarker.abstract.dart';
import 'package:sith/components/sitac/map/poiMarker/resourcesPoiMarker.component.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/components/sitac/sitac.component.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/colorUtils.dart';
import 'package:sith/domain/enum/firefighter.model.dart';
import 'package:sith/domain/enum/location.model.dart';
import 'package:sith/domain/intervention.model.dart';
import 'package:sith/domain/means.model.dart';
import 'package:sith/domain/poiDrone.model.dart';
import 'package:sith/domain/poiIcon.model.dart';
import 'package:sith/domain/telemetry.model.dart';

import '../poiDrone.service.dart';
import 'map.resources.dart';
import 'map.service.dart';
import 'map.view.dart';

// ignore: must_be_immutable
class MapPage extends StatefulWidget {
  MapPageState _pageState;
  final GlobalKey key;
  final Intervention intervention;

  List<PoiIcon> _icons;
  List<PoiDrone> _iconsDrone;

  MapPage(this.intervention, this._icons, this.key) {
    _pageState = MapPageState(this.intervention);
  }

  void setIcons(List<PoiIcon> icons) {
    this._icons = icons;
    _pageState.setIcons(_icons);
  }

  void setIconsDrone(List<PoiDrone> iconsDrone) {
    this._iconsDrone = iconsDrone;
    _pageState.setIconsDrone(_iconsDrone);
  }

  bool isLoaded() {
    return _pageState.mapLoaded;
  }

  @override
  State<StatefulWidget> createState() => _pageState;
}

class MapPageState extends State<MapPage> {
  final Intervention intervention;
  var points = <LatLng>[];

  Map<int, List<LatLng>> pointsToPushInDb;
  var savedPoints = <LatLng>[];
  var clearedPoints = <LatLng>[];

  MapService service;
  BuildContext context;
  bool mapLoaded = false;
  LatLng mapCenter;
  int areaId;
  Telemetry droneTelemetry;
  Marker droneMarker;
  int batteryLv;

  List<PersistentPoiMarker> persistentPoiMarkers = [];
  List<PoiMarker> meansPoiMarkers = [];
  List<PoiMarker> externalMeansPoiMarkers = [];
  List<PoiMarker> resourcesPoiMarkers = [];
  List<PoiDrone> poiDrone = [];

  LatLng _selectedCoordinates;

  bool isOnMove = false;
  PoiMarker iconToMove;

  moveMarker(marker) {
    setState(() {
      iconToMove = marker;
      isOnMove = true;
    });
  }

  deselectMoveMarker() {
    setState(() {
      iconToMove.isMoving = false;
      iconToMove = null;
      isOnMove = false;
    });
  }

  ///  Constructor
  MapPageState(this.intervention);

  MapController mapController;
  StatefulMapController statefulMapController;
  StreamSubscription<StatefulMapControllerStateChange> sub;

  @override
  void initState() {
    super.initState();
    areaId = 0;
    batteryLv = -1;
    pointsToPushInDb = new HashMap<int, List<LatLng>>();
    // intialize the controllers
    mapController = MapController();
    statefulMapController = StatefulMapController(mapController: mapController);

    statefulMapController.onReady
        .then((_) => print("The map controller is ready"));

    sub = statefulMapController.changeFeed.listen((change) => setState(() {}));

    getPersistentPoiIcons();
    mapCenter = widget.intervention.coordinate;
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return MapView(this).buildView(context);
  }

  MarkerLayerOptions persistentPOILayer() {
    List<Marker> markers = getMarker(persistentPoiMarkers);
    markers.add(getMarkerIntervention());
    return MarkerLayerOptions(
      markers: markers,
    );
  }

  MarkerLayerOptions meansLayer() {
    return MarkerLayerOptions(
      markers: getMarker(meansPoiMarkers),
    );
  }

  MarkerLayerOptions externalMeansLayer() {
    return MarkerLayerOptions(
      markers: getMarker(externalMeansPoiMarkers),
    );
  }

  MarkerLayerOptions resourcesLayer() {
    return MarkerLayerOptions(
      markers: getMarker(resourcesPoiMarkers),
    );
  }

  PolylineLayerOptions poiDronePolylineLayer() {
    List<Polyline> polylines = [];

    if (poiDrone.length > 0 &&
        poiDrone.first.type != MapResources.ZONE_INC &&
        poiDrone.first.type != MapResources.ZONE_EX) {
      List<LatLng> points = <LatLng>[];
      double width = 3.0;
      bool isDotted = false;
      Color color = Colors.black;

      for (int i = 0; i < poiDrone.length; i++)
        points.add(poiDrone[i].coordinates);

      polylines.add(Polyline(
          points: points,
          strokeWidth: width,
          color: color,
          isDotted: isDotted));
    }
    return PolylineLayerOptions(
      polylines: polylines,
    );
  }

  MarkerLayerOptions poiDroneMarkerLayer() {
    List<Marker> markers = [];
    poiDrone.forEach((icon) {
      if (!(icon.type == MapResources.ZONE_INC) &&
          !(icon.type == MapResources.ZONE_EX) &&
          !(icon.type == MapResources.CLOSED &&
              icon.step == poiDrone.length - 1))
        markers.add(new TargetStepMarker(
                icon.coordinates,
                context,
                intervention.uid,
                icon.uid,
                MapResources.POI_DRONE_PATH,
                icon.nbImages)
            .getMarker());
    });
    return MarkerLayerOptions(
      markers: markers,
    );
  }

  PolygonLayerOptions poiDronePolygonLayer() {
    List<Polygon> polygons = [];
    if (poiDrone.length > 0 &&
        (poiDrone.first.type == MapResources.ZONE_INC ||
            poiDrone.first.type == MapResources.ZONE_EX)) {
      List<LatLng> pointsDrone = <LatLng>[];
      double borderWidth = 2.0;
      Color borderColor = Colors.black;

      poiDrone.sort((a, b) => a.areaId.compareTo(b.areaId));

      for (int i = 0; i < poiDrone.length; i++) {
        PoiDrone dronePoint = poiDrone[i];
        Color color = dronePoint.type == MapResources.ZONE_EX
            ? Color.fromRGBO(139, 0, 0, 0.4)
            : Color.fromRGBO(120, 120, 120, 0.4);
        int nextIndex = i + 1;
        if (nextIndex == poiDrone.length ||
            dronePoint.areaId != poiDrone[nextIndex].areaId) {
          polygons.add(
            Polygon(
              points: List.from(pointsDrone),
              borderColor: borderColor,
              borderStrokeWidth: borderWidth,
              color: color,
            ),
          );
          pointsDrone.clear();
        } else {
          pointsDrone.add(dronePoint.coordinates);
        }
      }
    }
    return PolygonLayerOptions(
      polygons: polygons,
    );
  }

  addTelemetry(DocumentSnapshot doc) {
    droneTelemetry = Telemetry.from(doc);
    droneMarker = TargetStepMarker(
            LatLng(droneTelemetry.latitude, droneTelemetry.longitude),
            context,
            intervention.uid,
            "",
            MapResources.DRONE_PATH,
            0)
        .getDrone((droneTelemetry.battery * 100).floor());
    statefulMapController.addMarker(marker: droneMarker, name: "droneMarker");
  }

  List<Marker> getMarker(List<PoiMarker> poi) {
    List<Marker> markers = [];
    poi.forEach((p) => markers.add(p.getMarker(this.context)));
    return markers;
  }

  Marker getMarkerIntervention() {
    return Marker(
      width: 30.0,
      height: 30.0,
      point: mapCenter,
      builder: (context) => IconButton(
        padding: EdgeInsets.all(0),
        alignment: Alignment.topCenter,
        icon: Icon(Icons.location_on),
        color: ColorRessources.darkColor,
        iconSize: 30.0,
        onPressed: () {
          showDialog(
            context: context,
            child: InterventionDialog(intervention),
          );
        },
      ),
    );
  }

  addElementOnMap(LatLng point) {
    if(iconToMove != null){
      iconToMove.moveTo(point);
      deselectMoveMarker();
    }
    else if (MapService.targetMeans != null) {
      putMeansInMap(point);
    }
    else {
      if (MapService.targetMarkerType != null) {
        switch (MapService.targetMarkerType) {
          case MapResources.NONE: break;
          case MapResources.MEANS:
            createMeans(point);
            break;
          case MapResources.EXTERNAL_MEANS:
            createExternalMeans(point);
            break;
          case MapResources.RESOURCE:
            createResources(point);
            break;
          case MapResources.NON_CLOSED:
            createTravelStepNonClosed(point);
            break;
          case MapResources.CLOSED:
            createTravelStepClosed(point);
            break;
          case MapResources.ZONE_INC:
            createTravelStepZone(point, MapResources.ZONE_INC);
            break;
          case MapResources.ZONE_EX:
            createTravelStepZone(point, MapResources.ZONE_EX);
            break;
          case MapResources.VIDEO:
            break;
        }
      }
      MapService.targetMeans = null;
    }
  }

  putMeansInMap(LatLng point) {
    _selectedCoordinates = point;
    MapService.targetMeans.poiIcon.isOnMap = true;
    MapService.targetMeans.poiIcon.coordinates = point;
    MapService.targetMeans.poiIcon.isActive = false;
    // maj de l'icone en bdd
    PoiIconService()
        .updatePoiIcon(MapService.targetMeans.poiIcon)
        .then((onValue) {
      MapService.targetMeans = null;
    });
  }

  createTravelStepNonClosed(LatLng point) {
    points.add(point);
    TargetStepMarker markerStep = TargetStepMarker(point, context,
        intervention.uid, "", MapResources.POI_DRONE_DRAW_PATH, 0);
    statefulMapController.addMarker(
        name: 'pointOfInterest' + points.length.toString(),
        marker: markerStep.getMarker());
    statefulMapController.addLine(
        width: 2,
        color: Colors.black,
        isDotted: true,
        name: 'dronePath' + points.length.toString(),
        points: points);
    savedPoints.clear();
    clearedPoints.clear();
    refreshWidget();
  }

  bool validateTravelStepNonClosed() {
    bool isValidated = false;
    if (statefulMapController.markers.length >= 2) {
      isValidated = true;
      PoiDroneService().clearPreviousPoiDrone(intervention.uid).then((_) {
        if (statefulMapController.namedMarkers.containsKey("droneMarker")) {
          statefulMapController.removeMarker(name: "droneMarker");
        }
        for (int step = 0;
            step < statefulMapController.markers.length;
            step++) {
          LatLng latlng = statefulMapController.markers[step].point;
          PoiDroneService().insertPoiDrone(intervention.uid,
              new PoiDrone("", step, latlng, MapResources.NON_CLOSED, 0, 0));
        }
      }).then((onValue) {
        clearAllTravelStep();
        MapService.targetMarkerType = MapResources.NONE;
        refreshWidget();
      });
    }
    return isValidated;
  }

  createTravelStepClosed(LatLng point) {
    points.add(point);
    TargetStepMarker markerStep = TargetStepMarker(point, context,
        intervention.uid, "", MapResources.POI_DRONE_DRAW_PATH, 0);
    statefulMapController.addMarker(
        name: 'pointOfInterest' + points.length.toString(),
        marker: markerStep.getMarker());
    statefulMapController.addLine(
        width: 2,
        color: Colors.black,
        isDotted: true,
        name: 'dronePath',
        points: points);
    savedPoints.clear();
    clearedPoints.clear();
  }

  createTravelStepZone(LatLng point, String zoneType) {
    points.add(point);
    TargetStepMarker markerStep = TargetStepMarker(point, context,
        intervention.uid, "", MapResources.POI_DRONE_DRAW_PATH, 0);
    statefulMapController.addMarker(
        name: 'pointOfInterest' + areaId.toString() + points.length.toString(),
        marker: markerStep.getMarker());
    statefulMapController.addPolygon(
        borderWidth: 2,
        borderColor: Colors.black,
        color: zoneType == MapResources.ZONE_EX
            ? Color.fromRGBO(139, 0, 0, 0.4)
            : Color.fromRGBO(120, 120, 120, 0.4),
        name: 'droneZone' + areaId.toString(),
        points: List.from(points));
    savedPoints.clear();
  }

  validateTravelStepClosed() {
    bool isValidated = false;

    if (points.length >= 3) {
      isValidated = true;
      PoiDroneService().clearPreviousPoiDrone(intervention.uid).then((_) {
        if (statefulMapController.namedMarkers.containsKey("droneMarker"))
          statefulMapController.removeMarker(name: "droneMarker");

        points.add(points[0]);
        for (int step = 0; step < points.length; step++) {
          LatLng latlng = points[step];
          PoiDroneService().insertPoiDrone(intervention.uid,
              new PoiDrone("", step, latlng, MapResources.CLOSED, 0, 0));
        }
      }).then((onValue) {
        clearAllTravelStep();
        MapService.targetMarkerType = MapResources.NONE;
        refreshWidget();
      });
    }
    return isValidated;
  }

  validateTravelStepZone() {
    beginNextArea();
    PoiDroneService().clearPreviousPoiDrone(intervention.uid).then((_) {
      if (statefulMapController.namedMarkers.containsKey("droneMarker"))
        statefulMapController.removeMarker(name: "droneMarker");

      pointsToPushInDb.forEach((areaIdVal, currentList) {
        String zoneType =
            areaIdVal == 0 ? MapResources.ZONE_INC : MapResources.ZONE_EX;
        for (int step = 0; step < currentList.length; step++) {
          LatLng latlng = currentList[step];
          PoiDroneService().insertPoiDrone(intervention.uid,
              new PoiDrone("", step, latlng, zoneType, areaIdVal, 0));
        }
      });
    }).then((onValue) {
      clearAllTravelStep();
      MapService.targetMarkerType = MapResources.NONE;
      refreshWidget();
    });
    return true;
  }

  undoTravelStep() {
    if (clearedPoints.isNotEmpty) {
      for (LatLng point in clearedPoints) {
        points.add(point);
        TargetStepMarker markerStep = TargetStepMarker(point, context,
            intervention.uid, "", MapResources.POI_DRONE_DRAW_PATH, 0);
        statefulMapController.addMarker(
            name: 'pointOfInterest' + points.length.toString(),
            marker: markerStep.getMarker());
        statefulMapController.addLine(
            width: 2,
            color: Colors.black,
            isDotted: true,
            name: 'dronePath' + points.length.toString(),
            points: points);
      }
      clearedPoints.clear();
    } else if (points.isNotEmpty) {
      statefulMapController.markers.removeLast();
      statefulMapController.removeLine('dronePath' + points.length.toString());
      savedPoints.add(points.removeLast());
      refreshWidget();
    }
  }

  redoTravelStep() {
    if (savedPoints.isNotEmpty) {
      points.add(savedPoints.removeLast());
      TargetStepMarker markerStep = TargetStepMarker(points[points.length - 1],
          context, intervention.uid, "", MapResources.POI_DRONE_DRAW_PATH, 0);
      statefulMapController.addMarker(
          name: 'pointOfInterest' + points.length.toString(),
          marker: markerStep.getMarker());
      statefulMapController.addLine(
          width: 2,
          color: Colors.black,
          isDotted: true,
          name: 'dronePath' + points.length.toString(),
          points: points);
      refreshWidget();
    }
  }

  clearTravelStep({clearPolygon: true}) {
    savedPoints.clear();
    Iterable<LatLng> iterable = points;
    clearedPoints.addAll(iterable);
    statefulMapController.namedLines.clear();
    points.clear();
    if (clearPolygon) {
      areaId = 0;
      statefulMapController.markers.clear();
      statefulMapController.namedPolygons.clear();
      pointsToPushInDb.clear();
      if (MapService.targetMarkerType == MapResources.ZONE_EX) {
        MapService.targetMarkerType = MapResources.ZONE_INC;
      }
    }
    refreshWidget();
  }

  clearAllTravelStep() {
    areaId = 0;
    savedPoints.clear();
    clearedPoints.clear();
    points.clear();
    pointsToPushInDb.clear();
    statefulMapController.markers.clear();
    statefulMapController.namedLines.clear();
    statefulMapController.namedPolygons.clear();
    refreshWidget();
  }

  clearCurrentTravelStepLayer() {
    PoiDroneService().clearPreviousPoiDrone(intervention.uid).then((onValue) {
      refreshWidget();
    });
  }

  createMeans(LatLng point) {
    MapService.targetMarkerType = MapResources.NONE;
    _selectedCoordinates = point;
    List<SimpleDialogOption> items = [];
    Firefighter.values.forEach((f) => items.add(
          SimpleDialogOption(
            onPressed: () {
              _onSelectedCreateMeans(EnumToString.parse(f));
              Navigator.popUntil(context, ModalRoute.withName(Sitac.routeName));
            },
            child: Text(EnumToString.parse(f)),
          ),
        ));
    showDialog(
      context: context,
      builder: (context) => SimpleDialog(
        title: const Text('Type du moyen'),
        children: <Widget>[...items],
      ),
    );
  }

  createExternalMeans(LatLng point) {
    MapService.targetMarkerType = MapResources.NONE;
    PoiIcon newIcon;
    _selectedCoordinates = point;
    DocumentReference poiIconsDocument =
        PoiIconService().getPoiIconsDocument(intervention.uid);
    PoiIconService()
        .insertPoiIcon(
            poiIconsDocument,
            newIcon = PoiIcon(
                null,
                intervention.uid,
                null,
                _selectedCoordinates,
                convertColorToCategory(
                    convertStringToColor(MapService.targetColor)),
                MapService.targetAssetPath,
                null,
                " ",
                " ",
                false,
                true))
        .then((p) =>
            {meansPoiMarkers.add(ExternalMeansPoiMarker(newIcon, moveMarker))})
        .whenComplete(refreshWidget);
  }

  createResources(LatLng point) {
    MapService.targetMarkerType = MapResources.NONE;
    PoiIcon newIcon;
    _selectedCoordinates = point;
    DocumentReference poiIconsDocument =
        PoiIconService().getPoiIconsDocument(intervention.uid);
    PoiIconService()
        .insertPoiIcon(
            poiIconsDocument,
            newIcon = PoiIcon(
                null,
                intervention.uid,
                null,
                _selectedCoordinates,
                convertColorToCategory(
                    convertStringToColor(MapService.targetColor)),
                MapService.targetAssetPath,
                null,
                null,
                null,
                null,
                true))
        .then((p) =>
            {meansPoiMarkers.add(ResourcesPoiMarker(newIcon, moveMarker))})
        .whenComplete(refreshWidget);
  }

  void _onSelectedCreateMeans(String value) {
    PoiIcon newIcon;
    Means newMeans = Means(
        null,
        value,
        EnumToString.fromString(Firefighter.values, value),
        DateTime.now(),
        null,
        null,
        null,
        Location.CRM,
        convertColorToCategory(convertStringToColor(MapService.targetColor)),
        null,
        intervention.uid);
    CollectionReference meansCollection =
        MeansService().getMeansCollection(intervention.uid);
    DocumentReference poiIconsDocument =
        PoiIconService().getPoiIconsDocument(intervention.uid);
    meansCollection.add(newMeans.toJson()).then((m) => {
          PoiIconService()
              .insertPoiIcon(
                  poiIconsDocument,
                  newIcon = PoiIcon(
                      null,
                      intervention.uid,
                      m.documentID,
                      _selectedCoordinates,
                      newMeans.category,
                      MapResources.assetPath +
                          'moyen-prevu' +
                          MapResources.assetExtention,
                      newMeans.type,
                      EnumToString.parse(newMeans.type) + "",
                      "",
                      false,
                      true))
              .then((p) =>
                  {meansPoiMarkers.add(MeansPoiMarker(newIcon, moveMarker))})
              .whenComplete(refreshWidget),
          MeansService()
              .insertMeanDemand(intervention.uid, m.documentID, DateTime.now()),
        });
  }

  getPersistentPoiIcons() {
    MapService()
        .getPersistentPoiMarker(persistentPoiMarkers)
        .whenComplete(() => {mapLoaded = true, refreshWidget()});
  }

  void setIcons(List<PoiIcon> icons) {
    refreshMarkerList();
    icons.forEach((icon) => {
          addIconsInList(icon, meansPoiMarkers, externalMeansPoiMarkers,
              resourcesPoiMarkers)
        });
    refreshWidget();
  }

  void setIconsDrone(List<PoiDrone> iconsDrone) {
    refreshMarkerList();
    poiDrone.clear();
    poiDrone.addAll(iconsDrone);
    refreshWidget();
  }

  /// Function to add the icon in the right list : Means, externalMeans and resources
  /// must have isOnMap = true
  /// if idMeans != null then means
  /// else if isActive != null externalMeans
  /// else resource
  addIconsInList(PoiIcon icon, List<PoiMarker> means,
      List<PoiMarker> externalMeans, List<PoiMarker> resources) {
    if (icon.idMeans != null && icon.isOnMap)
      means.add(MeansPoiMarker(icon, moveMarker));
    else if (icon.isActive != null && (icon.isOnMap != null && icon.isOnMap))
      externalMeans.add(ExternalMeansPoiMarker(icon, moveMarker));
    else if (icon.isOnMap) resources.add(ResourcesPoiMarker(icon, moveMarker));
  }

  void refreshMarkerList() {
    meansPoiMarkers = [];
    externalMeansPoiMarkers = [];
    resourcesPoiMarkers = [];
  }

  refreshWidget() {
    if (!mounted) return;
    setState(() {});
  }

  bool beginNextArea() {
    bool isValidated = false;
    if (points.length >= 3) {
      points.add(points[0]);
      MapService.targetMarkerType = MapResources.ZONE_EX;
      pointsToPushInDb[areaId] = List.from(points);
      areaId++;
      statefulMapController.markers.clear();
      clearTravelStep(clearPolygon: false);
    }
    return isValidated;
  }
}
