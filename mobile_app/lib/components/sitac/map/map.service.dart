import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sith/components/sitac/map/map.resources.dart';
import 'package:sith/components/sitac/map/poiMarker/persistentPoiMarker.component.dart';
import 'package:sith/components/sitac/meansRepresentation/meansRepresentation.component.dart';
import 'package:sith/domain/persistentPoi.model.dart';
import 'package:sith/domain/poiIcon.model.dart';
import 'package:sith/services/firestore.service.dart';
import 'package:sith/services/persistentPoi.service.dart';

class MapService {
  static CollectionReference persistentPoiCollectionReference;
  static CollectionReference poiIconsCollectionReference;

  static final MapService _singleton = initiate();

  static String targetMarkerType;
  static String targetAssetPath;
  static String targetColor;

  static MeansRepresentation targetMeans;
  // static bool isMeans = false;

  factory MapService() {
    return _singleton;
  }

  /// Constructeur privé interne
  MapService._internal();

  static initiate() {
    persistentPoiCollectionReference = FireConn().getPersistentPoiCollection();
    return MapService._internal();
  }

  Future getPersistentPoiMarker(List<PersistentPoiMarker> list) async {
    await PersistentPoiService()
      .getPersistentPoi()
      .then((pPois) => pPois.documents.forEach((document) => {
        list.add(PersistentPoiMarker(PersistentPoi.from(document), null)),
      }));
  }

  List<PoiIcon> getResourcesPOI() {
    return [];
  }

  setTargetParameters(String targetMarkerType, String targetAssetPath){
    MapService.targetMarkerType = targetMarkerType;
    MapService.targetAssetPath = targetAssetPath;
    MapService.targetMeans = null ;
  }

  setTargetColor(String targetColor){
    MapService.targetColor = targetColor;
  }

  setTargetMeans(MeansRepresentation selectedMeans) {
    MapService.targetMarkerType = MapResources.NONE;
    MapService.targetAssetPath = targetAssetPath;
    MapService.targetMeans = selectedMeans;
  }

  resetTargetParameters(){
    MapService.targetMarkerType = MapResources.NONE;
    MapService.targetAssetPath = null;
    MapService.targetColor = null;
  }
}
