import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sith/services/firestore.service.dart';
import 'package:sith/services/storage.service.dart';

import 'bottomSheet.view.dart';

// ignore: must_be_immutable
class BottomSheetSitac extends StatefulWidget {
  String _interventionUid;
  String _poiDroneUid;

  BottomSheetSitac(this._interventionUid, this._poiDroneUid);

  @override
  State<StatefulWidget> createState() =>
      BottomSheetState(_interventionUid, _poiDroneUid);
}

class BottomSheetState extends State<BottomSheetSitac> {
  String interventionUid;
  String poiDroneUid;
  int current = 0;

  BottomSheetState(this.interventionUid, this.poiDroneUid);

  @override
  Widget build(BuildContext context) {
    return BottomSheetView(this).buildView();
  }

  refreshWidget(int index) {
    setState(() {
      current = index;
    });
  }

  Future<Widget> getImage(String fileName) async {
    Image m;
    await FireStorage().getImage(interventionUid, fileName).then((urlPath) {
      m = Image.network(urlPath.toString());
    });
    return m;
  }
}
