import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/sitac/drone/droneTravelStepMarker.component.dart';
import 'package:sith/components/sitac/poiDrone.service.dart';
import 'package:sith/components/utils/dateConverter.util.dart';

import 'bottomSheet.component.dart';

class BottomSheetView {
  final BottomSheetState waypointDialogState;

  BottomSheetView(this.waypointDialogState);

  Widget buildView() {
    return Container(child: getCarousel());
  }

  Future<bool> _onbackPressed() {
    TargetStepMarker.isPopupVisible = false;
    return Future.value(true);
  }

  Widget getCarousel() {
    return StreamBuilder<QuerySnapshot>(
        stream: PoiDroneService().getImages(waypointDialogState.interventionUid,
            waypointDialogState.poiDroneUid),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData || snapshot.data.documents.isEmpty) {
            return WillPopScope(
              onWillPop: () => _onbackPressed(),
              child: Container(
                child: Center(child: CircularProgressIndicator()),
              ),
            );
          }
          return WillPopScope(
            onWillPop: () => _onbackPressed(),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                    flex: 3,
                    child: CarouselSlider.builder(
                        enableInfiniteScroll: false,
                        enlargeCenterPage: true,
                        aspectRatio: 2.0,
                        itemCount: snapshot.data.documents.length,
                        onPageChanged: (index) {
                          waypointDialogState.refreshWidget(index);
                        },
                        itemBuilder: (context, index) {
                          return _imageCardBuilder(
                              snapshot.data.documents[index]);
                        }),
                  ),
                  Flexible(child: _indicators(snapshot.data.documents)),
                ]),
          );
        });
  }

  FutureBuilder _imageCardBuilder(DocumentSnapshot urlPath) {
    return FutureBuilder(
        future: waypointDialogState.getImage(urlPath.data['name']),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              child: _imageCard(snapshot.data, urlPath.data['name']),
            );
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return _waitingIndicator(context);
          }
          return Container();
        });
  }

  Stack _imageCard(dynamic imageWidget, String fileName) {
    return Stack(children: <Widget>[
      imageWidget,
      Positioned(
        bottom: 0.0,
        left: 0.0,
        right: 0.0,
        child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(200, 0, 0, 0),
                  Color.fromARGB(0, 0, 0, 0)
                ],
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
              ),
            ),
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            child: _imageText(fileName)),
      )
    ]);
  }

  Text _imageText(String fileName) {
    return Text(
      DateConverter.formatDateFromFileName(fileName),
      style: TextStyle(
        color: Colors.white,
        fontSize: 20.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Row _indicators(List<DocumentSnapshot> items) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: map<Widget>(
        items,
        (index, url) {
          return _sliderIndicator(index);
        },
      ),
    );
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  Container _sliderIndicator(int index) {
    return Container(
      width: 8.0,
      height: 8.0,
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: waypointDialogState.current == index
              ? Color.fromRGBO(0, 0, 0, 0.9)
              : Color.fromRGBO(0, 0, 0, 0.4)),
    );
  }

  Container _waitingIndicator(BuildContext context) {
    return Container(height: 20, width: 20, child: CircularProgressIndicator());
  }
}
