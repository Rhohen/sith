import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/means/means.service.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/domain/enum/category.model.dart';
import 'package:sith/domain/enum/location.model.dart';
import 'package:sith/domain/means.model.dart';

import 'means-datatable.view.dart';

class MeansDataTablePage extends StatefulWidget {
  static const routeName = '/MeansDataTablePage';

  final String _interventionUid;

  MeansDataTablePage(this._interventionUid);

  @override
  State<StatefulWidget> createState() =>
      new MeansDataTablePageState(this._interventionUid);
}

class MeansDataTablePageState extends State<MeansDataTablePage> {
  Means meanToAdd;
  String interventionUid;

  Flushbar flushbar;

  MeansDataTablePageState(this.interventionUid);

  @override
  void initState() {
    flushbar = Flushbar();
    meanToAdd = new Means(null, null, null, null, null, null, null,
        Location.CRM, Category.DEFAUT, null, null);
    super.initState();
  }

  void refreshWidget() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return new MeansDatatableView(this).buildView();
  }

  Stream<QuerySnapshot> getMeans() {
    return MeansService().getMeans(interventionUid);
  }

  void addMean() {
    if (this.meanToAdd.type == null) {
      showFlushMessage(FlutterI18n.translate(context, "addmeans_error"),
          ColorRessources.addMeansErrorColor, Duration(seconds: 3));
    } else {
      this.meanToAdd.requestHour = new DateTime.now();
      this.meanToAdd.idIntervention = interventionUid;
      DocumentReference documentReference =
          MeansService().createMean(interventionUid);
      Means meanTempo = meanToAdd;
      this.refreshWidget();

      meanToAdd = new Means(null, null, null, null, null, null, null,
          Location.CRM, Category.DEFAUT, null, null);

      MeansService().insertMean(documentReference, meanTempo).then((onValue) {
        MeansService()
          .insertMeanDemand(interventionUid, documentReference.documentID,
              meanTempo.requestHour)
          .then((onValue) {
            this.refreshWidget();
        });
      });
    }
  }

  void updateMean(Means means) {
    MeansService().updateMean(interventionUid, means);
    PoiIconService().updatePoiIconFromMeans(means);
  }

  showFlushMessage(String message, Color color, Duration duration) {
    flushbar.dismiss().then((onValue) {
      flushbar = Flushbar(
        flushbarPosition: FlushbarPosition.TOP,
        icon: Icon(
          Icons.info_outline,
          size: 28.0,
          color: color,
        ),
        title: "Erreur",
        leftBarIndicatorColor: color,
        message: "$message",
        duration: duration,
        isDismissible: true,
        dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      );
      flushbar.show(context);
    });
  }
}
