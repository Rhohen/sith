import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/colorUtils.dart';
import 'package:sith/components/utils/dateConverter.util.dart';
import 'package:sith/domain/enum/category.model.dart';
import 'package:sith/domain/enum/firefighter.model.dart';
import 'package:sith/domain/means.model.dart';

import 'means-datatable.component.dart';

class MeansDatatableView {
  MeansDataTablePageState meansDataTablePage;

  MeansDatatableView(_meanDataTablePage) {
    meansDataTablePage = _meanDataTablePage;
  }

  Widget buildView() {
    return new StreamBuilder<QuerySnapshot>(
        stream: meansDataTablePage.getMeans(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData)
            return new Text(FlutterI18n.translate(context, "loading"));
          return SingleChildScrollView(
              child: Table(
            border: TableBorder.all(),
            children: [_header(), _addMeanRow(), ..._listOfRows(snapshot.data)],
            columnWidths: {5: FlexColumnWidth(1.5)},
          ));
        });
  }

  TableRow _header() {
    return TableRow(children: [
      _addTitle(FlutterI18n.translate(meansDataTablePage.context, "name")),
      _addTitle(FlutterI18n.translate(meansDataTablePage.context, "request")),
      _addTitle(FlutterI18n.translate(meansDataTablePage.context, "departure")),
      _addTitle(FlutterI18n.translate(meansDataTablePage.context, "engage")),
      _addTitle(FlutterI18n.translate(meansDataTablePage.context, "release")),
      _addTitle(FlutterI18n.translate(meansDataTablePage.context, "category")),
      _addTitle(FlutterI18n.translate(meansDataTablePage.context, "action")),
    ]);
  }

  Text _addTitle(String text) {
    return Text(text,
        textAlign: TextAlign.center,
        style: TextStyle(fontWeight: FontWeight.bold));
  }

  List<TableRow> _listOfRows(QuerySnapshot snapshot) {
    List<TableRow> newList =
        snapshot.documents.map((DocumentSnapshot documentSnapshot) {
      return _createTableRow(
          Means.from(documentSnapshot.documentID, documentSnapshot.data));
    }).toList();
    return newList;
  }

  TableRow _createTableRow(Means mean) {
    return new TableRow(
        children: [
          _addText(mean.name != null
              ? mean.name
              : mean.type != null ? EnumToString.parse(mean.type) : ""),
          _addText(mean.requestHour != null
              ? DateFormat('dd/MM kk:mm').format(mean.requestHour)
              : ""),
          _addText(mean.departureHour != null
              ? DateFormat('dd/MM kk:mm').format(mean.departureHour)
              : ""),
          _addText(mean.engageHour != null
              ? DateFormat('dd/MM kk:mm').format(mean.engageHour)
              : ""),
          _addText(mean.releaseHour != null
              ? DateFormat('dd/MM kk:mm').format(mean.releaseHour)
              : ""),
          _changeCategory(mean),
          _showActions(mean),
        ],
        decoration: mean.isValid != false
            ? BoxDecoration(
                color: (mean.category == Category.DEFAUT)
                    ? ColorRessources.greyColor
                    : convertCategoryEnumToColor(mean.category))
            : BoxDecoration(
                color: ColorRessources.greyColor,
                gradient: new LinearGradient(
                    colors: [
                      convertCategoryEnumToColor(mean.category),
                      ColorRessources.greyColor
                    ],
                    begin: Alignment.centerRight,
                    end: new Alignment(0.98, 0.0),
                    tileMode: TileMode.mirror)));
  }

  Center _addText(String text) {
    return Center(
        heightFactor: 2.9, child: Text(text, textAlign: TextAlign.right));
  }

  Widget _changeCategory(Means mean) {
    return DropdownButton<Category>(
      value: mean.category,
      onChanged: (value) {
        mean.category = value;
        meansDataTablePage.updateMean(mean);
      },
      items: Category.values.map((Category category) {
        return DropdownMenuItem<Category>(
            value: category, child: Text(EnumToString.parse(category)));
      }).toList(),
      //isDense: true,
      isExpanded: true,
    );
  }

  _showActions(Means mean) {
    if (mean.isValid == null) {
      return Center(
          heightFactor: 2.9,
          child: Text(
              FlutterI18n.translate(meansDataTablePage.context, "waiting"),
              textAlign: TextAlign.center));
    } else if (mean.isValid) {
      return Center(
          heightFactor: 2.9,
          child: Text(
              FlutterI18n.translate(meansDataTablePage.context, "valid"),
              textAlign: TextAlign.center));
    } else {
      return Center(
          heightFactor: 2.9,
          child: Text(
              FlutterI18n.translate(meansDataTablePage.context, "refuse"),
              textAlign: TextAlign.center));
    }
  }

  TableRow _addMeanRow() {
    return TableRow(children: [
      _selectTypeFirefighter(),
      Text(""),
      Text(""),
      Text(""),
      Text(""),
      Center(
          heightFactor: 2.9,
          child: Text(
            EnumToString.parse(meansDataTablePage.meanToAdd.category),
            textAlign: TextAlign.center,
          )),
      _addMean(),
    ]);
  }

  Widget _selectTypeFirefighter() {
    return DropdownButton<Firefighter>(
      value: meansDataTablePage.meanToAdd.type,
      onChanged: (value) {
        meansDataTablePage.meanToAdd.type = value;
        meansDataTablePage.refreshWidget();
      },
      items: Firefighter.values.map((Firefighter firefighter) {
        return DropdownMenuItem<Firefighter>(
            value: firefighter, child: Text(EnumToString.parse(firefighter)));
      }).toList(),
      //isDense: true,
      isExpanded: true,
    );
  }

  Widget _addMean() {
    return Center(
        heightFactor: 1.9,
        child: InkWell(
          child: Icon(Icons.add),
          onTap: meansDataTablePage.addMean,
        ));
  }
}
