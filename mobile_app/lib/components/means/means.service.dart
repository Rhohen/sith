import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/domain/means.model.dart';
import 'package:sith/services/firestore.service.dart';

class MeansService {
  static CollectionReference meansCollectionReference;
  static CollectionReference meansDemandCollectionReference;

  static final MeansService _singleton = initiate();

  factory MeansService() {
    return _singleton;
  }

  /// Constructeur privé interne
  MeansService._internal();

  static initiate() {
    return MeansService._internal();
  }

  Stream<QuerySnapshot> getMeans(String interventionUid) {
    meansCollectionReference = FireConn()
        .getInterventionsCollection()
        .document(interventionUid)
        .collection("moyens");
    return meansCollectionReference
        .orderBy('requestHour', descending: true)
        .snapshots();
  }

  Stream<QuerySnapshot> getMeansDemands() {
    meansDemandCollectionReference = FireConn().getMeanDemandsCollection();
    return meansDemandCollectionReference
        .orderBy('requestHour', descending: true)
        .snapshots();
  }

  DocumentReference createMean(String interventionUid) {
    meansCollectionReference = FireConn()
        .getInterventionsCollection()
        .document(interventionUid)
        .collection("moyens");
    return meansCollectionReference.document();
  }

  Future insertMean(DocumentReference document, Means mean) {
    Means meansTmp = mean;
    meansTmp.uid = document.documentID;
    PoiIconService().insertPoiIcon(
        PoiIconService().getPoiIconsDocument(mean.idIntervention),
        PoiIconService().getPoiIconFromMeans(meansTmp));
    return document.setData(mean.toJson());
  }

  Future getMean(String interventionUid, String meanUid) {
    meansCollectionReference = FireConn()
        .getInterventionsCollection()
        .document(interventionUid)
        .collection("moyens");
    return meansCollectionReference.document(meanUid).get();
  }

  Future insertMeanDemand(
      String interventionUid, String meanUid, DateTime requestHour) {
    meansDemandCollectionReference = FireConn().getMeanDemandsCollection();
    return meansDemandCollectionReference.document().setData({
      "idIntervention": interventionUid,
      "idMean": meanUid,
      "requestHour": requestHour
    });
  }

  Future updateMean(String interventionUid, Means mean) {
    meansCollectionReference = FireConn()
        .getInterventionsCollection()
        .document(interventionUid)
        .collection("moyens");
    return meansCollectionReference
        .document(mean.uid)
        .updateData(mean.toJson());
  }

  Future deleteMeansDemand(String meanDemandUid) {
    meansDemandCollectionReference = FireConn().getMeanDemandsCollection();
    return meansDemandCollectionReference.document(meanDemandUid).delete();
  }

  Future deleteMeansDemandByInterventionId(String interventionUid) {
    meansDemandCollectionReference = FireConn().getMeanDemandsCollection();
    return meansDemandCollectionReference
        .where('idIntervention', isEqualTo: interventionUid)
        .getDocuments()
        .then((onValue) {
      onValue.documents.forEach((demand) {
        demand.reference.delete();
      });
    });
  }

  CollectionReference getMeansCollection(String interventionUid) {
    return FireConn().getInterventionsCollection().document(interventionUid).collection("moyens");
  }

  releaseMeans(String idIntervention) async {
    Means mean;
    meansCollectionReference = FireConn().getInterventionsCollection()
        .document(idIntervention)
        .collection("moyens");
    await meansCollectionReference
        .getDocuments()
        .then((querySnapshot) =>
    {
      querySnapshot.documents.forEach((document) =>
      {
        mean = Means.from(document.documentID, document.data),
        mean.releaseHour = DateTime.now(),
        updateMean(idIntervention, mean)
      })
    });
  }

  Future<QuerySnapshot> fetchAllMeans(String interventionId) {
    return FireConn()
        .getInterventionsCollection()
        .document(interventionId)
        .collection("moyens").getDocuments();
  }
}
