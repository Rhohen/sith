import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:sith/components/intervention/intervention.service.dart';
import 'package:sith/domain/intervention.model.dart';
import 'package:sith/domain/means.model.dart';

import '../means.service.dart';
import 'means-demand-card.view.dart';
import 'means-demand-list.component.dart';

class MeansDemandListView {
  MeansDemandListPageState meansDemandPage;
  BuildContext context;

  MeansDemandListView(this.meansDemandPage, this.context);

  StreamBuilder buildView() {
    return StreamBuilder<QuerySnapshot>(
      stream: meansDemandPage.getMeansDemandsSnapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData)
          return new Text(FlutterI18n.translate(context, "loading"));
        return new ListView(
            children: snapshot.data.documents.map((DocumentSnapshot document) {
              return Card(
                  child: FutureBuilder<Widget>(
                      future: _getMeanDemandCard(document),
                      initialData: Center(child: CircularProgressIndicator()),
                      builder: (BuildContext context,
                          AsyncSnapshot<Widget> meanDemandCard) {
                        if (!meanDemandCard.hasData)
                          return Center(child: CircularProgressIndicator());
                        return meanDemandCard.data;
                      }));
            }).toList());
      },
    );
  }

  Future<MeansDemandCard> _getMeanDemandCard(
      DocumentSnapshot documentSnapshot) async {
    Intervention intervention;
    Means mean;
    intervention = Intervention.from(await InterventionService()
        .getIntervention(documentSnapshot.data['idIntervention']));
    DocumentSnapshot meanSnapshot = await MeansService()
        .getMean(intervention.uid, documentSnapshot.data['idMean']);
    mean = Means.from(documentSnapshot.data['idMean'], meanSnapshot.data);
    return MeansDemandCard(
        intervention, mean, documentSnapshot.documentID, meansDemandPage,
        context);
  }
}
