import 'dart:collection';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:sith/components/intervention/form/draggable/draggable.component.dart';
import 'package:sith/components/intervention/vehicules.service.dart';
import 'package:sith/components/means/means_demand_list/means-demand-list.component.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/components/utils/colorUtils.dart';
import 'package:sith/domain/enum/category.model.dart';
import 'package:sith/domain/enum/location.model.dart';
import 'package:sith/domain/intervention.model.dart';
import 'package:sith/domain/means.model.dart';
import 'package:sith/domain/vehicule.model.dart';

import '../means.service.dart';

class MeansDemandCard extends StatelessWidget {
  final Intervention _intervention;
  final Means _mean;
  final String _meanDemandUid;
  final MeansDemandListPageState _meansDemandList;
  final BuildContext _context;

  MeansDemandCard(this._intervention, this._mean, this._meanDemandUid,
      this._meansDemandList, this._context);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[_showMeanDemandTile()],
    );
  }

  CircleAvatar categoryCircle(Category category) {
    return CircleAvatar(backgroundColor: convertCategoryEnumToColor(category));
  }

  _showMeanDemandTile() {
    List<Widget> actions = [
      SlideAction(
        onTap: () {
          _meansDemandList.refuseMeanDemand(_meanDemandUid, _mean);
        },
        child: Container(
          decoration: BoxDecoration(
            color: ColorRessources.orangeColor,
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.priority_high, color: ColorRessources.whiteColor),
                Text(FlutterI18n.translate(_context, "refuse_demand"),
                    style: TextStyle(color: ColorRessources.whiteColor)),
              ],
            ),
          ),
        ),
      )
    ];

    return Slidable(
        actionPane: SlidableScrollActionPane(),
        enabled: true,
        closeOnScroll: false,
        secondaryActions: <Widget>[...actions],
        key: ObjectKey(0),
        child: ListTile(
          title: Text(EnumToString.parse(_mean.type) +
              (_intervention != null ? (" - " + _intervention.name) : "")),
          subtitle: Text(DateFormat('dd/MM kk:mm').format(_mean.requestHour)),
          trailing: categoryCircle(_mean.category),
          onTap: () {
            Map<String, List<Vehicule>> availableVehicules = new HashMap();

            VehiculesService()
                .getVehicules(EnumToString.parse(_mean.type))
                .then((QuerySnapshot snapshot) {
              snapshot.documents.forEach((vehicule) {
                Vehicule myVehicule =
                    Vehicule.from(vehicule.documentID, vehicule.data);
                String type = EnumToString.parse(myVehicule.type);
                if (!availableVehicules.containsKey(type))
                  availableVehicules[type] = new List();
                availableVehicules[type].add(myVehicule);
              });
              _showDialog(availableVehicules);
            });
          },
        ));
  }

  Future _showDialog(availableVehicules) {
    return showDialog(
        context: _context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text(FlutterI18n.translate(context, "validate_button")),
            children: <Widget>[
              Container(
                  height: max(MediaQuery.of(_context).size.height * 0.15, 150),
                  width: MediaQuery.of(_context).size.width * 0.4,
                  child: DraggableComponent(
                      availableVehicules, _validateVehicule, true)),
            ],
          );
        });
  }

  void _validateVehicule(DraggableComponentState componentState) {
    DateTime date;
    String interventionUid = _intervention.uid;
    Means mean;

    MeansService().getMean(interventionUid, _mean.uid).then((meanVal) {
      componentState.deployedVehicules.forEach((vehicule) => {
            date = DateTime.now(),
            mean = Means.from(_mean.uid, meanVal.data),
            mean.name = vehicule.name,
            mean.departureHour = date,
            mean.location = Location.CRM,
            mean.idIntervention = interventionUid,
            mean.category = vehicule.category,
            mean.isValid = true,
            MeansService().updateMean(interventionUid, mean).then((onValue) => {
                  vehicule.isAvailable = false,
                  vehicule.idIntervention = interventionUid,
                  VehiculesService()
                      .replaceVehicule(vehicule)
                      .then((onValue) => {
                            MeansService()
                                .deleteMeansDemand(_meanDemandUid)
                                .then((onValue) =>
                                    {Navigator.of(_context).maybePop()})
                          }),
                  PoiIconService().updatePoiIconFromMeans(mean),
                })
          });
    });
  }
}
