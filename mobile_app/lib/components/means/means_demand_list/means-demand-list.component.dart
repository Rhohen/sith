import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sith/components/means/means.service.dart';
import 'package:sith/components/sitac/poiIcons.service.dart';
import 'package:sith/domain/means.model.dart';
import 'package:sith/domain/poiIcon.model.dart';

import 'means-demand-list.view.dart';

class MeansDemandListPage extends StatefulWidget {
  static const routeName = '/MeansDemandListPage';

  @override
  State<StatefulWidget> createState() => new MeansDemandListPageState();
}

class MeansDemandListPageState extends State<MeansDemandListPage> {
  @override
  void initState() {
    super.initState();
  }

  void refreshWidget() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return new MeansDemandListView(this, context).buildView();
  }

  Stream<QuerySnapshot> getMeansDemandsSnapshots() {
    return MeansService().getMeansDemands();
  }

  Future refuseMeanDemand(String meanDemandUid, Means mean) async {
    mean.isValid = false;
    MeansService().deleteMeansDemand(meanDemandUid);
    PoiIcon icon = await  PoiIconService().getPoiIconOfMeans(mean.idIntervention, mean);
    PoiIconService().deleteIcon(icon);
    return MeansService().updateMean(mean.idIntervention, mean);
  }
}
