import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sith/components/means/means_datatable/means-datatable.component.dart';
import 'package:sith/components/sitac/sitac.component.dart';
import 'package:sith/domain/intervention.model.dart';
import 'package:sith/services/user.service.dart';

import 'components/home/home.component.dart';
import 'components/intervention/form/form.component.dart';
import 'components/intervention/intervention.component.dart';
import 'components/intervention/list/list.component.dart';
import 'components/login/login.component.dart';
import 'domain/vehicule.model.dart';

class Router {
  static PageTransition route(RouteSettings settings) {
    switch (settings.name) {
      case LoginPage.routeName:
        return PageTransition(
          child: LoginPage(),
          type: PageTransitionType.fade,
          settings: settings,
        );
        break;
      case InterventionsListPage.routeName:
        return PageTransition(
          child: InterventionsListPage(UserService()
              .getLocalUser()
              .isCodis),
          type: PageTransitionType.fade,
          settings: settings,
        );
        break;
      case InterventionFormPage.routeName:
        Map<String, List<Vehicule>> vehicules = settings.arguments;
        return PageTransition(
          child: InterventionFormPage(vehicules),
          type: PageTransitionType.fade,
          settings: settings,
        );
        break;
      case InterventionPage.routeName:
        Intervention intervention = settings.arguments;
        return PageTransition(
          child: InterventionPage(intervention),
          type: PageTransitionType.fade,
          settings: settings,
        );
        break;
      case MeansDataTablePage.routeName:
        String interventionUid = settings.arguments;
        return PageTransition(
          child: MeansDataTablePage(interventionUid),
          type: PageTransitionType.fade,
          settings: settings,
        );
        break;
      case HomePage.routeName:
        return PageTransition(
          child: HomePage(),
          type: PageTransitionType.fade,
          settings: settings,
        );
        break;
      case Sitac.routeName:
        Intervention intervention = settings.arguments;
        return PageTransition(
          child: Sitac(intervention),
          type: PageTransitionType.fade,
          settings: settings,
        );
        break;
      default:
        return null;
    }
  }
}
