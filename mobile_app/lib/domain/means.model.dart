import 'package:enum_to_string/enum_to_string.dart';
import 'package:sith/domain/enum/category.model.dart';
import 'package:sith/domain/enum/firefighter.model.dart';
import 'package:sith/domain/enum/location.model.dart';

class Means {
  String uid;
  String name;
  Firefighter type;
  DateTime requestHour;
  DateTime departureHour;
  DateTime engageHour;
  DateTime releaseHour;
  Location location;
  Category category;
  bool isValid;
  String idIntervention;

  

  Means(
    this.uid,
      this.name,
      this.type,
      this.requestHour,
      this.departureHour,
      this.engageHour,
      this.releaseHour,
      this.location,
      this.category,
      this.isValid,
      this.idIntervention);

  factory Means.from(String uid, Map<String, dynamic> json) {
    return new Means(
      uid,
      json['name'],
      EnumToString.fromString(Firefighter.values, json['type']),
      json['requestHour'] != null ? json['requestHour'].toDate() : json['requestHour'],
      json['departureHour'] != null ? json['departureHour'].toDate() : json['departureHour'],
      json['engageHour'] != null ? json['engageHour'].toDate() : json['engageHour'],
      json['releaseHour'] != null ? json['releaseHour'].toDate() : json['releaseHour'],
      EnumToString.fromString(Location.values, json['location']),
      EnumToString.fromString(Category.values, json['category']),
      json['isValid'],
      json['idIntervention']);
  }

  toJson() {
    return {
      "name": name,
      "type": EnumToString.parse(type),
      "requestHour": requestHour,
      "departureHour": departureHour,
      "engageHour": engageHour,
      "releaseHour": releaseHour,
      "location": EnumToString.parse(location),
      "category": EnumToString.parse(category),
      "isValid": isValid,
      "idIntervention": idIntervention
    };
  }

}
