import 'package:enum_to_string/enum_to_string.dart';
import 'package:sith/domain/enum/category.model.dart';
import 'package:sith/domain/enum/firefighter.model.dart';

class Vehicule {
  String uid;
  bool isAvailable;
  String idIntervention;
  Firefighter type;
  String name;
  Category category;

  Vehicule(this.uid, this.isAvailable, this.idIntervention, this.type,
      this.name, this.category);

  toJson() {
    return (idIntervention == null || idIntervention.isEmpty)
        ? {
            "isAvailable": isAvailable,
            "type": EnumToString.parse(type),
            "name": name,
      "category": EnumToString.parse(category),
          }
        : {
            "isAvailable": isAvailable,
            "type": EnumToString.parse(type),
            "idIntervention": idIntervention,
            "name": name,
      "category": EnumToString.parse(category),
          };
  }

  factory Vehicule.from(String uid, Map<String, dynamic> json) {
    return new Vehicule(
      uid,
      json['isAvailable'],
      json['idIntervention'],
      EnumToString.fromString(Firefighter.values, json['type']),
      json['name'],
      EnumToString.fromString(Category.values, json['category']),
    );
  }
}
