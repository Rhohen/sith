import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:latlong/latlong.dart';

class PoiDrone {
  String uid;
  int step;
  LatLng coordinates;
  String type;
  int areaId;
  int nbImages;

  PoiDrone(this.uid, this.step, this.coordinates, this.type, this.areaId,
      this.nbImages);

  factory PoiDrone.from(DocumentSnapshot document) {
    GeoPoint geo = document['coordinates']['geopoint'];
    return new PoiDrone(
        document.documentID,
        document['step'],
        LatLng(geo.latitude, geo.longitude),
        document['type'],
        document['areaId'],
        document['nbImages']);
  }

  toJson() {
    Geoflutterfire geo = Geoflutterfire();
    GeoFirePoint point = geo.point(
      latitude: coordinates.latitude,
      longitude: coordinates.longitude,
    );
    return {
      "step": step,
      "coordinates": point.data,
      "type": type,
      "areaId": areaId,
      "nbImages": 0
    };
  }
}
