import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:latlong/latlong.dart';
import 'package:sith/domain/enum/category.model.dart';
import 'package:sith/domain/enum/firefighter.model.dart';

class PoiIcon {

  String uid;
  String idIntervention;
  String idMeans;
  LatLng coordinates;
  Category category;
  String iconPath;
  Firefighter type;
  String label;
  String township;
  bool isActive;
  bool isOnMap;

  PoiIcon(
    this.uid,
    this.idIntervention,
    this.idMeans,
    this.coordinates,
    this.category,
    this.iconPath,
    this.type,
    this.label,
    this.township,
    this.isActive,
    this.isOnMap,
  );

  factory PoiIcon.from(DocumentSnapshot document) {
    GeoPoint geo = document['coordinates']['geopoint'];
    return new PoiIcon(
      document.documentID,
      document['idIntervention'],
      document['idMeans'],
      LatLng(geo.latitude, geo.longitude),
      EnumToString.fromString(Category.values, document['category']),
      document['iconPath'],
      EnumToString.fromString(Firefighter.values, document['type']),
      document['label'],
      document['township'],
      document['isActive'],
      document['isOnMap']
    );
  }

  toJson() {
    Geoflutterfire geo = Geoflutterfire();
    GeoFirePoint point = geo.point(latitude: coordinates.latitude, longitude: coordinates.longitude);
    return {
      "idIntervention": idIntervention,
      "idMeans": idMeans,
      "coordinates": point.data,
      "category": EnumToString.parse(category),
      "iconPath": iconPath,
      "type": EnumToString.parse(type),
      "label" : label,
      "township": township,
      "isActive": isActive,
      "isOnMap": isOnMap
    };
  }

}
