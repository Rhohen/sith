class User {
  String uid;
  bool isCodis;

  User(this.uid, this.isCodis);

  toJson() {
    return {"uid": uid, "isCodis": isCodis};
  }

  static emptyUser() {
    return new User("", false);
  }

  factory User.from(Map<String, dynamic> json) {
    return new User(
      json['uid'],
      json['isCodis'],
    );
  }
}
