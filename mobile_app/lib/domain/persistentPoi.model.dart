import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:latlong/latlong.dart';
import 'package:sith/components/sitac/map/map.resources.dart';

class PersistentPoi {
  LatLng coordinates;
  String iconPath;
  String color;

  PersistentPoi(this.coordinates, this.iconPath, this.color);

  factory PersistentPoi.from(DocumentSnapshot document) {
    GeoPoint geo = document['coordinates'];
    return new PersistentPoi(
        LatLng(geo.latitude, geo.longitude),
        MapResources.assetPath + document['iconFilename'] + MapResources.assetExtention,
        document['color']
    );
  }

}