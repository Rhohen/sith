import 'package:cloud_firestore/cloud_firestore.dart';

class Telemetry {
  double battery;
  double latitude;
  double longitude;

  Telemetry(this.battery, this.latitude, this.longitude);

  toJson() {
    return {"battery": battery, "latitude": latitude, "longitude": longitude};
  }

  static emptyTelemetry() {
    return new Telemetry(0, 0, 0);
  }

  factory Telemetry.from(DocumentSnapshot doc) {
    double val = 0.0;
    if (doc['battery'] is int)
      val = (doc['battery'] as int).toDouble();
    else
      val = doc['battery'];

    return new Telemetry(
      val,
      doc['latitude'],
      doc['longitude'],
    );
  }
}
