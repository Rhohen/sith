import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:latlong/latlong.dart';

import 'enum/codeSinistre.model.dart';

class Intervention {
  String uid;
  String name;
  CodeSinistre codeSinistre;
  String contact;
  String address;
  LatLng coordinate;
  bool closed;

  Intervention(this.uid, this.name, this.codeSinistre, this.contact,
      this.address, this.coordinate, this.closed);

  toJson() {
    return {
      "name": name,
      "codeSinistre": EnumToString.parse(codeSinistre),
      "contact": contact,
      "address": address,
      "latitude": (coordinate != null) ? coordinate.latitude : null,
      "longitude": (coordinate != null) ? coordinate.longitude : null,
      "closed": closed
    };
  }

  factory Intervention.from(DocumentSnapshot doc) {
    return Intervention(
        doc.documentID,
        doc['name'] ?? '',
        EnumToString.fromString(CodeSinistre.values, doc['codeSinistre']) ??
            null,
        doc['contact'] ?? 0,
        doc['address'] ?? '',
        doc['latitude'] != null
            ? LatLng(doc['latitude'], doc['longitude'])
            : null,
        doc['closed'] ?? false);
  }
}
