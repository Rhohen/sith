import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:sith/components/login/login.component.dart';
import 'package:sith/domain/user.model.dart';

import 'firestore.service.dart';

class UserService {
  static CollectionReference userCollectionReference;

  static final UserService _singleton = initiate();

  User _user = User.emptyUser();

  factory UserService() {
    return _singleton;
  }

  /// Constructeur privé interne
  UserService._internal();

  static initiate() {
    userCollectionReference = FireConn().getUsersCollection();
    return UserService._internal();
  }

  Future<User> getUser() {
    return getUserAuth().then((userAuth) {
      return userCollectionReference
          .document(userAuth.uid)
          .get()
          .then((userFire) {
        _user = User.from(userFire.data);
        return _user;
      });
    });
  }

  Future<void> switchRole() async {
    return getUserAuth().then((userAuth) {
      userCollectionReference
          .document(userAuth.uid)
          .updateData({'isCodis': !_user.isCodis});
      _user.isCodis = !_user.isCodis;
    });
  }

  Future insertUser(User user) {
    _user = user;
    return userCollectionReference.document(user.uid).setData(user.toJson());
  }

  Future updateUser(User user) {
    return userCollectionReference.document(user.uid).updateData(user.toJson());
  }

  Future<FirebaseUser> getUserAuth() {
    return FirebaseAuth.instance.currentUser();
  }

  logout(BuildContext context) {
    FirebaseAuth.instance.signOut().then((onValue) => {
          _user = User.emptyUser(),
          Navigator.pushNamedAndRemoveUntil(context, LoginPage.routeName,
              ModalRoute.withName(LoginPage.routeName))
        });
  }

  User getLocalUser() {
    return _singleton._user;
  }
}
