import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:mapbox_search/mapbox_search.dart';

class GeoCoding {
  /// API Key of the MapBox.
  final String apiKey;

  /// Specify the user’s language. This parameter controls the language of the text supplied in responses.
  ///
  /// Check the full list of [supported languages](https://docs.mapbox.com/api/search/#language-coverage) for the MapBox API
  final String language;

  /// The point around which you wish to retrieve place information.
  final Location location;

  ///Limit results to one or more countries. Permitted values are ISO 3166 alpha 2 country codes separated by commas.
  ///
  /// Check the full list of [supported countries](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) for the MapBox API
  final String country;

  final String _url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';

  GeoCoding({
    @required this.apiKey,
    this.language,
    this.location,
    this.country,
  }) : assert(apiKey != null);

  String _createUrl(String address) {
    String finalUrl = _url + address + '.json?';
    finalUrl += 'access_token=$apiKey';

    if (this.location != null) {
      finalUrl += '&proximity=${this.location.lng}%2C${this.location.lat}';
    }

    finalUrl += '&limit=1';

    if (country != null) {
      finalUrl += '&country=$country';
    }

    if (language != null) {
      finalUrl += '&language=$language';
    }

    return finalUrl;
  }

  Future<List<MapBoxPlace>> getLatLng(String address) async {
    String url = _createUrl(address);
    final response = await http.get(url);

    if (response?.body?.contains('message') ?? false) {
      throw Exception(json.decode(response.body)['message']);
    }

    return Predictions.fromRawJson(response.body).features;
  }

}
