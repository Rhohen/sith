import 'package:cloud_firestore/cloud_firestore.dart';

class FireConn {
  static final FireConn _singleton = initialisation();
  final Firestore databaseReference = Firestore.instance;

  factory FireConn() {
    return _singleton;
  }

  static FireConn initialisation() {
    return FireConn._internal();
  }

  FireConn._internal();

  CollectionReference getInterventionsCollection() {
    return databaseReference.collection("interventions");
  }

  CollectionReference getMeanDemandsCollection() {
    return databaseReference.collection("meanDemands");
  }

  CollectionReference getUsersCollection() {
    return databaseReference.collection("users");
  }

  CollectionReference getPersistentPoiCollection() {
    return databaseReference.collection("persistentPoi");
  }

  CollectionReference getPersistentVehiculesCollection() {
    return databaseReference.collection("persistentVehicules");
  }
}
