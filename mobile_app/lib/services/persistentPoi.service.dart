

import 'package:cloud_firestore/cloud_firestore.dart';

import 'firestore.service.dart';

class PersistentPoiService {
  static CollectionReference persistentPoiCollectionReference;

  static final PersistentPoiService _singleton = initiate();

  factory PersistentPoiService() {
    return _singleton;
  }

  /// Constructeur privé interne
  PersistentPoiService._internal();

  static initiate() {
    persistentPoiCollectionReference = FireConn().getPersistentPoiCollection();
    return PersistentPoiService._internal();
  }

  Future<QuerySnapshot> getPersistentPoi() {
    return persistentPoiCollectionReference.getDocuments();
  }

}