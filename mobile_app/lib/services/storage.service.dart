import 'package:firebase_storage/firebase_storage.dart';

class FireStorage {
  static final FireStorage _singleton = initialisation();
  final FirebaseStorage storageReference = FirebaseStorage.instance;

  factory FireStorage() {
    return _singleton;
  }

  static FireStorage initialisation() {
    return FireStorage._internal();
  }

  FireStorage._internal();

  Future<dynamic> getImage(String interventionUid, String fileName) {
    return storageReference.ref().child(interventionUid+"/"+fileName).getDownloadURL();
  }

}