import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sith/services/firestore.service.dart';

class TelemetryService {
  static CollectionReference interventionCollectionReference;

  static final TelemetryService _singleton = initiate();

  factory TelemetryService() {
    return _singleton;
  }

  TelemetryService._internal();

  static initiate() {
    interventionCollectionReference = FireConn().getInterventionsCollection();
    return TelemetryService._internal();
  }

  Stream<DocumentSnapshot> getDroneTelemetry(String interventionId) {
    return interventionCollectionReference
        .document(interventionId)
        .collection('drone')
        .document("telemetry")
        .snapshots();
  }
}
