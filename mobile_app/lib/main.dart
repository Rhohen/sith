import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n_delegate.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:sith/components/splashscreen/splashscreen.view.dart';
import 'package:sith/components/utils/color.ressources.dart';
import 'package:sith/router.dart';

Future main() async {
  final FlutterI18nDelegate flutterI18nDelegate = FlutterI18nDelegate(
      useCountryCode: false,
      fallbackFile: 'en',
      path: 'assets/i18n',
      forcedLocale: new Locale('fr'));
  WidgetsFlutterBinding.ensureInitialized();
  await flutterI18nDelegate.load(null);
  runApp(new MyApp(flutterI18nDelegate));
}

class MyApp extends StatelessWidget {
  static const routeName = '/root';
  final FlutterI18nDelegate flutterI18nDelegate;

  MyApp(this.flutterI18nDelegate);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) => Router.route(settings),
      title: 'Flutter Demo',
      localizationsDelegates: [
        flutterI18nDelegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      theme: ThemeData(
        primaryColor: ColorRessources.primaryColor,
        cursorColor: ColorRessources.darkColor,
        accentColor: ColorRessources.accentColor,
        textSelectionHandleColor: ColorRessources.darkColor,
        bottomSheetTheme:
            BottomSheetThemeData(backgroundColor: ColorRessources.darkColorTransparent),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: new Text("Sith", style: TextStyle(fontSize: 20)),
          backgroundColor: ColorRessources.darkColor,
        ),
        body: SplashScreen(),
      ),
    );
  }
}
