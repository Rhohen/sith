# -*-coding:Latin-1 -*

import argparse
import asyncio
import random
import threading
import time

import firebase
import firebase_admin
import seleniumScreenshot
from firebase_admin import credentials, storage, firestore
from mavsdk import (MissionItem)
from mavsdk import System
from shapely.geometry import Polygon, Point, LineString

mission_points = []
mission_items = []
latitude = None
longitude = None
battery = None
last_time_called = None
travel_changes = False
stop_saving = False
boucle = None
zone_travel = None
length_mission_points = None
old_length_mission_points = 0
old_mission_points = None

cred = credentials.Certificate(
    './pinponsith-firebase-adminsdk-pgx2v-60c261be56.json')
default_app = firebase_admin.initialize_app(cred, {
    'storageBucket': 'pinponsith.appspot.com'
})
db = firestore.client()
bucket = storage.bucket()

firebase.init_db(db)
seleniumScreenshot.init_bucket(bucket)


def send_telemetry():
    global latitude
    global longitude
    global battery
    threading.Timer(1.0, send_telemetry).start()
    if battery is not None and latitude is not None and longitude is not None:
        data = {
            u'battery': battery,
            u'latitude': latitude,
            u'longitude': longitude
        }
        firebase.set_telemetry(intervention_uid, data)
        print("sending telemetry... (coordonates : " + str(latitude) +
              " ; " + str(longitude) + " | battery : " + str(battery) + ")")


# Calculate random coord to cover zone

def random_points_within(poly, exclusion_poly, num_points):
    min_x, min_y, max_x, max_y = poly.bounds

    points = []
    last_point = None

    while len(points) < num_points:
        random_point = Point(
            [random.uniform(min_x, max_x), random.uniform(min_y, max_y)])

        if random_point.within(poly):
            valid = True
            if last_point is not None:
                for key in exclusion_poly.keys():
                    valid = valid and (not exclusion_poly[key].intersects(
                        LineString([last_point, random_point])))
            if last_point is None or valid:
                points.append(random_point)
                last_point = random_point
    return points


def calculate_travel():
    global mission_points
    global boucle
    global zone_travel
    global length_mission_points

    mission_points = []
    poi_drone_ref = firebase.get_poi_drones(intervention_uid)

    docs = poi_drone_ref.stream()

    polygon_points = []
    seen_hashcode = []
    inclusion_zone_points = dict()
    for doc in docs:
        poi_drone = doc.to_dict()
        coordinates = poi_drone.get("coordinates")
        geopoint = coordinates.get("geopoint")
        print(u'Latitude => {}'.format(geopoint.latitude))
        print(u'Longitude => {}'.format(geopoint.longitude))
        boucle = poi_drone.get("type") == "closed"
        zone_travel = poi_drone.get("type") == "zone_inc"
        geohash = coordinates.get("geohash")
        if not geohash in seen_hashcode:
            seen_hashcode.append(geohash)
            if poi_drone.get("areaId") <= 0:
                if boucle or poi_drone.get("type") == "nonclosed":
                    mission_points.append((geopoint.latitude, geopoint.longitude, doc.id))
                polygon_points.append((geopoint.latitude, geopoint.longitude))
            else:
                if poi_drone.get("areaId") in inclusion_zone_points.keys():
                    inclusion_zone_points[poi_drone.get("areaId")].append((geopoint.latitude, geopoint.longitude))
                else:
                    inclusion_zone_points[poi_drone.get("areaId")] = [(geopoint.latitude, geopoint.longitude)]

    if zone_travel and len(polygon_points) > 0:
        poly = Polygon(polygon_points)
        exclusion_poly = dict()
        for key in inclusion_zone_points.keys():
            exclusion_poly[key] = Polygon(inclusion_zone_points[key])
        for key in exclusion_poly.keys():
            poly_points = poly.difference(exclusion_poly[key])
            poly = Polygon(poly_points)
        points = random_points_within(poly, exclusion_poly, 150)
        for point in points:
            x, y = point.coords.xy
            mission_points.append((x[0], y[0], "random"))
    length_mission_points = len(mission_points)


def travel_changes_detection(col_snapshot, changes, read_time):
    global travel_changes
    global stop_saving
    global last_time_called
    for change in changes:
        if change.type.name == 'ADDED' and (last_time_called is None or time.time() - last_time_called >= 15):
            time.sleep(2)
            travel_changes = True
            stop_saving = False
            calculate_travel()
            last_time_called = time.time()
        if change.type.name == 'REMOVED':
            stop_saving = True


def create_mission_items():
    global mission_points
    global mission_items
    global boucle
    global zone_travel
    mission_items = []
    for point in mission_points:
        print("creating mission point: %f %f %s" % point)
        mission_items.append(MissionItem(point[0],
                                         point[1],
                                         25,
                                         10,
                                         True,
                                         float('nan'),
                                         float('nan'),
                                         MissionItem.CameraAction.NONE,
                                         float('nan'),
                                         float('nan')))

    if not boucle and not zone_travel:
        mission_items_without_first = mission_items[:-1]
        mission_items_without_first = mission_items_without_first[::-1]
        mission_items_without_first = mission_items_without_first[:-1]
        for mission_point in mission_items_without_first:
            mission_items.append(mission_point)

    return len(mission_points)


##
# Argument Parsign
##

parser = argparse.ArgumentParser()
parser.add_argument("-uid", type=str, required=True, help="UID of intervention to process")
parser.add_argument("-altitude", type=float, help="Altitude for drone to maintain", default=44)
parser.add_argument("-addr", type=str, help="address of the drone to connect to, default is \'udp://:14540\'",
                    default="udp://:14540")
args = parser.parse_args()

intervention_uid = args.uid
drone_addr = args.addr
altitude = args.altitude

# Watch the collection query
query_watch = db.collection(u'interventions').document(intervention_uid).collection(u'poiDrone').order_by(u'step') \
    .on_snapshot(travel_changes_detection)


# Creating drone mission

async def run():
    global mission_items
    global mission_points
    global old_length_mission_points
    global length_mission_points
    global old_mission_points
    global zone_travel
    drone = System()
    await drone.connect(system_address=drone_addr)

    print("Waiting for drone to connect...")
    async for state in drone.core.connection_state():
        if state.is_connected:
            print(f"Drone discovered with UUID: {state.uuid}")
            break

    asyncio.ensure_future(print_battery(drone))
    asyncio.ensure_future(print_position(drone))
    asyncio.ensure_future(print_mission_progress(drone))
    termination_task = asyncio.ensure_future(observe_is_in_air(drone))

    # Waiting for firestore to detect insert in base
    time.sleep(4)

    if len(mission_points) == 0:
        print("There were no mission points...")
        return

    old_length_mission_points = create_mission_items()

    length_mission_points = 0

    old_mission_points = mission_points

    await drone.mission.set_return_to_launch_after_mission(True)

    print("-- Uploading mission")
    await drone.mission.upload_mission(mission_items)

    print("-- Arming")
    await drone.action.arm()

    print("-- Starting mission")
    await drone.mission.start_mission()

    send_telemetry()

    await termination_task


async def print_position(drone):
    global latitude
    global longitude
    async for position in drone.telemetry.position():
        latitude = position.latitude_deg
        longitude = position.longitude_deg


async def print_battery(drone):
    global battery
    async for battery in drone.telemetry.battery():
        battery = battery.remaining_percent


async def print_mission_progress(drone):
    global travel_changes
    global zone_travel
    global mission_items
    global mission_points
    global length_mission_points
    global old_length_mission_points
    global old_mission_points
    global stop_saving
    async for mission_progress in drone.mission.mission_progress():
        print(f"Mission progress: "
              f"{mission_progress.current_item_index}/"
              f"{mission_progress.mission_count}")
        if not zone_travel:
            if old_length_mission_points > 0 and mission_progress.current_item_index != 0:
                if mission_progress.current_item_index > old_length_mission_points:
                    mission_item = old_mission_points[(mission_progress.current_item_index - (
                            (mission_progress.current_item_index - old_length_mission_points) * 2)) - 1]
                else:
                    mission_item = old_mission_points[mission_progress.current_item_index - 1]
                print("Taking sceenshot...")
                process = threading.Thread(target=seleniumScreenshot.take_google_maps,
                                           args=[mission_item[2], mission_item[0], mission_item[1], altitude,
                                                 intervention_uid, stop_saving])
                process.start()
        if travel_changes and mission_progress.current_item_index > 0 and length_mission_points > 0:
            travel_changes = False
            create_mission_items()
            print("-- Changing mission")
            old_length_mission_points = length_mission_points
            old_mission_points = mission_points
            length_mission_points = 0
            await drone.mission.upload_mission(mission_items)
            time.sleep(1)
            await drone.mission.start_mission()
        if (boucle and mission_progress.current_item_index == old_length_mission_points) or (
                not boucle and mission_progress.current_item_index == (
                old_length_mission_points + (old_length_mission_points - 2))):
            print("-- Restart mission")
            await drone.mission.upload_mission(mission_items)
            time.sleep(0.5)
            await drone.mission.start_mission()


async def observe_is_in_air(drone):
    """ Monitors whether the drone is flying or not and
    returns after landing """

    was_in_air = False

    async for is_in_air in drone.telemetry.in_air():
        if is_in_air:
            was_in_air = is_in_air

        if was_in_air and not is_in_air:
            await asyncio.get_event_loop().shutdown_asyncgens()
            return


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run())
