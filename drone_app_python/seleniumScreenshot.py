import io
import time
from datetime import datetime

import firebase
from PIL import Image
from google.api_core import exceptions
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

nbImagesDict = dict()


def init_bucket(image_bucket):
    global bucket
    bucket = image_bucket


def take_selenium_screenshot(url):
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--window-size=1920x1080")
    driver = webdriver.Chrome(
        chrome_options=chrome_options, executable_path='/usr/bin/chromedriver')
    driver.get(url)
    driver.fullscreen_window()
    time.sleep(2.5)
    data = driver.get_screenshot_as_png()
    driver.quit()
    return data


def save_screenshot(id, img_byte_arr, intervention_uid, stop_saving):
    global nbImagesDict
    time_stamp = datetime.now()
    timestr = time_stamp.strftime("%Y%m%d-%H%M%S")
    filename = id + "-" + timestr + ".png"
    blob = bucket.blob("{}/".format(intervention_uid) + filename)
    blob.upload_from_string(img_byte_arr, content_type='image/png')
    # blob.download_to_filename("LastUploadedFile.jpg")
    if not id in nbImagesDict.keys():
        nbImagesDict[id] = 1
    else:
        nbImagesDict[id] = nbImagesDict[id] + 1
    file = {
        u'name': filename,
        u'timestamp': time_stamp
    }
    poi_drone = {
        u'nbImages': nbImagesDict[id]
    }
    firebase.set_image_filename(intervention_uid, file, id)
    if not stop_saving:
        try:
            firebase.set_image_number(intervention_uid, poi_drone, id)
        except exceptions.NotFound:
            print("PoiDrone is deleted")
    print(blob.public_url)


def resize_and_save_screenshot(id, data, intervention_uid, stop_saving):
    width = 1800
    height = 900
    im = Image.open(io.BytesIO(data))
    im = im.crop((int(400), int(50), int(width), int(height)))
    img_byte_arr = io.BytesIO()
    im.save(img_byte_arr, format='PNG', optimize=True, quality=65)
    img_byte_arr = img_byte_arr.getvalue()
    save_screenshot(id, img_byte_arr, intervention_uid, stop_saving)


def take_google_maps(id, latitude, longitude, altitude, intervention_uid, stop_saving):
    url = 'https://www.google.fr/maps/@{},{},{}m/data=!3m1!1e3'.format(latitude, longitude, altitude)
    data = take_selenium_screenshot(url)
    resize_and_save_screenshot(id, data, intervention_uid, stop_saving)
