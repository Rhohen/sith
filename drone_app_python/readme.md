### Launching drone

docker run --rm -it jonasvautherin/px4-gazebo-headless:v1.10.1

### Launching mavsdk server

./mavsdk_server -p 50051

### Launching QGroundControl

./startQGControl.sh

### Chrome driver installation for Selenium

wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
sudo mv chromedriver /usr/bin/chromedriver
sudo chown root:root /usr/bin/chromedriver
sudo chmod +x /usr/bin/chromedriver