database = None


def init_db(db):
    global database
    database = db


def set_telemetry(intervention_uid, data):
    database.collection(u'interventions').document(intervention_uid).collection(
        u'drone').document(u'telemetry').set(data)


def set_image_filename(intervention_uid, file, id):
    database.collection(u'interventions').document(intervention_uid).collection(u'poiDrone').document(id).collection(
        u'images').document().set(file)


def set_image_number(intervention_uid, poi_drone, id):
    database.collection(u'interventions').document(intervention_uid).collection(u'poiDrone').document(id).update(
        poi_drone)


def get_poi_drones(intervention_uid):
    return database.collection(u'interventions').document(
        intervention_uid).collection(u'poiDrone').order_by(u'step')
